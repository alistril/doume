import datetime

from django.conf import settings
from django.core.urlresolvers import reverse
from django.db import models
from django.db.models import signals
from django.contrib.auth.models import User


from interactive_notification.models import *
from django.contrib.contenttypes import generic
from django.utils.translation import ugettext_lazy as _

class News(models.Model)
    message = models.TextField(_("message"))
    
