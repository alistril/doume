from django.utils import simplejson
from dajax.core import Dajax

from categories.models import Category
from tagging.models import Tag,TaggedItem
from haystack.query import SearchQuerySet
from dooms.models import Doom
from categories.helpers import get_popular_dooms_context,get_prefered_dooms_context,get_category_dooms_context
from dajaxice.decorators import dajaxice_register

from django.template.loader import render_to_string
from django.template import RequestContext
from miscutils import js_context
from django.core.urlresolvers import reverse


import logging

@dajaxice_register
def detail_display(request,n):
    dajax = Dajax()
    if n==-1:
        n=None
    context = get_popular_dooms_context(request,n)
    context.update({"buy_doom_redirect":reverse("categories_show_category_tree",args=[""])})
    mostpopular_html = render_to_string("categories/category_tree_dooms.html",context,RequestContext(request))
    
    dajax.assign('#doom-categories-panel','innerHTML',mostpopular_html)
    
    js_context.add_all(dajax)
    return dajax.json()

@dajaxice_register
def category_display(request,n):    
    log = logging.getLogger(__name__)
    log.debug("ajax: category_display")
    if n==-1:
        n=None  
    dajax = Dajax()
    
    context = get_category_dooms_context(request,n)
    context.update({"buy_doom_redirect":reverse("categories_show_category_tree",args=[""])})
    doom_html = render_to_string("categories/category_tree_dooms.html",context,RequestContext(request))
    
    dajax.assign('#doom-categories-panel','innerHTML',doom_html)
    
    js_context.add_all(dajax)    
    return dajax.json()


@dajaxice_register
def prefered_dooms_display(request): 
    log = logging.getLogger(__name__)
    log.debug("ajax: prefered_dooms_display")
    
    dajax = Dajax()
    
    context = get_prefered_dooms_context(request)
    context.update({"buy_doom_redirect":reverse("categories_show_category_tree",args=[""])})
    doom_html = render_to_string("categories/category_prefered_dooms.html",context)
    
    dajax.assign('#prefered-dooms-panel','innerHTML',doom_html)
    
    js_context.add_all(dajax)
    return dajax.json()


    
