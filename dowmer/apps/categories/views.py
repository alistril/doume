from django.core.urlresolvers import reverse
from django.shortcuts import render_to_response
from django.template import RequestContext


from django.contrib.auth.decorators import login_required
from categories.helpers import get_category_dooms_context,get_prefered_dooms_context
import logging
from categories.models import Category
from django.utils import simplejson
from django.http import HttpResponse

@login_required
def show_category_tree(request,openednode=None,show_categories=True):
    log = logging.getLogger(__name__)    
    context = get_category_dooms_context(request,openednode)
    
    context.update(get_prefered_dooms_context(request))
    context.update({"buy_doom_redirect":reverse("categories_show_category_tree",args=[""])})
    if openednode:
        return render_to_response("categories/category_tree.html",context,context_instance=RequestContext(request))
    else:
        return render_to_response("categories/category_root.html",context,context_instance=RequestContext(request))
    


def list_nodes(request):
    n = int(request.GET.get("id",-1))
    
    if n==-1:
        category = Category.objects.get(id=1)
        categories = category.get_siblings(include_self=True)
    else:
        category = Category.objects.get(id=n)
        categories = category.get_children()
        
    nodes = []
    for cat in categories:
        if cat.is_leaf_node():
            nodes.append({"title":cat.name,"li_attr" : { "id" : cat.id }, 'data' : { 'jstree' : { 'closed' : False } }})    
        else:
            nodes.append({"title":cat.name,"li_attr" : { "id" : cat.id }, 'data' : { 'jstree' : { 'closed' : True } }})    
    
    return HttpResponse( simplejson.dumps(nodes),'application/javascript')
