from django.conf.urls.defaults import *

urlpatterns = patterns("",
    url(r"^tree/(?P<openednode>[\w\._-]*)$", "categories.views.show_category_tree", name="categories_show_category_tree"),    
    url(r"^tree/list_nodes/$", "categories.views.list_nodes", name="categories_list_nodes"),
)
