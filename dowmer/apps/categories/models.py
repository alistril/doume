from django.db import models
from django.utils.translation import ugettext_lazy as _
from mptt.models import MPTTModel, TreeForeignKey
from django.core.urlresolvers import reverse 
from django.db.models.signals import post_save
from django.dispatch import receiver
from pubsub.utils import create_node
        
class Category(MPTTModel):
    name = models.CharField(_("name"), max_length=50, null=True, blank=True)
    description = models.CharField(_("description"), max_length=50, null=True, blank=True)
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children')
    
    class MPTTMeta:
        level_attr = 'mptt_level'
        order_insertion_by=['name']

    def get_absolute_url(self):
        return reverse("categories_show_category_tree",args=[self.pk])
        
    def __unicode__(self):
        return self.name
             
        
    def __hash__(self):
        return self.pk%100
    
       
    def __cmp__(self, other):
        if other is None:
            return -1
        else:
            return self.pk-other.pk
    
    def get_tags(self):
        return " ".join([ancestor.name for ancestor in self.get_ancestors(ascending=False, include_self=True)])

@receiver(post_save, sender=Category)
def create_category_node(sender, instance=None, created=False,**kwargs):
    try:
        create_node(u"/categories/"+unicode(instance.name))
    except Exception as inst:
        import sys, traceback
        exc_type, exc_value, exc_traceback = sys.exc_info()
        print "*** print_tb:"
        traceback.print_tb(exc_traceback, limit=1, file=sys.stdout)
        print "*** print_exception:"
        traceback.print_exception(exc_type, exc_value, exc_traceback,
                                  limit=200, file=sys.stdout)
