from categories.models import Category
from haystack.query import SearchQuerySet
from haystack.inputs import AutoQuery
from dooms.models import Doom
from interface.utils import interface_for_user

from dooms.helpers import get_prefered_dooms_and_friendships
import logging

categories_log = logging.getLogger(__name__)

def get_popular_dooms_list(request,openednode=None,show_categories=False):
    categories_log.debug("get_popular_dooms_list")
    if openednode:
        category = Category.objects.get(id=openednode) 
        search_queryset = SearchQuerySet().filter(content=AutoQuery(category.get_tags())).models(Doom).filter(on_sale=1).order_by('-modified')
    else:
        search_queryset = SearchQuerySet().all().models(Doom).filter(on_sale=1).order_by('-modified')

    doom_lists = []
    
    if show_categories: 
           
        limit = interface_for_user(request.user).categories_dooms_limit    
        dooms_results = search_queryset[:limit]
    else:
        limit = interface_for_user(request.user).popular_dooms_limit
        dooms_results = search_queryset[:limit]
    dooms = []
    
    for doom_result in dooms_results:  
        if doom_result.object:
            dooms.append(doom_result.object)
    
    doom_lists.append({u"Most popular":dooms})
    return doom_lists

def get_popular_dooms_context(request,openednode=None):    
    doom_lists = get_popular_dooms_list(request,openednode,False)
    
    context = {'show_categories': False,
            'doom_lists': doom_lists}
    
    if openednode:
        category = Category.objects.get(id=openednode)
        context.update({'opennode':category})
        
    return context
       
def get_category_dooms_context(request,openednode=None):
    context = dict()
    if openednode:
        category = Category.objects.get(id=openednode)
        categories = category.get_children()
        context.update({'opennode':category})
    else:
        try:
            category = Category.objects.get(id=1)
            categories = category.get_root().get_siblings(include_self=True)
        except Category.DoesNotExist: #maybe there are no categories defined
            categories = []
            context.update({"flags" : "no_categories_defined" })
    
    doom_lists = get_popular_dooms_list(request,openednode,True)
    categories_dooms_limit = interface_for_user(request.user).categories_dooms_limit
    
    for cat in categories:
        dooms_results = SearchQuerySet().filter(content=AutoQuery(cat.get_tags())).models(Doom).filter(on_sale=1).order_by('-modified')[:categories_dooms_limit]
   
        dooms = []
    
        for doom_result in dooms_results:  
            if doom_result.object:
                dooms.append(doom_result.object)
                
        doom_lists.append({cat:dooms})
   
    context.update({'show_categories': True,
                    'doom_lists': doom_lists
                    })
                    
    return context


    
def get_prefered_dooms_context(request):
    categories_log.debug("get_prefered_dooms_context")
    
    prefered_dooms,friendships = get_prefered_dooms_and_friendships(request)
    
    return {'prefered_dooms' : prefered_dooms,'friendships' : friendships}

