from django.conf.urls.defaults import *


urlpatterns = patterns("",
    url(r"^$", "profiles.views.profiles", name="profile_list"),
    url(r"^profile/(?P<username>[\w\._-]+)/$", "profiles.views.profile", name="profile_detail"),
    url(r"^edit/$", "profiles.views.profile_edit", name="profile_edit"),
)
