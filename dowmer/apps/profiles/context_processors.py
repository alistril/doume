from django.conf import settings
from friends.models import FriendshipInvitation, Friendship
from teams.models import Team
from django.contrib.auth.models import User
from django.contrib.auth.forms import AuthenticationForm 
import logging
log = logging.getLogger(__name__)

def profile(request):
    if request.user.is_authenticated():
        other_user = request.user
        other_friends = Friendship.objects.friends_for_user(other_user)
        other_teams = Team.objects.filter(members=other_user).order_by("name")
        is_me = True
        is_friend = False
        random_profiles = User.objects.exclude(from_user__to_user=request.user).exclude(to_user__from_user=request.user).exclude(pk__in=[settings.doume.pk,settings.void.pk,request.user.pk])
        previous_friend_invitations_from = FriendshipInvitation.objects.invitations(to_user=other_user, from_user=request.user)
        previous_friend_invitations_to = FriendshipInvitation.objects.invitations(to_user=request.user, from_user=other_user)

        context ={
            "is_me": is_me,
            "is_friend": is_friend,                    
            "other_user": other_user,
            "other_friends": other_friends,
            "other_teams": other_teams,
            "previous_friend_invitations_from" : previous_friend_invitations_from,
            "previous_friend_invitations_to" : previous_friend_invitations_to,
            "random_profiles" : random_profiles
            }
    else:
        context = {
            "is_me": False,
            "is_friend": False,                    
            "other_user": None,
            "other_friends": None,
            "other_teams": None,
            "previous_friend_invitations_from" : None,
            "previous_friend_invitations_to" : None,
            "random_profiles" : User.objects.none(),
            "auth_form" : AuthenticationForm()
            }

        
    return context
