from django.utils import simplejson

from avatar.templatetags.avatar_tags import avatar_url
from django.contrib.auth.models import User
from dajaxice.decorators import dajaxice_register
from interface.models import Visibility,Interface
from interface.utils import is_visible
from django.template.loader import render_to_string
from django.template import RequestContext

import logging
pflogger = logging.getLogger(__name__)

@dajaxice_register
def get_avatar_url(request,user): 
    pflogger.debug("AJAX: get_avatar_url")
    
    return simplejson.dumps(
        {
            "url" : avatar_url(user,size=25),
            "user" : user
        }
    )
    
@dajaxice_register
def get_panel_html(request,username):
    pflogger.debug("AJAX: get_panel_html")
    if request.user.is_authenticated():
        pflogger.debug("request.user ok")
        
        user = User.objects.get(username=username)
        
        visibility_profile = user.get_profile().interface.visibility.profile_general
        pflogger.info("here0")
        if is_visible(request.user,user,visibility_profile):            
            return render_to_string("profiles/personal_panel.html",RequestContext(request))
            
        else:
            pflogger.error("visibility access denied for general profile")
    else:
        pflogger.error("user is not auth")
