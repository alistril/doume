from django import forms

from profiles.models import Profile
from django.utils.translation import ugettext_lazy as _, ugettext

class ProfileForm(forms.ModelForm):    
    
    class Meta:
        model = Profile
        
        exclude = [
            "user",
            "interface",
            "blogrss",
            "timezone",
            "language",
            "twitter_user",
            "twitter_password",
        ]