from django.db import models
from django.utils.translation import ugettext_lazy as _

from django.core.urlresolvers import reverse
from django.db.models.signals import post_save

from django.contrib.auth.models import User
from interface.models import Interface,create_interface

from payments.models import PaymentService

from pubsub.utils import create_node

import logging
from django.conf import settings

class Profile(models.Model):
    user = models.ForeignKey(User, unique=True, verbose_name=_("user"))    
    
    first_name = models.CharField(_("first name"), max_length=50, null=True, blank=True)
    last_name = models.CharField(_("last name"), max_length=50, null=True, blank=True)
    nickname = models.CharField(_("nickname"), max_length=50, null=True, blank=True)
    age = models.IntegerField(_("age"), null=True, blank=True)    
    location = models.CharField(_("city"),
        max_length = 40,
        null = True,
        blank = True
    )
    GENDER_CHOICES = (
        (u'M', u'Male'),
        (u'F', u'Female'),
    )

    gender = models.CharField(_("gender"), max_length=2, null=True, blank=True,choices=GENDER_CHOICES)
    
    interface = models.ForeignKey(Interface, verbose_name=_("interface"),default=1)
    paymentservice = models.ManyToManyField(PaymentService,null=True,blank=True)
    paymentservice_active = models.ForeignKey(PaymentService,null=True,blank=True,verbose_name=_("paymentservice_active"),related_name="profile_paymentservice_active_related")
    
    class Meta:
        verbose_name = _("profile")
        verbose_name_plural = _("profiles")
    
    def __unicode__(self):
        return self.user.username
    
    def get_absolute_url(self):
        return reverse("profile_detail", kwargs={
            "username": self.user.username
        })


def create_profile(sender, instance=None, created=False, raw=False, **kwargs):
    log = logging.getLogger(__name__)
    if instance is None:
        return
    

    if created:
        objs = Profile.objects.filter(user=instance)
        if len(objs)==0:
            profile = Profile(user=instance)
            interface = create_interface(instance.username)
            if not (interface is None):
                profile.interface = interface
                
            profile.save()

        try:            
            create_node(u"/"+instance.username+"/friends")
            create_node(u"/"+instance.username+"/only")
            create_node(u"/"+instance.username+"/wall")            
        except Exception as inst:
            import sys,traceback
            exc_type, exc_value, exc_traceback = sys.exc_info()
            print "*** print_tb:"
            traceback.print_tb(exc_traceback, limit=1, file=sys.stdout)
            print "*** print_exception:"
            traceback.print_exception(exc_type, exc_value, exc_traceback,
                                      limit=200, file=sys.stdout)
            
"""
def create_ldap_user(sender, instance=None, created=False,**kwargs):
    ldap_add_record = [
     ('objectclass', ['person','organizationalperson','inetorgperson']),
     ('uid', [str(instance.username)]),
     ('cn', [str(instance.username)] ),
     ('sn', [str(instance.username)] ),
     ('userpassword', [str(instance.username)]),
     ('ou', ['users'])
    ]
    if created:
        try:
            con = ldap.initialize(getattr(settings, 'AUTH_LDAP_SERVER_URI')) 
            ret = con.simple_bind_s( getattr(settings, 'AUTH_LDAP_BIND_DN'),getattr(settings, 'AUTH_LDAP_BIND_PASSWORD') )
            con.add_s(getattr(settings,'AUTH_LDAP_USER_DN_TEMPLATE') % {'user':str(instance.username)}, ldap_add_record)
        except ldap.LDAPError, error_message:
            print "Error: %s " % error_message
"""
post_save.connect(create_profile, sender=User)

