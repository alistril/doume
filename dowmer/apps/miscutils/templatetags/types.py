from django import template
register = template.Library()

@register.filter('type')
def type(ob):
    return ob.__class__.__name__