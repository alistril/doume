from django.db.models.signals import post_syncdb
from django.dispatch import receiver
from profiles import models as profiles_models 
from pubsub.utils import create_node

@receiver(post_syncdb, sender=profiles_models)
def create_nodes(sender, **kwargs):
    try:
        create_node(u"/everyone")
    except Exception as inst:
        print str(inst)

