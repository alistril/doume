from payment_services.paypal.models import PaypalService
from django.contrib import admin

admin.site.register(PaypalService)