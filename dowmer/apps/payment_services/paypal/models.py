from django.contrib.contenttypes.models import ContentType

from django.contrib.contenttypes import generic
from django.utils.translation import ugettext_lazy as _
from django.db import models
from helpers import PayPal
from django.conf import settings
from django.core.urlresolvers import reverse
from payment_services.models import PaymentService
from django.contrib.contenttypes import generic

class PaypalService(models.Model):    
    parent = generic.GenericRelation(PaymentService)
    identifier = models.CharField(_("identifier"),max_length=255,null=True)
    password = models.CharField(_("password"),max_length=255,null=True)

    def preprocess(self,transaction):
        SITE_URL = getattr(settings, 'SITE_URL')
        if transaction.redirect_url is None:
            redirect_url = ""
        else:
            redirect_url = transaction.redirect_url

        finished_url = SITE_URL + reverse(u"payments_finished",args=[transaction.id])+u"?redirect="+redirect_url
        cancel_url = SITE_URL + reverse(u"payments_canceled",args=[transaction.id])+u"?redirect="+redirect_url
        paypal = PayPal()
        pp_token = paypal.SetExpressCheckout(transaction.amount,finished_url ,cancel_url)
        return paypal.PAYPAL_URL+pp_token

    def handle_payments_to_void(self,transaction):
        pp = PayPal()
        response_tokens = pp.MassPay( self.identifier, transaction.amount, "doume transaction", "none")
        return True

    def handle_payments_to_doume(self,request,transaction):
        SITE_URL = getattr(settings, 'SITE_URL')
        cancel_url = SITE_URL + reverse("payments_canceled",args=[transaction.id])
        pp = PayPal()
        token = request.GET.get('token', '')
        payerID = request.GET.get('PayerID', '')
        payment_details  = pp.DoExpressCheckoutPayment(token,SITE_URL,cancel_url,payerID,transaction.amount)
        print transaction.id
        print payment_details
        print payment_details['ACK']
        print 'Success' in payment_details['ACK']
        return 'Success' in payment_details['ACK']
        

    @staticmethod
    def name():
        return "Paypal"

    def __unicode__(self):
        return "Paypal"

    @staticmethod
    def get_form():
        from payment_services.paypal.forms import PaypalForm
        return PaypalForm

    class Meta:
        app_label = 'payment_services'