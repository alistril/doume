import payment_services.forms
from django.shortcuts import render_to_response
from payments.helpers import DoomTransfer,DoomTransferDetail
from payment_services.models import PaymentService

def get_detail_form(request):
    form = payment_services.forms.ObtainServiceForm(request.POST,user=request.user)
    if form.is_valid():
        try:
            model_instance = form.cleaned_data["service_type"].get_object_for_this_type(parent__user=request.user) #only one account per type
            detail_form = form.get_detailed_form_class()(instance=model_instance)
        except form.cleaned_data["service_type"].model_class().DoesNotExist:
            detail_form = form.get_detailed_form_class()()
        
        return render_to_response( "payment_services/detail_form.html", {"form" : detail_form })
    else:
        return context.update({
                        "type":"error",
                        "description" : "invalid form",
                        "transfer":DoomTransfer.TRANSACTION_FAILED,
                        "message_type":DoomTransferDetail.INVALID_DATA,                            
                        "form_errors":frm._errors
                        })
