from django.contrib.contenttypes.models import ContentType

from django.contrib.contenttypes import generic
from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.contrib.auth.models import User


class PaymentService(models.Model):
    user = models.ForeignKey(User)
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = generic.GenericForeignKey('content_type', 'object_id')

    credit_ability = models.BooleanField(_("credit_ability"),default=True)
    debit_ability = models.BooleanField(_("debit_ability"),default=True)

    credit_activated = models.BooleanField(_("credit_activated"),default=True)
    debit_activated = models.BooleanField(_("debit_activated"),default=True)

    def preprocess(self,transaction):
        return self.content_object.preprocess(transaction)

    def handle_payments_to_void(self,transaction):
        return self.content_object.handle_payments_to_void(transaction)

    def handle_payments_to_doume(self,request,transaction):
        return self.content_object.handle_payments_to_doume(request,transaction)

    def get_detailed_form_class(self):
        return self.content_object.get_form()
