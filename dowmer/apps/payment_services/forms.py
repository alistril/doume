from django import forms
from django.forms import ModelForm

from dooms.models import Doom
from django.utils.translation import ugettext_lazy as _, ugettext
from django.conf import settings
import payments.models
import payment_services.models
from django.contrib.contenttypes.models import ContentType

class ObtainServiceForm(forms.Form):
    service_type = forms.ModelChoiceField(queryset=ContentType.objects.filter(app_label="payment_services").exclude(model="paymentservice"))

    def __init__(self,*args,**kwargs):        
        self.user = foo = kwargs.pop('user')
        super(ObtainServiceForm,self).__init__(*args,**kwargs)
        
    def get_detailed_form_class(self):
        return self.cleaned_data["service_type"].model_class().get_form()
        
    def get_detailed_model_class(self):
        return self.cleaned_data["service_type"].model_class()
        
    def save(self):
        try:
            service =  payment_services.models.PaymentService.objects.get(user=self.user,content_type=self.cleaned_data["service_type"])
            print "FOUND",service
            return service
        except payment_services.models.PaymentService.DoesNotExist:
            print "CREATING"
            specialized_object = self.cleaned_data["service_type"].model_class().objects.create()
            print specialized_object
            print "CREATED"
            return payment_services.models.PaymentService.objects.create(content_object=specialized_object,user=self.user)