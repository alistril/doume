from django import forms
from django.utils.translation import ugettext_lazy as _

from im.utils import chat_message
from pubsub.utils import publish
from django.utils import simplejson 
from django.utils.html import escape
from dooms.models import Doom
from django.template.loader import render_to_string
from django.template import RequestContext
import re

class ChatRoomForm(forms.Form):
    message = forms.CharField(label="Message:", required=False)
    
    def __init__(self,*args,**kwargs):
        self.request  = kwargs.pop("request",None)
        super(ChatRoomForm,self).__init__(*args,**kwargs)
        
        
    def clean(self):
        message_raw = self.cleaned_data["message"]
        self.cleaned_data["message"] = escape(message_raw)
        
        def repl(m):
            doom = Doom.objects.filter(id_string=m.group(1),is_active=True,status=Doom.PENDING)
            
            if len(doom)==1:                
                return render_to_string("chat/chat_doom.html",{"doom":doom[0]},RequestContext(self.request))                
            else:
                return m.group(1)
        
        self.cleaned_data["message"] = re.sub(r"\#([a-zA-Z]+[a-zA-Z0-9]*)", repl, self.cleaned_data["message"])
        
        return self.cleaned_data
        
    def broadcast(self,user):
        msg = chat_message(user,self.cleaned_data["message"])
        data = simplejson.dumps(msg)
        publish("/everyone",data)
    