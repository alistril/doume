from django.core.urlresolvers import reverse
from django.shortcuts import render_to_response
from django.template import RequestContext


from django.contrib.auth.decorators import login_required
from categories.helpers import get_category_dooms_context,get_prefered_dooms_context
from forms import ChatRoomForm
from django.utils import simplejson
import logging
from django.http import HttpResponse
import time

@login_required
def chatroom(request):
    log = logging.getLogger(__name__)
    if request.method == "POST":
        form = ChatRoomForm(request.POST,request=request)
        if form.is_valid():
            form.broadcast(request.user)
            return HttpResponse(simplejson.dumps({"type" : "success"}),'application/javascript')
    context = {"chatroomform":ChatRoomForm()}
    return render_to_response("chat/room.html",context,context_instance=RequestContext(request))
    
@login_required
def wait(request):
    time.sleep( 5 )
    
    