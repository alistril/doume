from django.conf.urls.defaults import *


urlpatterns = patterns("",
    url(r"^$", "chat.views.chatroom", name="chatroom"),
    url(r"^wait/$", "chat.views.wait", name="chat_wait"),
)
