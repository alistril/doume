from help.models import HelpToolTip
from django.http import HttpResponseRedirect
from django.conf import settings

def enable(request):
    next = request.GET.get("next","/")
    htt = HelpToolTip.objects.get(user=request.user)
    htt.help_enabled = True;
    htt.config = getattr(settings, "HELPTOOLTIPS_DEFAULT")
    htt.save()  
    return HttpResponseRedirect(next)  
    
    
def disable(request):
    next = request.GET.get("next","/")
    htt = HelpToolTip.objects.get(user=request.user)
    htt.help_enabled = False;
    htt.config = getattr(settings, "HELPTOOLTIPS_DISABLED")
    htt.save()
    return HttpResponseRedirect(next)