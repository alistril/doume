from django.db import models
from django.contrib.auth.models import User 
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.conf import settings
import jsonfield

class HelpToolTip(models.Model):
    user = models.OneToOneField(User)
    config = jsonfield.JSONField(null=True , blank=True)
    help_enabled = models.BooleanField(default=True)
    
            
@receiver(post_save, sender=User)
def create_profile(sender, instance, created, **kwargs):    
    if created: 
        help, new = HelpToolTip.objects.get_or_create(user=instance,config = getattr(settings, "HELPTOOLTIPS_DEFAULT"))