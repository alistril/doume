from django.utils import simplejson
from help.models import HelpToolTip

from dajaxice.decorators import dajaxice_register     

@dajaxice_register
def sync(request,config):     
    if request.user.is_authenticated():
        tooltip = HelpToolTip.objects.get(user = request.user)
        tooltip.config = config
        tooltip.save()