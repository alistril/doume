from django.conf import settings
from help.models import HelpToolTip

def tooltip_config(request):
    context = {}    
    if request.user.is_authenticated():
        try:
            tooltip = HelpToolTip.objects.get(user=request.user)
            context = { "help_tooltips" : tooltip.config,"help_enabled": tooltip.help_enabled}
        except HelpToolTip.DoesNotExist:
            pass
    else:        
        context = { "help_tooltips" : getattr(settings, "HELPTOOLTIPS_DEFAULT"), "help_enabled": getattr(settings, "HELPTOOLTIPS_DEFAULT_ENABLED") }
                
    return context
