from django.conf.urls.defaults import *


urlpatterns = patterns("",
    url(r"^enable/$", "help.views.enable", name="help_enable"),
    url(r"^disable/$", "help.views.disable", name="help_disable")
)
