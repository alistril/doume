from help.models import HelpToolTip
from django.contrib import admin

class HelpToolTipAdmin(admin.ModelAdmin):
    list_display = ['user']
    
admin.site.register(HelpToolTip,HelpToolTipAdmin)