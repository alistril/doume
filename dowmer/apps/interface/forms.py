from django import forms

from interface.models import Interface, Visibility
from django.utils.translation import ugettext_lazy as _, ugettext
from django.conf import settings

class VisibilityForm(forms.ModelForm): 
    
    class Meta:
        model = Visibility
        
    
class InterfaceForm:
    visibility_form = None    
    instance = None
    args_dictionary = {}
    
    def __init__(self,*args,**kwargs):
        self.instance = kwargs.get("instance")
        #default interface is read only, do not even show it on the form
        if self.instance.name=="default":
            self.visibility_form = None
        else:
            if len(args)!=0:
                request_post = args[0]
                self.visibility_form = VisibilityForm(request_post, instance=self.instance.visibility)        
            else:
                self.visibility_form = VisibilityForm(instance=self.instance.visibility)
                
            self.args_dictionary = dict({"visibility_form":self.visibility_form})

    def save(self):
        if self.visibility_form:
            self.visibility_form.save()
        
    def is_valid(self):
        if self.visibility_form:
            return self.visibility_form.is_valid()
        else:
            return True