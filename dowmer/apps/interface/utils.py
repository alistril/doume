from friends.models import Friendship
from interface.models import Interface
from django.contrib.auth.models import User
import logging

interlog = logging.getLogger(__name__)

def interface_for_user(user):
    if user.is_authenticated():
        return user.get_profile().interface
    else:
        return Interface.objects.get(name="default")

def is_visible(user_wanting_to_see,user_being_watched,user_being_watched_policy):
    if user_being_watched_policy==0: #everyone
        return True
    elif user_being_watched_policy==1: #friends
        if Friendship.objects.are_friends(user_wanting_to_see, user_being_watched) or is_visible(user_wanting_to_see, user_being_watched,4):
            return True
        else:
            return False
    elif user_being_watched_policy==2: #Friends of friends
        interlog.error("Friends of friends not implemented")
        return True #!!! too open of a policy
    elif user_being_watched_policy==3: #
        interlog.error("Teams not implemented")
        return True #!!! too open of a policy
    elif user_being_watched_policy==4: #just me        
        return user_wanting_to_see.id==user_being_watched.id
    elif user_being_watched_policy==5: #completly hidden
        return False