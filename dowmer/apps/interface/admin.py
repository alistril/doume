from interface.models import *
from django.contrib import admin

admin.site.register(Visibility)
admin.site.register(Interface)
