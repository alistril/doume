import datetime

from django.db import models
from django.utils.translation import ugettext_lazy as _

class Error(models.Model):    
    message = models.CharField(_("message"),max_length=255,null=True)
    exception = models.CharField(_("exception"),max_length=255,null=True)