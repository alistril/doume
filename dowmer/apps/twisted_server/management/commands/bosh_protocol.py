import xml.dom.minidom 
from django.conf import settings
from twisted.internet import defer
from twisted.internet.protocol import Protocol
import logging
try:
    from http_parser.parser import HttpParser
except ImportError:
    from http_parser.pyparser import HttpParser

log = logging.getLogger(__name__)
    
class BOSHProtocol(Protocol):
    def reset(self):
        self.bosh_server = getattr(settings, 'XMPP_HOST')
        self.bosh_service = getattr(settings, 'BOSH_SERVICE')
        self.bosh_port = getattr(settings, 'XMPP_BOSH_PORT')
        self.p = HttpParser()
        self.body = []
        
    def connectionMade(self):
        self.reset()

    def dataReceived(self, data):  
        recved = len(data)
        nparsed = self.p.execute(data, recved)
        assert nparsed == recved
        
        #if self.p.is_headers_complete():
            
        if self.p.is_partial_body():
            self.body.append(self.p.recv_body())
            
        if self.p.is_message_complete():            
            content = "".join(self.body)
            domres = xml.dom.minidom.parseString(content)
            self.d.callback(domres)

    def connectionLost(self, reason):
        log.info('connection lost: %s' % reason.getErrorMessage())
        #self.d.errback(reason.getErrorMessage())
        
        
    def make_request(self,data):
        self.reset()
        stuff = str("POST %s HTTP/1.1\r\n\
Host: %s\r\n\
Content-Type: application/xml\r\n\
Content-Length: %d\r\n\
Content-type: application/xml\r\n\
Accept: */*\r\n\
Connection: keep-alive\r\n\
Accept-Encoding: gzip,deflate,sdch\r\n\
\r\n\
%s" % (self.bosh_service,self.bosh_server,len(data),data))        
        self.transport.write(stuff)
        self.d = defer.Deferred()
        return self.d
