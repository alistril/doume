import sys
import os

from twisted.application import internet, service
from twisted.web import server, resource, wsgi, static
from twisted.python import threadpool
from twisted.internet import reactor

import async_resource


PORT = 8000

# Environment setup for your Django project files:
#sys.path.append("severus")
sys.path.insert(0, os.path.abspath(os.path.dirname(__file__))+"\\severus")
sys.path.insert(0, os.path.abspath(os.path.dirname(__file__))+"\\severus\apps")

os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'
from django.conf import settings
settings.configure(DATABASE_ENGINE='sqlite3', DATABASE_NAME='dev.db',ROOT_URLCONF='severus.urls')
from django.core.handlers.wsgi import WSGIHandler
#from bank.models import Account

def wsgi_resource():
    pool = threadpool.ThreadPool()
    pool.start()
    # Allow Ctrl-C to get you out cleanly:
    reactor.addSystemEventTrigger('after', 'shutdown', pool.stop)
    wsgi_resource = wsgi.WSGIResource(reactor, pool, WSGIHandler())
    return wsgi_resource


# Twisted Application Framework setup:
application = service.Application('twisted-django')

# WSGI container for Django, combine it with twisted.web.Resource:
# XXX this is the only 'ugly' part: see the 'getChild' method in twresource.Root
wsgi_root = wsgi_resource()
root = async_resource.Root(wsgi_root)

# Servce Django media files off of /media:
#staticrsrc = static.File(os.path.join(os.path.abspath("."), "mydjangosite/media"))
#root.putChild("media", staticrsrc)

# The cool part! Add in pure Twisted Web Resouce in the mix
# This 'pure twisted' code could be using twisted's XMPP functionality, etc:
root.putChild("async", async_resource.TestResource())

# Serve it up:
main_site = server.Site(root)
internet.TCPServer(PORT, main_site).setServiceParent(application)