from django.core.management.base import BaseCommand, CommandError

import sys
import os
import re

from twisted.application import internet, service
from twisted.web import server, resource, wsgi, static
from twisted.python import threadpool
from twisted.internet import reactor
from django.contrib import admin

from twisted.application import service

import async_resource


PORT = 8000

from django.conf import settings

from django.core.handlers.wsgi import WSGIHandler

def wsgi_resource():
    pool = threadpool.ThreadPool()
    pool.start()
    # Allow Ctrl-C to get you out cleanly:
    reactor.addSystemEventTrigger('after', 'shutdown', pool.stop)
    wsgi_resource = wsgi.WSGIResource(reactor, pool, WSGIHandler())
    return wsgi_resource
    
class Command(BaseCommand):
    def handle(self, *args, **options):
        # Twisted Application Framework setup:
        application = service.Application('twisted-django')

        # WSGI container for Django, combine it with twisted.web.Resource:
        # XXX this is the only 'ugly' part: see the 'getChild' method in twresource.Root
        wsgi_root = wsgi_resource()
        root = async_resource.Root(wsgi_root)

        # Servce Django media files off of /media:        
        site_media = static.File(os.path.join(settings.PROJECT_ROOT, "site_media"))        
        root.putChild("site_media",site_media)
        
                
        # The cool part! Add in pure Twisted Web Resouce in the mix
        # This 'pure twisted' code could be using twisted's XMPP functionality, etc:
        root.putChild("async", async_resource.TestResource())
        
        # Serve it up:
        main_site = server.Site(root)
        reactor.listenTCP(PORT, main_site)        
        reactor.run()