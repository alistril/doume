from twisted.web import server, resource, client
from twisted.internet import defer
from django.conf import settings
from django.utils import simplejson
from twisted.internet import reactor
from twisted.internet.protocol import Factory
from twisted.internet.endpoints import TCP4ClientEndpoint

from pubsub.utils import subscribe_manual
from django.contrib.auth.models import User
from im.utils import subscribe_to_friends,subscribe_to_wallpost_dooms_home,subscribe_to_wall,subscribe_to_wallpost_dooms_wall
from django.contrib.sessions.models import Session
from django.contrib.auth.models import User
from bosh_utils import BoshSession
from bosh_protocol import BOSHProtocol
from pubsub.utils import publish
from im.utils import chat_join

import logging
log = logging.getLogger(__name__)

class Root(resource.Resource):

    def __init__(self, wsgi_resource):
        resource.Resource.__init__(self)
        self.wsgi_resource = wsgi_resource

    def getChild(self, path, request):
        path0 = request.prepath.pop(0)
        request.postpath.insert(0, path0)
        return self.wsgi_resource



        
class TestResource(resource.Resource):
    
    def __init__(self):
        resource.Resource.__init__(self)
        
    def gotProtocol(self,p,request):        
        bs = BoshSession(p,request)
        self.p = p
        d = bs.session_creation_request()
        d.addCallback(self.finishRequest)
        
    def finishRequest(self,creds):
        self.p.transport.loseConnection()
         
        jid = creds['jid']
        sid = creds['sid']
        rid = creds['rid']
        
        request = creds['request']
        page = request.args.get('page',None)
        if page==None:
            page=1
        else:
            page = page[0]
        
        session_key = request.getCookie('sessionid')
        session = Session.objects.get(session_key=session_key)
        uid = session.get_decoded().get('_auth_user_id')
        
        log.info("creating BOSH session with jid: %s" % jid)
        
        if uid:
            request_user = User.objects.get(pk=uid)
            
            wallpost_sub_user = request.args.get('wallpost_sub_user',[None])[0]
            wallpost_sub_home = request.args.get('wallpost_sub_home',[None])[0]
            broadcast_chat_id = request.args.get('broadcast_chat_id',[None])[0]
                
            log.info("subscribing to everyone...")
            subscribe_manual(jid,"/everyone")
    
            subscribe_to_friends(jid,request_user)
            if wallpost_sub_user:
                user = User.objects.get(username=wallpost_sub_user)
                subscribe_to_wallpost_dooms_wall(jid,user,page)
                subscribe_to_wall(request_user,jid,user)
            if wallpost_sub_home:
                subscribe_to_wallpost_dooms_home(jid,request_user,page)
                
            if broadcast_chat_id:
                publish("/everyone",simplejson.dumps(chat_join(request_user,jid)))
                    
            data = {"jid":jid,"sid":sid,"rid":rid}
        else:
            data = {"jid":"N/A","sid":"N/A","rid":0}            
            
        request.write(simplejson.dumps(data))
        request.finish()
        
    def getChild(self, name,request):
        return self
            
    def render_GET(self, request): 
        self.bosh_server = getattr(settings, 'XMPP_HOST')
        self.bosh_service = getattr(settings, 'BOSH_SERVICE')
        self.bosh_port = getattr(settings, 'XMPP_BOSH_PORT')
        factory = Factory()
        factory.protocol = BOSHProtocol
        point = TCP4ClientEndpoint(reactor, self.bosh_server, self.bosh_port)
        d = point.connect(factory)
        d.addCallback(self.gotProtocol,request=request)
        
        return server.NOT_DONE_YET
    
    def render_POST(self, request): 
        self.bosh_server = getattr(settings, 'XMPP_HOST')
        self.bosh_service = getattr(settings, 'BOSH_SERVICE')
        self.bosh_port = getattr(settings, 'XMPP_BOSH_PORT')
        
        factory = Factory()
        factory.protocol = BOSHProtocol
        point = TCP4ClientEndpoint(reactor, self.bosh_server, self.bosh_port)
        d = point.connect(factory)
        d.addCallback(self.gotProtocol,request=request)
        
        
        #request.finish()
        return server.NOT_DONE_YET