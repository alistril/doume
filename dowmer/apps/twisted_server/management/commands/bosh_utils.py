import xml.dom.minidom 
import random
from django.conf import settings
from twisted.internet import defer
import logging

log = logging.getLogger(__name__)

def get_tag(dom,tag,operation):    
    _tag = dom.getElementsByTagName(tag)
    if len(_tag)<1: 
        log.error(operation + ": Failed (no " +tag+" element)")
        log.debug("XMPP SERVER RESPONSE:")
        log.debug(dom.toprettyxml(indent="    "))
        raise Exception(operation + ": Failed (no " +tag+" element)")
        
    return _tag
    
class BoshSession(object):
    def __init__(self,bosh_link,request):            
        self.impl = xml.dom.minidom.getDOMImplementation()
        
         
        self.rid = random.randint(0, 10000000)
        self.bosh_server = getattr(settings, 'XMPP_HOST')
        self.bosh_service = getattr(settings, 'BOSH_SERVICE')
        self.bosh_port = getattr(settings, 'XMPP_BOSH_PORT')
        self.bosh_link = bosh_link
        self.request = request
        
    
    def session_creation_request(self): 
    
        self.newdoc = self.impl.createDocument(None, "body", None)
        body = self.newdoc.documentElement
        body.setAttribute("content","text/xml; charset=utf-8")
        body.setAttribute("hold","1")
        
        body.setAttribute("rid",str(self.rid))
        body.setAttribute("to",self.bosh_server)
        body.setAttribute("wait",'60')
        body.setAttribute("window",'5')
        body.setAttribute("ver",'1.6')
        body.setAttribute("xmpp:version",'1.0')
        body.setAttribute("xml:lang",'en')
        body.setAttribute("xmlns",'http://jabber.org/protocol/httpbind')
        body.setAttribute("xmlns:xmpp",'urn:xmpp:xbosh')
            
        xml_data = body.toxml()
        d = self.bosh_link.make_request(xml_data)
        d.addCallback(self.authentication_and_resource_binding)
        
        self.d = defer.Deferred()
        return self.d
        
    def authentication_and_resource_binding(self,response):        
        body = get_tag(response,"body","Session Creation Request")        
        self.sid = body[0].attributes["sid"].value
        
        self.rid=self.rid+1
        self.newdoc = self.impl.createDocument(None, "body", None)
        body = self.newdoc.documentElement
        body.setAttribute("content","text/xml; charset=utf-8")
        body.setAttribute("rid",str(self.rid))
        body.setAttribute("sid",self.sid)
        body.setAttribute("xmlns",'http://jabber.org/protocol/httpbind')
        auth = self.newdoc.createElement("auth")    
        body.appendChild(auth)
        auth.setAttribute("xmlns",'urn:ietf:params:xml:ns:xmpp-sasl')
        auth.setAttribute("mechanism","ANONYMOUS")
        
        xml_data = body.toxml() 
        
        d = self.bosh_link.make_request(xml_data)
        d.addCallback(self.restart_request)
        
    def restart_request(self,response):        
        self.rid=self.rid+1
        self.newdoc = self.impl.createDocument(None, "body", None)
        body = self.newdoc.documentElement
        body.setAttribute("content","text/xml; charset=utf-8")
        body.setAttribute("rid",str(self.rid))
        body.setAttribute("sid",self.sid)
        body.setAttribute("to",self.bosh_server)
        body.setAttribute("xmpp:restart",'true')
        body.setAttribute("xmlns",'http://jabber.org/protocol/httpbind')
        body.setAttribute("xmlns:xmpp",'urn:xmpp:xbosh')
        
        xml_data = body.toxml()  
        
        d = self.bosh_link.make_request(xml_data)            
        d.addCallback(self.resource_binding_request)

    def resource_binding_request(self,response):        
        self.rid=self.rid+1
        self.newdoc = self.impl.createDocument(None, "body", None)
        body = self.newdoc.documentElement
        body.setAttribute("content","text/xml; charset=utf-8")
        body.setAttribute("rid",str(self.rid))
        body.setAttribute("sid",self.sid)
        body.setAttribute("xmlns",'http://jabber.org/protocol/httpbind')
        iq = self.newdoc.createElement("iq")    
        body.appendChild(iq)
        iq.setAttribute("id",'bind_1')
        iq.setAttribute("type","set")
        iq.setAttribute("xmlns","jabber:client")
        bind = self.newdoc.createElement("bind")    
        iq.appendChild(bind)    
        bind.setAttribute("xmlns","urn:ietf:params:xml:ns:xmpp-bind")
        
        xml_data = body.toxml()
        
        d = self.bosh_link.make_request(xml_data)            
        d.addCallback(self.init_session_request)
        
    def init_session_request(self,response):        
        self.rid=self.rid+1
        body = get_tag(response,"body","Resource binding request")
        iq = get_tag(body[0],"iq","Resource binding request")
        jidnode = get_tag(iq[0],"jid","Resource binding request")
        if len(jidnode[0].childNodes)<1:
            log.error("Resource binding request: Failed (no data inside jid node)")
            log.debug("XMPP SERVER RESPONSE:")
            log.debug(response.toprettyxml(indent="    "))
            raise Exception("Resource binding request: Failed (no data inside jid node)")
        self.jid =  jidnode[0].childNodes[0].data            
        
        self.newdoc = self.impl.createDocument(None, "body", None)
        body = self.newdoc.documentElement
        body.setAttribute("content","text/xml; charset=utf-8")
        body.setAttribute("rid",str(self.rid))
        body.setAttribute("sid",self.sid)
        body.setAttribute("xmlns",'http://jabber.org/protocol/httpbind')
        iq = self.newdoc.createElement("iq")    
        body.appendChild(iq)
        iq.setAttribute("id",'session_1')
        iq.setAttribute("type","set")
        iq.setAttribute("xmlns","jabber:client")
        session = self.newdoc.createElement("session")    
        iq.appendChild(session)    
        session.setAttribute("xmlns","urn:ietf:params:xml:ns:xmpp-session")
        
        xml_data = body.toxml()  
        
        d = self.bosh_link.make_request(xml_data)            
        d.addCallback(self.finish_bosh)
        
    
    def finish_bosh(self,response):
        self.rid=self.rid+1
        self.d.callback({'request' : self.request, 'jid':self.jid,'sid':self.sid,'rid':self.rid})
