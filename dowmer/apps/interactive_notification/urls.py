from django.conf.urls.defaults import *

urlpatterns = patterns("",    
    url(r"^$", "interactive_notification.views.list", name="interactive_notification_list"),    
    url(r"^all/$", "interactive_notification.views.list_all", name="interactive_notification_list_all"),
    url(r"^modify_status/$", "interactive_notification.views.modify_status", name="interactive_notification_modify_status"),
    #url(r"^interactive_notifications/(?P<slug>[-\w]+)/$", "interactive_notification.views.list", name="interactive_notification_list"),

)
