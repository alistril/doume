from django.utils import simplejson

from django.template.loader import render_to_string
from django.template import RequestContext
from django.core.urlresolvers import reverse
from helpers import get_notifications,get_notifications_for_user
from dajaxice.decorators import dajaxice_register
from pubsub.utils import publish
from im.utils import NotifTypes
from interface.models import Interface


import logging
inlogger = logging.getLogger(__name__)


def modify_status(request,notif_id,status):
    inlogger.debug("ajax: modify_status")    
    group,bridge,is_member,notifications=get_notifications(request)
    
    if request.user.is_authenticated():
        if notif_id != None and status != None:
            notification = notifications.get(id=notif_id) #only one object should be returned

        
            notification.status = status
            notification.save()
            
            return simplejson.dumps(
                { "notif_id" : notif_id, "status" : status }
            )
    
@dajaxice_register
def list_all(request):        
    inlogger.debug("ajax: list_all")
    notifications = get_notifications_for_user(request.user).select_related().all()
    data=[]
    user_interface = request.user.get_profile().interface
    limit = user_interface.notifications_menu_limit
    
    i=0
    for notif in notifications:        
        if notif.status == 0:
            notif.status = 2
            notif.save()
        if i<limit:
            inlogger.debug(notif.content_type.model)
            inlogger.debug(notif)
            data.append({
                    'id' : notif.id,
                    'content_object' : notif.content_object.serilize(),
                    'content_model' : notif.content_type.model
                })
        i=i+1

    publish(u"/"+request.user.username+u"/only",simplejson.dumps({"type":NotifTypes.NOTIFICATIONS_READ}))
    return simplejson.dumps(data,use_decimal=True)
