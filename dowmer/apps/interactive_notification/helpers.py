from django.template import RequestContext
from interactive_notification.models import InteractiveNotification
from django.db.models import Q
from django.contrib import messages

def group_and_bridge(request):
    """
    Given the request we can depend on the GroupMiddleware to provide the
    group and bridge.
    """
    
    # be group aware
    group = getattr(request, "group", None)
    if group:
        bridge = request.bridge
    else:
        bridge = None
    
    return group, bridge

def group_context(group, bridge):
    # @@@ use bridge
    ctx = {
        "group": group,
    }
    if group:
        ctx["group_base"] = bridge.group_base_template()
    return ctx


def get_notifications_for_user(user):
    return InteractiveNotification.objects.filter(
        Q(reciever=user) | 
        Q(reciever_team__creator=user)
        ).order_by('-moment')

def get_notifications(request):
    group, bridge = group_and_bridge(request)
    notifications=None
    # Is the user a member of the parent group?
    if not request.user.is_authenticated():
        is_member = False
    else:
        if group:
            is_member = group.user_is_member(request.user)
        else:
            is_member = True
    
    if group:
        if is_member:
            notifications = group.content_objects(InteractiveNotification)
        else:
            messages.add_message(request, messages.WARNING,"Please stop hacking the site")
    else:
        #if my team is notified and I am the captain, I want to be notified
        notifications = get_notifications_for_user(request.user).select_related().all()
        
    return group, bridge,is_member,notifications
