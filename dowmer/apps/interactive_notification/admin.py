from django.contrib import admin

from interactive_notification.models import InteractiveNotification,SystemNoticeNotification

admin.site.register(InteractiveNotification)
admin.site.register(SystemNoticeNotification)
