from django.template import RequestContext
from django.shortcuts import render_to_response

from interactive_notification.helpers import get_notifications,group_context
from django.core.paginator import Paginator, InvalidPage, EmptyPage

import logging
inlog = logging.getLogger(__name__)
        
def list(request):
    group,bridge,is_member,notifications=get_notifications(request)
        
    return render_to_response("interactive_notification/list.html", {
            "interactive_notifications_list" : notifications
        },context_instance=RequestContext(request))
    
def list_all(request):    
    group,bridge,is_member,notifications_all=get_notifications(request)
    limit = request.user.get_profile().interface.notifications_page_limit
    paginator = Paginator(notifications_all, limit)
    try:
        page = int(request.GET.get('page', '1'))
    except ValueError:
        page = 1

    try:
        paginator_page = paginator.page(page)
    except (EmptyPage, InvalidPage):
        paginator_page = paginator.page(paginator.num_pages)
    context ={"paginator_page" : paginator_page}
        
    return render_to_response("interactive_notification/list_all.html", context,context_instance=RequestContext(request))
    
  
def modify_status(request):        
    group,bridge,is_member,notifications=get_notifications(request)
    
    if request.user.is_authenticated():
        notif_id = request.POST.get("notif_id", None)
        status = request.POST.get("status", None)
        if notif_id != None and status != None:
            notification = notifications.get(id=notif_id) #only one object should be returned

        
            notification.status = status
            notification.save()
            
    
            return render_to_response("interactive_notification/list.html", {
                "interactive_notifications_list" : notifications,            
                "is_member": is_member,    
            },context_instance=RequestContext(request, group_context(group, bridge)))
            
    return render_to_response("interactive_notification/list.html", {
                "interactive_notifications_list" : None,            
                "is_member": False,    
            },context_instance=RequestContext(request, group_context(group, bridge)))
