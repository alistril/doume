from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic
from teams.models import Team
from pubsub.utils import publish
import logging
from django.db import transaction

log = logging.getLogger(__name__)

class InteractiveNotification(models.Model):
    reciever = models.ForeignKey(User,null=True,related_name="interactivenotification_user_reciever")
    reciever_team = models.ForeignKey(Team,null=True,related_name="interactivenotification_team_reciever")
    sender = models.ForeignKey(User,null=True,related_name="interactivenotification_user_sender")
    sender_team = models.ForeignKey(Team,null=True,related_name="interactivenotification_team_sender")
    moment = models.DateTimeField(_("moment"),auto_now_add=True,null=True)    
    
    # status field:
    # 0 unread/new (and show)
    # 1 unread and hide
    # 2 read and show
    # 3 read and hide    
    status = models.IntegerField(_("status"),null=True,default=0) # read, unread etc...
    force_order = models.IntegerField(_("status"),null=True) # force notification to appear at a certain order even if it's not ok with the date 

    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = generic.GenericForeignKey('content_type', 'object_id')
    
    group_id = models.IntegerField(null=True)
    group_type = models.ForeignKey(ContentType, null=True,related_name="interactive_notification_group_type_related")
    group = generic.GenericForeignKey("group_type", "group_id")
    
    def associate_group(self,group):
        self.group = group
        self.save()
        
    def read_and_hide(self):
        self.status = 3
        self.save();
        
    def visibe(self):
        return (self.status==0 or self.status==2)
    
    def serilize(self):
        return {"content_type":self.content_type.model,"content_object":self.content_object.serilize()}
    
class SystemNoticeNotificationManager(models.Manager):
    @transaction.commit_on_success
    def create(self,**kwargs):
        from im.utils import generic_notification,NotifTypes
        user = kwargs.pop("user",None)
        notice = self.model.objects.create(**kwargs)
        notif = notice.notification.create(reciever=user)
        if type==self.model.INFO:
            log.info("For user %s:%s" % (user,kwargs.get("content"),"None"))
        elif type==self.model.WARNING:
            log.warning("For user %s:%s" % (user,kwargs.get("content"),"None"))
        elif type==self.model.ERROR:
            log.error("For user %s:%s" % (user,kwargs.get("content"),"None"))
        publish(u"/"+unicode(user.username)+u"/only",generic_notification(notif,NotifTypes.SYSTEM_NOTICE))
        return notice
    
class SystemNoticeNotification(models.Model):
    INFO = 0
    WARNING = 1
    ERROR = 2    
    NOTICE_TYPES = (
        (INFO, u'Info'),
        (WARNING, u'Warning'),
        (ERROR, u'Error'),        
    )
    
    type = models.IntegerField(_("type"), null=False, blank=False,choices=NOTICE_TYPES,default=INFO)
    content = models.CharField(_("content"),max_length=4096,null=False, blank=False)
    notification = generic.GenericRelation(InteractiveNotification)
    
    objects = models.Manager()
    objects_with_notification = SystemNoticeNotificationManager()
    
    def serilize(self):
        return {"type":self.type,"content":self.content}        
    
    