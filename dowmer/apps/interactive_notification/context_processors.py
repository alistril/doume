from interactive_notification.models import InteractiveNotification

def unread(request):
    if request.user.is_authenticated():
        return {"notifications_unread_count":InteractiveNotification.objects.filter(reciever=request.user,status=0).count()}
    else:
        return {"notifications_unread_count":0}