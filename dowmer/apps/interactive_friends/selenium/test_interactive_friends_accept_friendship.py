from interactive_friends_accept_friendship import InteractiveFriendsAcceptFriendShip
from django.utils import unittest
       

class InteractiveFriendsAcceptFriendShipTest(InteractiveFriendsAcceptFriendShip,unittest.TestCase):
    def setUp(self):
        InteractiveFriendsAcceptFriendShip.init(self)
    def test(self):
        InteractiveFriendsAcceptFriendShip.execute(self)
    def tearDown(self):
        InteractiveFriendsAcceptFriendShip.cleanup(self)