from common_test_dependencies.selenium.settings import TIMEOUT,DOMAIN,SCREENSHOT_DIR
from selenium.webdriver.support.ui import WebDriverWait 
from common_test_dependencies.selenium.auth import login
from common_test_dependencies.selenium.friends import add_friend,check_friendship_accepted
from common_test_dependencies.selenium.xmpp import reset_user,delete_user,reset_node
from common_test_dependencies.selenium.system import reset_db
import logging


class InteractiveFriendsAcceptFriendShip(object):
    def init(self):
        reset_db('lewis_alice_motar_registered.json')
        reset_user("lewis")
        reset_user("alice")
        reset_user("motar")
        
        
    def lewis_accepts_friend(self,friend):
       WebDriverWait(self.lewis_interactive_notifications,TIMEOUT ).until(lambda driver : self.lewis_interactive_notifications.find_element_by_id("if_invitation_accept_1"))
       WebDriverWait(self.lewis_interactive_notifications,TIMEOUT ).until(lambda driver : self.lewis_interactive_notifications.find_element_by_id("if_invitation_accept_2"))
       invitation_1 = self.lewis_interactive_notifications.find_element_by_id("if_invitation_1")
       if invitation_1.text.find("%s[%s]" % (friend,friend))!=-1:
           invitation_friend_accept = self.lewis_interactive_notifications.find_element_by_id("if_invitation_accept_1")
       else:
           invitation_friend_accept = self.lewis_interactive_notifications.find_element_by_id("if_invitation_accept_2")

       invitation_friend_accept.click()
       WebDriverWait(self.lewis_interactive_notifications,TIMEOUT )


    def execute(self):       
        self.lewis_interactive_notifications = login("lewis","lewis","http://%s/interactive_notifications/all/" % DOMAIN)

        self.alice_interactive_notifications = add_friend("alice","lewis")
        self.motar_interactive_notifications = add_friend("motar","lewis")

        self.lewis_interactive_notifications.get_screenshot_as_file('%s/lewis_interactive_notifications_before_accept.png' % SCREENSHOT_DIR)

        self.lewis_accepts_friend("alice")
        self.lewis_accepts_friend("motar")


        check_friendship_accepted(self.lewis_interactive_notifications,"alice")
        check_friendship_accepted(self.lewis_interactive_notifications,"motar")
        check_friendship_accepted(self.alice_interactive_notifications,"lewis")
        check_friendship_accepted(self.motar_interactive_notifications,"lewis")

        self.lewis_interactive_notifications.get_screenshot_as_file('%s/lewis_interactive_notifications_after_accept.png' % SCREENSHOT_DIR)
        self.alice_interactive_notifications.get_screenshot_as_file('%s/alice_interactive_notifications_after_accept.png' % SCREENSHOT_DIR)
        self.motar_interactive_notifications.get_screenshot_as_file('%s/motar_interactive_notifications_after_accept.png' % SCREENSHOT_DIR)
        

    def cleanup(self):
        self.lewis_interactive_notifications.quit()
        self.motar_interactive_notifications.quit()
        self.alice_interactive_notifications.quit()
        delete_user("lewis")
        delete_user("alice")
        delete_user("motar") 