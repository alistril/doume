from django.conf import settings
from django.conf.urls.defaults import *
from django.views.generic.simple import direct_to_template

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns("",
    #url(r"^admin/$", include(admin.site.urls)),
    url(r"^friendship_request/(?P<username>[\w\._-]+)$", "interactive_friends.views.friendship_request", name="interactive_friends_friendship_request"),
    url(r"^friendship_accept/$", "interactive_friends.views.friendship_accept", name="interactive_friends_friendship_accept"),
    url(r"^friendship_decline/$", "interactive_friends.views.friendship_decline", name="interactive_friends_friendship_decline"),
)
