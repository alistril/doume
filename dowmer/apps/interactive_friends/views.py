from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext

from django.contrib import messages
from django.contrib.auth.decorators import login_required

from django.utils.translation import ugettext, ugettext_lazy as _

from friends.models import *
from friends.forms import *

@login_required
def friendship_accept(request):
    if request.method == "POST":
        invitation_id = request.POST.get("invitation", None)
        try:
            invitation = FriendshipInvitation.objects.get(id=invitation_id)
            if invitation.to_user == request.user:
                invitation.accept()
                messages.add_message(request, messages.SUCCESS,
                    ugettext("Accepted friendship request from %(from_user)s") % {
                        "from_user": invitation.from_user
                    }
                )
            else: #nasty hacking attempt
                messages.add_message(request, messages.SUCCESS,"please stop trying to hack the site")
                
        except FriendshipInvitation.DoesNotExist:
            pass
     
    return HttpResponseRedirect(reverse("interactive_notification_list"))
@login_required
def friendship_decline(request):
    if request.method == "POST":
        invitation_id = request.POST.get("invitation", None)
        try:
            invitation = FriendshipInvitation.objects.get(id=invitation_id)
            if invitation.to_user == request.user:
                invitation.decline()
                messages.add_message(request, messages.SUCCESS,
                    ugettext("Declined friendship request from %(from_user)s") % {
                        "from_user": invitation.from_user
                    }
                )
            else:
                messages.add_message(request, messages.WARNING,"please stop trying to hack the site")
        except FriendshipInvitation.DoesNotExist:
            pass
    return HttpResponseRedirect(reverse("interactive_notification_list"))
    
def friendship_request(request,username,template_name="interactive_friends/add_friend.html"):
    
    if request.method == "POST":
        invite_form = InviteFriendForm(request.user, request.POST)
        if invite_form.is_valid():
            messages.add_message(request, messages.SUCCESS, ugettext("You have successfully added a new friend."))
            invite_form.save()   
            
        else:
            messages.add_message(request, messages.WARNING, ugettext("Error adding a new friend."))
            
        return HttpResponseRedirect(reverse("wall_detail", args=[request.user.username]))
    else:
        invite_form = InviteFriendForm(request.user, {
                "to_user": username,
                "message": ugettext("Let's be friends!"),
                })

        return render_to_response(template_name, {
                "username" : username,
                "invite_form": invite_form
                }, context_instance=RequestContext(request))
    
