from django.utils import simplejson
from dajax.core import Dajax
from friends.models import FriendshipInvitation

from dooms.models import Doom
from helpers import *

from django.template.loader import render_to_string
from django.template import RequestContext
from dajaxice.decorators import dajaxice_register
from pubsub.utils import publish
from im.utils import invitation_notification,NotifTypes,wallpost_invitation_notification
from miscutils import js_context
from wall.helpers import create_wall_post

import logging
iflogger = logging.getLogger(__name__)

@dajaxice_register
def friendship_accept(request,id): 
    iflogger.debug("AJAX: friendship_accept")
    try:
        invitation = FriendshipInvitation.objects.get(id=id)
    except FriendshipInvitation.DoesNotExist:
        return simplejson.dumps({"type": "error","invitation":"does not exist"})
        
    iflogger.debug("AJAX: invitation fetched")
    if invitation.to_user == request.user and invitation.status=="2":    
        iflogger.debug("AJAX: user authentificated")
        invitation.accept()
        
        wallpost_acceptor = create_wall_post(group=None,user=invitation.from_user,owner=invitation.to_user,content_object=invitation,category=0) #create news type of post containing an invitation
        wallpost_requestor = create_wall_post(group=None,user=invitation.to_user,owner=invitation.from_user,content_object=invitation,category=0) #create news type of post containing an invitation
        
        #notif_invite_acceptor = invitation_notification(invitation,NotifTypes.ACCEPTED_INVITATION)
        #notif_invite_requestor = invitation_notification(invitation,NotifTypes.INVITATION_IS_ACCEPTED)
        notif_invite_acceptor = wallpost_invitation_notification(wallpost_acceptor,NotifTypes.ACCEPTED_INVITATION)
        notif_invite_requestor = wallpost_invitation_notification(wallpost_requestor,NotifTypes.INVITATION_IS_ACCEPTED)
        iflogger.debug("AJAX: invitations generated")
        publish("/"+invitation.to_user.username+"/friends",notif_invite_acceptor)
        publish("/"+invitation.to_user.username+"/wall",notif_invite_acceptor)
        
        publish("/"+invitation.from_user.username+"/friends",notif_invite_requestor)
        publish("/"+invitation.from_user.username+"/wall",notif_invite_requestor)
        
        iflogger.debug("AJAX: notifications published")
    else: #nasty hacking attempt
        iflogger.info("AJAX: hacking attempt")
        pass
 
    iflogger.debug("AJAX: friendship_accept: success")
    return simplejson.dumps(invitation.serilize())
    
@dajaxice_register
def friendship_decline(request,id): 
    iflogger.debug("AJAX: friendship_decline")
    invitation = FriendshipInvitation.objects.get(id=id)
    iflogger.debug("AJAX: invitation fetched")
    if invitation.to_user == request.user:    
        iflogger.debug("AJAX: user authentificated")
        invitation.decline()
        notif_invite_rejector = invitation_notification(invitation,NotifTypes.REJECTED_INVITATION)
        notif_invite_requestor = invitation_notification(invitation,NotifTypes.INVITATION_IS_REJECTED)
        iflogger.debug("AJAX: invitations generated")
        publish("/"+invitation.to_user.username+"/only",notif_invite_rejector)
        publish("/"+invitation.from_user.username+"/only",notif_invite_requestor)
        iflogger.debug("AJAX: notifications published")
    else: #nasty hacking attempt
        iflogger.info("AJAX: hacking attempt")
        pass
 
    iflogger.debug("AJAX: friendship_decline: success")
    return simplejson.dumps(invitation.serilize())


@dajaxice_register
def update_friends_panel(request):
    iflogger.debug("AJAX: update_friends_panel")
    dajax = Dajax()
    friendpanel_html = render_to_string("interactive_friends/friends_panel.html",{},context_instance=RequestContext(request))
    
    dajax.assign('#friends-panel','innerHTML',friendpanel_html)
    
    js_context.add_all(dajax)
    return dajax.json()


