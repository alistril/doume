from django.template import RequestContext
from wall.models import WallPost,Message
from django.contrib import messages

from django.core.exceptions import ObjectDoesNotExist
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response, get_object_or_404
from django.contrib.auth.models import User
from friends.models import Friendship
from haystack.views import SearchView
from django.conf import settings

from transactionbutton.forms import SearchDoomForm,SearchCompetitionForm

from dooms.models import Doom
from dooms.competitions.models import Competition
from categories.helpers import get_prefered_dooms_context
from dooms.helpers import get_bought_nonsold_dooms_context

    

def window(request, template_name="transactionbutton/window.html"):
    return render_to_response(template_name,  
                              context_instance=RequestContext(request))

@login_required
def search_doom(request, form_class=SearchDoomForm, template_name="transactionbutton/tb_search_doom.html"):
    form_inst = form_class()
    context = get_prefered_dooms_context(request)
    context.update({"search_form" : form_inst})
    return render_to_response(template_name,  context,
                              context_instance=RequestContext(request))

@login_required
def search_competition(request, form_class=SearchCompetitionForm, template_name="transactionbutton/tb_search_competition.html"):
    form_inst = form_class()
    context = get_bought_nonsold_dooms_context(request)
    context.update({"search_form" : form_inst})
    return render_to_response(template_name,  context,                            
                              context_instance=RequestContext(request))
