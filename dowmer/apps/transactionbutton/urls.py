from django.conf.urls.defaults import *

urlpatterns = patterns("",   
     url(r"^window/$", "transactionbutton.views.window", name="transactionbutton_window"),
    url(r"^tb_doom_search/$","transactionbutton.views.search_doom", name="transactionbutton_doom_search"),
    url(r"^tb_competition_search/$", "transactionbutton.views.search_competition",name="transactionbutton_competition_search"),
)
