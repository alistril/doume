from django import forms

from django.utils.translation import ugettext_lazy as _, ugettext
from django.conf import settings
from django.db.models import Q
from haystack.forms import SearchForm
from haystack.query import SearchQuerySet
from ajax_select.fields import AutoCompleteSelectMultipleField, AutoCompleteSelectField
from ajax_select import make_ajax_field
import logging
import random

tblogger = logging.getLogger(__name__)

class SearchDoomForm(forms.Form):
    doom = AutoCompleteSelectField('select_doom',label=_("find another doom:"), required=False)
    
    
class SearchCompetitionForm(forms.Form):    
    competition = AutoCompleteSelectMultipleField('select_competition',label=_("create new doom (write a competition name):"), required=False)
    #competition  = make_ajax_field(Competition,'description','select_competition')