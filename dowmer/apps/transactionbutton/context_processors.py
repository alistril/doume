from transactionbutton.forms import SearchDoomForm
from dooms.forms import DoomForm

def search_forms(request):
    context = {}
    if(request.user.is_authenticated()):
        #context.update(get_prefered_dooms_context(request))
        context.update({"search_form_doom" : SearchDoomForm()})
        #context.update(get_bought_nonsold_dooms_context(request))
        context.update({"search_form_comp" : DoomForm(owner=request.user)})

    return context
