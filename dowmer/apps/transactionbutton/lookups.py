from dooms.models import Doom
from django.db.models import Q
from haystack.query import SearchQuerySet
from django.template.loader import render_to_string
from dooms.competitions.forms import competition_form_dict
import logging

tblookuplog = logging.getLogger(__name__)

class DoomLookup(object):

    def get_query(self,q,request):
        """ return a query set.  you also have access to request.user if needed """
        tblookuplog.debug("get_query")
        tblookuplog.debug(SearchQuerySet().filter(content=q).models(Doom))
        
        sqs = SearchQuerySet().filter(content=q).models(Doom)
        return sqs

    def format_result(self,res):
        tblookuplog.debug("format_result")
        """ the search results display in the dropdown menu.  may contain html and multiple-lines. will remove any |  """
        return render_to_string('dooms/doom_preview.html', { 'object': res.object})

    def format_item(self,res):
        tblookuplog.debug("format_item")
        """ the display of a currently selected object in the area below the search box. html is OK """        
        return render_to_string('transactionbutton/tb_preview_doom_selectable.html', { 'object': res.object})

    def get_objects(self,ids):
        tblookuplog.debug("get_objects")
        """ given a list of ids, return the objects ordered as you would like them on the admin page.
            this is for displaying the currently selected items (in the case of a ManyToMany field)
        """
        tblookuplog.debug(ids)
        return []

