from django.utils import simplejson


import xml.dom.minidom 
from django.conf import settings
import random
import logging

try:
    from geventhttpclient.httplib import HTTPConnection
except ImportError:
    logging.warning("CANNOT IMPORT geventhttpclient.httplib.HTTPConnection. Using httplib instead!")
    from httplib import HTTPConnection



from pubsub.utils import subscribe_manual
from friends.models import Friendship,FriendshipInvitation
from wall.models import WallPost
from home.helpers import get_home_wallposts_and_dooms
from interface.utils import is_visible
from django.core.paginator import Paginator, InvalidPage, EmptyPage

class NotifTypes:
    DOOM_NEW = 1
    DOOM_UPDATE = 2
    DOOM_NEW_SALE = 3
    DOOM_BOUGHT = 4
    DOOM_SOLD = 5
    INVITATION_IS_ACCEPTED = 6
    ACCEPTED_INVITATION = 7
    INVITATION_SENT = 8
    INVITATION_RECIEVED = 9
    INVITATION_IS_REJECTED = 10
    REJECTED_INVITATION = 11
    NEWS_SENT = 12
    NEWS_RECIEVED = 13
    NOTIFICATIONS_READ = 14
    DOOM_STATUS = 15    
    SYSTEM_NOTICE = 16
    CHAT_MESSAGE = 17
    CHAT_JOIN = 18
    
def chat_join(user,jid):
    return {u"type" : NotifTypes.CHAT_JOIN,u"user":user.username,u"jid": jid}
    
def chat_message(user,content):
    return {u"type" : NotifTypes.CHAT_MESSAGE,u"user":user.username,u"content": content}
    
def invitation_notification(invitation,type):
    notif = basic_notification(type)
    notif.update(
    {
                    u"invitation" : invitation.serilize()
    }
                    )

    return simplejson.dumps(notif)
    
def wallpost_invitation_notification(wallpost,type):
    notif = basic_notification(type)
    notif.update(
                    {
                        u"invitation" : wallpost.content_object.serilize()                                    
                    }
                    )
                    
    notif.update(
        {
        u"id" : wallpost.id,
        u"wallpost_owner" : wallpost.owner.username,
        u"writer" : wallpost.writer.username,        
        u"category" : wallpost.category
        }
    )

    return simplejson.dumps(notif)
    
def basic_notification(type):
    return {u"type": type}

def generic_notification(notif,type):
    return simplejson.dumps({u"type": type,u"notification" : notif.serilize(),u"id":notif.id},use_decimal=True)
    
def doom_basic_notification(doom,type):
    notif = basic_notification(type)    
    notif.update(
        {
            u"doom": {
                "id":doom.id,
                "owner":doom.owner.username,
                "onsale":doom.on_sale,
                "status":doom.status
            }        
        }    
    )

    return notif

def news_basic_notification(wallpost,type):
    notif = basic_notification(type)

    return notif
    
def doom_notification(doom,type):    
    return simplejson.dumps(doom_basic_notification(doom,type))
    
def wallpost_doom_notification(wallpost,type):
    if wallpost.category==0:
        notif = news_basic_notification(wallpost,type)        
    else:
        notif = doom_basic_notification(wallpost.content_object.transaction.doom,type)  
        notif.update({u"transaction" : wallpost.content_object.transaction.serilize()})
        
    notif.update(
        {
        u"id" : wallpost.id,        
        u"wallpost_owner" : wallpost.owner.username,
        u"category" : wallpost.category        
        }     
    )
    notif.update(wallpost.content_object.serilize())
    if wallpost.writer:
        notif.update({u"writer": wallpost.writer.username})
    elif type==NotifTypes.DOOM_BOUGHT:
        notif.update({u"writer": wallpost.content_object.transaction.user_paid.username})
    elif type==NotifTypes.DOOM_SOLD:
        notif.update({u"writer": wallpost.content_object.transaction.user_paying.username})

    return simplejson.dumps(notif,use_decimal=True)    
    
def doom_new_notification():
    notif = basic_notification(NotifTypes.DOOM_NEW)
    return simplejson.dumps(notif)


    
def doom_update_notification(doomid):
    notif = basic_notification(NotifTypes.DOOM_UPDATE)
    notif.update({u"doomid":doomid})
    return simplejson.dumps(notif)

def get_tag(dom,tag,operation):
    log = logging.getLogger(__name__)
    _tag = dom.getElementsByTagName(tag)
    if len(_tag)<1: 
        log.error(operation + ": Failed (no " +tag+" element)")
        log.debug("XMPP SERVER RESPONSE:")
        log.debug(dom.toprettyxml(indent="    "))
        raise Exception(operation + ": Failed (no " +tag+" element)")
        
    return _tag
    
def create_bosh_session():
    log = logging.getLogger(__name__)
    # <body content='text/xml; charset=utf-8'
      # from='user@example.com'
      # hold='1'
      # rid='1573741820'
      # to='example.com'
      # route='xmpp:example.com:9999'
      # secure='true'
      # wait='60'
      # xml:lang='en'
      # xmpp:version='1.0'
      # xmlns='http://jabber.org/protocol/httpbind'
      # xmlns:xmpp='urn:xmpp:xbosh'/>
      
    bosh_server = getattr(settings, 'XMPP_HOST')
    bosh_service = getattr(settings, 'BOSH_SERVICE')
    bosh_port = getattr(settings, 'XMPP_BOSH_PORT')
    
    http_headers = {"Content-type": "application/xml",
                        "Accept": "*/*",
                        "Connection": "keep-alive",
                        "Accept-Encoding": "gzip,deflate,sdch",
                        }
    
    
    
    impl = xml.dom.minidom.getDOMImplementation()

    newdoc = impl.createDocument(None, "body", None)
    body = newdoc.documentElement
    body.setAttribute("content","text/xml; charset=utf-8")
    body.setAttribute("hold","1")
    rid = random.randint(0, 10000000)
    body.setAttribute("rid",str(rid))
    body.setAttribute("to",bosh_server)
    body.setAttribute("wait",'60')
    body.setAttribute("window",'5')
    body.setAttribute("ver",'1.6')
    body.setAttribute("xmpp:version",'1.0')
    body.setAttribute("xml:lang",'en')
    body.setAttribute("xmlns",'http://jabber.org/protocol/httpbind')
    body.setAttribute("xmlns:xmpp",'urn:xmpp:xbosh')
        
    xml_data = body.toxml()    
    
    log.info("connecting to: %s" , bosh_server)
    conn = HTTPConnection(bosh_server,bosh_port)
    
    log.info("Session Creation Request: Processing")
    log.debug("XMPP SERVER REQUEST:")
    log.debug(xml_data)

    conn.request("POST", bosh_service, xml_data, http_headers)
    response = conn.getresponse()
    
    if response.status==200:                
        domres = xml.dom.minidom.parseString(response.read())
        body = get_tag(domres,"body","Session Creation Request")        
        sid = body[0].attributes["sid"].value
        rid=rid+1        
        log.info("Session Creation Request: Success")
        log.debug("XMPP SERVER RESPONSE:")        
        log.debug(domres.toprettyxml(indent="    "))
        
        log.debug("sid=%s",sid)
    else:
        log.error("Session Creation Request: Failed") 
        log.debug("XMPP SERVER RESPONSE:")
        log.debug(response.read())
        raise Exception("Session Creation Request: Failed")

#        <body rid='1573741821'
#              sid='SomeSID'
#              xmlns='http://jabber.org/protocol/httpbind'>
#          <auth xmlns='urn:ietf:params:xml:ns:xmpp-sasl' 
#                mechanism='SCRAM-SHA-1'>
#             biwsbj1qdWxpZXQscj1vTXNUQUF3QUFBQU1BQUFBTlAwVEFBQUFBQUJQVTBBQQ==
#           </auth>
#        </body>        
    
    newdoc = impl.createDocument(None, "body", None)
    body = newdoc.documentElement
    body.setAttribute("content","text/xml; charset=utf-8")
    body.setAttribute("rid",str(rid))
    body.setAttribute("sid",sid)
    body.setAttribute("xmlns",'http://jabber.org/protocol/httpbind')
    auth = newdoc.createElement("auth")    
    body.appendChild(auth)
    auth.setAttribute("xmlns",'urn:ietf:params:xml:ns:xmpp-sasl')
    auth.setAttribute("mechanism","ANONYMOUS")
    xml_data = body.toxml() 
    log.info("Authentication and Resource Binding: Processing")
    log.debug("XMPP SERVER REQUEST:")
    log.debug(xml_data)
    
    conn.request("POST", bosh_service, xml_data, http_headers)
    response = conn.getresponse()
    
    if response.status==200:
        domres = xml.dom.minidom.parseString(response.read())
        body = get_tag(domres,"body","Authentication and Resource Binding")
        success = get_tag(body[0],"success","Authentication and Resource Binding")
        
        log.info("Authentication and Resource Binding: Success")        
        log.debug("XMPP SERVER RESPONSE:")
        log.debug(domres.toprettyxml(indent="    "))
    else:
        log.error("Authentication and Resource Binding: Failed")
        log.debug("XMPP SERVER RESPONSE:")        
        log.debug(response.read())
        raise Exception("Authentication and Resource Binding: Failed")
        
    rid=rid+1
    #<body rid='1573741824'
      #sid='SomeSID'
      #to='example.com'
      #xml:lang='en'
      #xmpp:restart='true'
      #xmlns='http://jabber.org/protocol/httpbind'
      #xmlns:xmpp='urn:xmpp:xbosh'/>
      
    newdoc = impl.createDocument(None, "body", None)
    body = newdoc.documentElement
    body.setAttribute("content","text/xml; charset=utf-8")
    body.setAttribute("rid",str(rid))
    body.setAttribute("sid",sid)
    body.setAttribute("to",bosh_server)
    body.setAttribute("xmpp:restart",'true')
    body.setAttribute("xmlns",'http://jabber.org/protocol/httpbind')    
    body.setAttribute("xmlns:xmpp",'urn:xmpp:xbosh')
    
    xml_data = body.toxml() 
    log.info("Restart request: Processing")
    log.debug("XMPP SERVER REQUEST:")
    log.debug(xml_data)
    
    conn.request("POST", bosh_service, xml_data, http_headers)
    response = conn.getresponse()
    
    if response.status==200:
        domres = xml.dom.minidom.parseString(response.read())
        body = get_tag(domres,"body","Restart request")
        stream_features = get_tag(body[0] ,"stream:features","Restart request")
        
        log.info("Restart request: Success")        
        log.debug("XMPP SERVER RESPONSE:")
        log.debug(domres.toprettyxml(indent="    "))
    else:
        log.error("Restart request: Failed")
        log.debug("XMPP SERVER RESPONSE:")        
        log.debug(response.read())
        raise Exception("Restart request: Failed")
        
    rid=rid+1

#<body rid='1573741825'
      #sid='SomeSID'
      #xmlns='http://jabber.org/protocol/httpbind'>
  #<iq id='bind_1'
      #type='set'
      #xmlns='jabber:client'>
    #<bind xmlns='urn:ietf:params:xml:ns:xmpp-bind'>
      #<resource>httpclient</resource>
    #</bind>
  #</iq>
#</body>

    newdoc = impl.createDocument(None, "body", None)
    body = newdoc.documentElement
    body.setAttribute("content","text/xml; charset=utf-8")
    body.setAttribute("rid",str(rid))
    body.setAttribute("sid",sid)
    body.setAttribute("xmlns",'http://jabber.org/protocol/httpbind')
    iq = newdoc.createElement("iq")    
    body.appendChild(iq)
    iq.setAttribute("id",'bind_1')
    iq.setAttribute("type","set")
    iq.setAttribute("xmlns","jabber:client")
    bind = newdoc.createElement("bind")    
    iq.appendChild(bind)    
    bind.setAttribute("xmlns","urn:ietf:params:xml:ns:xmpp-bind")
    
    xml_data = body.toxml() 
    log.info("Resource binding request: Processing")
    log.debug("XMPP SERVER REQUEST:")
    log.debug(xml_data)
    
    conn.request("POST", bosh_service, xml_data, http_headers)
    response = conn.getresponse()

    if response.status==200:
        domres = xml.dom.minidom.parseString(response.read())
        body = get_tag(domres,"body","Resource binding request")
        iq = get_tag(body[0],"iq","Resource binding request")
        jidnode = get_tag(iq[0],"jid","Resource binding request")
        if len(jidnode[0].childNodes)<1:
            log.error("Resource binding request: Failed (no data inside jid node)")
            log.debug("XMPP SERVER RESPONSE:")
            log.debug(domres.toprettyxml(indent="    "))
            raise Exception("Resource binding request: Failed (no data inside jid node)")
        jid =  jidnode[0].childNodes[0].data
        
        log.info("Resource binding request: Success")        
        log.debug("XMPP SERVER RESPONSE:")
        log.debug(domres.toprettyxml(indent="    "))
    else:
        log.error("Resource binding request: Failed")
        log.debug("XMPP SERVER RESPONSE:")        
        log.debug(response.read())
        raise Exception("Resource binding request: Failed")
    
    log.debug("jid=%s",jid)
    rid=rid+1

    newdoc = impl.createDocument(None, "body", None)
    body = newdoc.documentElement
    body.setAttribute("content","text/xml; charset=utf-8")
    body.setAttribute("rid",str(rid))
    body.setAttribute("sid",sid)
    body.setAttribute("xmlns",'http://jabber.org/protocol/httpbind')
    iq = newdoc.createElement("iq")    
    body.appendChild(iq)
    iq.setAttribute("id",'session_1')
    iq.setAttribute("type","set")
    iq.setAttribute("xmlns","jabber:client")
    session = newdoc.createElement("session")    
    iq.appendChild(session)    
    session.setAttribute("xmlns","urn:ietf:params:xml:ns:xmpp-session")
    
    xml_data = body.toxml() 
    log.info("Init Session: Processing")
    log.debug("XMPP SERVER REQUEST:")
    log.debug(xml_data)
    
    conn.request("POST", bosh_service, xml_data, http_headers)
    response = conn.getresponse()
    
    if response.status==200:
        domres = xml.dom.minidom.parseString(response.read())
        body = get_tag(domres,"body","Init Session")
        iq = get_tag(body[0],"iq","Init Session")
        type = iq[0].getAttribute("type")
        if type=="result":
            log.info("Init Session: Success")        
            log.debug("XMPP SERVER RESPONSE:")
            log.debug(domres.toprettyxml(indent="    "))
        else:
            log.error("Init Session: Failed")
            log.debug("XMPP SERVER RESPONSE:")
            log.debug(domres.toprettyxml(indent="    "))
            raise Exception("Init Session: Failed")            
    else:
        log.error("Init Session: Failed")
        log.debug("XMPP SERVER RESPONSE:")        
        log.debug(response.read())
        raise Exception("Init Session: Failed")

    rid=rid+1    
    log.info("BOSH session established with jid=%s,sid=%s,rid=%s",jid,sid,rid)
    return jid,sid, rid

#subscribes to ME and FRIENDS    


def subscribe_to_friends(jid,user):
    log = logging.getLogger(__name__)
    friendships = Friendship.objects.friends_for_user(user)
    
    try:            
        log.info("subscribing to " + user.username + "/{only,friends}...")
        subscribe_manual(jid,"/"+user.username+"/only")
        subscribe_manual(jid,"/"+user.username+"/friends")
        for friendship in friendships:  
            friend = friendship["friend"]
            log.info("subscribing to " + friend.username + "/friends...")
            
            subscribe_manual(jid,"/"+friend.username+"/friends")
    except Exception as inst:
        log.error("Error: %s",str(inst))




def subscribe_to_wallpost_dooms_home(jid,user,page):
    from dooms.models import Doom
    log = logging.getLogger(__name__)
    wallposts,dooms,finished_dooms = get_home_wallposts_and_dooms(user)
    log.debug("wallposts[dooms] subscription list:")
    log.debug(wallposts)
    if wallposts!=None:
        paginator = Paginator(wallposts, 10)
        page = paginator.page(page)    
        
        wallposts_page = wallposts[0 if page.start_index()-1<0 else page.start_index()-1 :page.end_index()]
        dooms_frontsale = dooms
        dooms_page = Doom.objects.filter(is_active=True).filter(transaction__messagetransactionwallpost__wallpost__in=wallposts_page).exclude(id__in=dooms_frontsale.values_list('id',flat=True)).filter(transaction__messagetransactionwallpost__in=wallposts.filter(content_type__model="messagetransactionwallpost").values_list('object_id',flat=True)).distinct()        
        
        try:            
            log.info("subscribing to wallpost dooms on front sale...")
            for doom in dooms_frontsale:              
                log.info("subscribing to doom " + str(doom) + "(/dooms/" + str(doom.id) + ")")            
                subscribe_manual(jid,"/dooms/" + str(doom.id))
            log.info("subscribing to wallpost dooms on page...")
            for doom in dooms_page:              
                log.info("subscribing to doom " + str(doom) + "(/dooms/" + str(doom.id) + ")")            
                subscribe_manual(jid,"/dooms/" + str(doom.id))
        except Exception as inst:
            log.error("Error: %s",str(inst))

#subscribe to all dooms on frontsale+all other dooms on current page
def subscribe_to_wallpost_dooms_wall(jid,user,page):
    from dooms.models import Doom
    log = logging.getLogger(__name__)
    wallposts = WallPost.objects.filter(owner=user).order_by("-moment")
    if len(wallposts):
        paginator = Paginator(wallposts, 10)
        page = paginator.page(page)    
        wallposts_page = wallposts[0 if page.start_index()-1<0 else page.start_index()-1:page.end_index()]
        dooms_frontsale = Doom.objects.filter(on_sale=True,owner=user,is_active=True,status=Doom.PENDING)
        dooms_page = Doom.objects.filter(is_active=True).filter(transaction__messagetransactionwallpost__wallpost__in=wallposts_page).exclude(id__in=dooms_frontsale.values_list('id',flat=True)).filter(transaction__messagetransactionwallpost__in=wallposts.filter(content_type__model="messagetransactionwallpost").values_list('object_id',flat=True)).distinct()        
        
        try:            
            log.info("subscribing to wallpost dooms on front sale...")
            for doom in dooms_frontsale:              
                log.info("subscribing to doom " + str(doom) + "(/dooms/" + str(doom.id) + ")")            
                subscribe_manual(jid,"/dooms/" + str(doom.id))
            log.info("subscribing to wallpost dooms on page...")
            for doom in dooms_page:              
                log.info("subscribing to doom " + str(doom) + "(/dooms/" + str(doom.id) + ")")            
                subscribe_manual(jid,"/dooms/" + str(doom.id))
        except Exception as inst:
            log.error("Error: %s",str(inst))
    
def subscribe_to_wall(who,jid,user):
    log = logging.getLogger(__name__)
    visibility_wall = user.get_profile().interface.visibility.wall 
    if not is_visible(who,user,visibility_wall):
        log.warning("Access denied to %s trying to access %s's wall",who.username,user.username)
        return
    try:                    
        log.info("subscribing to /" + user.username + "/wall")
        subscribe_manual(jid,"/"+user.username+"/wall")
    except Exception as inst:
        log.error("Error: %s",str(inst))
