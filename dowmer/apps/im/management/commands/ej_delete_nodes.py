import logging

from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
from categories.models import Category
from pubsub.utils import delete
from dooms.models import Doom


class Command(BaseCommand):
    """
    Acts as an auth service for ejabberd through ejabberds external auth
    option. See contrib/ejabberd/ejabber.cfg for an example configuration.
    """

    help = "Creates nodes for all users"


    def handle(self, **options):
        try:
            users = User.objects.all()
            print "deleting everyone..."        
            try:
                delete(u"/everyone")
            except Exception as inst:
                print "Exception:" + str(inst)
                
            print "deleting users..."
            
            for user in users:
                print "deleting node for " + user.username + "/only..."
                try:
                    delete(u"/"+user.username + "/only")
                                        
                except Exception as inst:
                    print "Exception:" + str(inst)
                    
                print "deleting node for " + user.username + "/wall..."
                try:
                    delete(u"/"+user.username + "/wall")
                                        
                except Exception as inst:
                    print "Exception:" + str(inst)
            
                print "deleting node for " + user.username + "/friends..."
                try:
                    delete(u"/"+user.username+"/friends")
                                        
                except Exception as inst:
                    print "Exception:" + str(inst)
                
            print "deleting nodes for categories..."
            for category in Category.objects.all():
                print "deleting node for " + category.name + "..."
                try:
                    delete(u"/categories/"+category.name)
                except Exception as inst:
                    print "Exception:" + str(inst)
                    
            print "deleting nodes for dooms..."
            for doom in Doom.objects.all():
                print "deleting node for " + str(doom) + "..."
                try:
                    delete(u"/dooms/"+str(doom.id))
                except Exception as inst:
                    print "Exception:" + str(inst)
        except KeyboardInterrupt:
            logging.debug("Received Keyboard Interrupt")
            raise SystemExit(0)

    def __del__(self):
        """
        What to do when we are shut off.
        """
        logging.info('ej_create_nodes process stopped')
