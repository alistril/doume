
import logging
import os
import struct
import sys

from django.core.management.base import BaseCommand
from django.contrib.auth.models import User, check_password
from categories.models import Category
from django.conf import settings
from pubsub.utils import subscribe,configure_temporary_subscriptions,subscribe_temp,get_nodes_config_default
from friends.models import Friendship


class Command(BaseCommand):
    """
    Acts as an auth service for ejabberd through ejabberds external auth
    option. See contrib/ejabberd/ejabber.cfg for an example configuration.
    """

    help = "Creates nodes for all users"


    def handle(self, **options):
        try:
            #get_nodes_config_default()
            #subscribe_temp(User.objects.get(username="admin"),u"/everyone")
            #return;
            users = User.objects.all()
            print "subscribing to everyone..."
            for user in users:
                print "subscribing " + user.username + "..."
                try:
                    subscribe(user,u"/everyone")
                    
                except Exception as inst:
                    print "Exception:" + str(inst)                
                
            print "subscribing to users..."
            
            for user in users:
                print "subscribing to " + user.username + "'s friends..."
                friendships = Friendship.objects.friends_for_user(user)
                prefered_dooms = None
                
                for friendship in friendships:  
                    friend = friendship["friend"]
                    print "subscribing to " + friend.username + "..."                    
                    try:
                        subscribe(user,"/"+friend.username)
                    except Exception as inst:
                        print "Exception:" + str(inst)

        except KeyboardInterrupt:
            logging.debug("Received Keyboard Interrupt")
            raise SystemExit(0)

    def __del__(self):
        """
        What to do when we are shut off.
        """
        logging.info('ej_create_nodes process stopped')
