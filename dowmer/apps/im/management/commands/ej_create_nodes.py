
import logging

from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
from categories.models import Category
from pubsub.utils import create_node
from dooms.models import Doom

class Command(BaseCommand):
    """
    Acts as an auth service for ejabberd through ejabberds external auth
    option. See contrib/ejabberd/ejabber.cfg for an example configuration.
    """

    help = "Creates nodes for all users"


    def handle(self, **options):
        
        try:
            print "creating node for everyone..."
            try:
                create_node(u"/everyone")
            except Exception as inst:
                print "Exception:" + str(inst)

            print "creating nodes for users..."
            users = User.objects.all()
            for user in users:
                print "creating node for " + user.username + "/only..."
                try:
                    create_node(u"/"+user.username + "/only")
                                        
                except Exception as inst:
                    print "Exception:" + str(inst)
                    
                print "creating node for " + user.username + "/wall..."
                try:
                    create_node(u"/"+user.username + "/wall")
                                        
                except Exception as inst:
                    print "Exception:" + str(inst)
            
                print "creating node for " + user.username + "/friends..."
                try:
                    create_node(u"/"+user.username+"/friends")
                                        
                except Exception as inst:
                    print "Exception:" + str(inst)
            
                
            print "creating nodes for categories..."
            for category in Category.objects.all():
                print "creating node for " + category.name + "..."
                try:
                    create_node(u"/categories/"+category.name)
                except Exception as inst:
                    print "Exception:" + str(inst)
                    
            print "creating nodes for dooms..."
            for doom in Doom.objects.all():
                print "creating node for " + str(doom) + "..."
                try:
                    create_node(u"/dooms/"+str(doom.id))
                except Exception as inst:
                    print "Exception:" + str(inst)                    
            
        except KeyboardInterrupt:
            logging.debug("Received Keyboard Interrupt")
            raise SystemExit(0)

    def __del__(self):
        """
        What to do when we are shut off.
        """
        logging.info('ej_create_nodes process stopped')
