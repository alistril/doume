from im.utils import create_bosh_session,subscribe_to_friends,subscribe_to_wallpost_dooms_home,subscribe_to_wall,subscribe_to_wallpost_dooms_wall
from pubsub.utils import subscribe_manual,publish
import logging
from dajaxice.decorators import dajaxice_register
from django.utils import simplejson
from django.contrib.auth.models import User
from im.utils import chat_join

@dajaxice_register
def create_im_session(request,wallpost_sub_user=None,wallpost_sub_home=False,broadcast_chat_id=False,page=1): 
    log = logging.getLogger(__name__)
    log.info("AJAX: create_im_session")
    
    if request.user.is_authenticated():
        jid,sid,rid = create_bosh_session()    
        log.info("subscribing to everyone...")
        subscribe_manual(jid,"/everyone")
        subscribe_to_friends(jid,request.user)
        if wallpost_sub_user:
            user = User.objects.get(username=wallpost_sub_user)
            subscribe_to_wallpost_dooms_wall(jid,user,page)
            subscribe_to_wall(request.user,jid,user)
        if wallpost_sub_home:
            subscribe_to_wallpost_dooms_home(jid,request.user,page)
        if broadcast_chat_id:
            publish("/everyone",simplejson.dumps(chat_join(request.user,jid)))
        data = {"jid":jid,"sid":sid,"rid":rid}
    else:
        data = {"jid":"N/A","sid":"N/A","rid":0}
    
    return simplejson.dumps(data)
