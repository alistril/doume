from django.template import RequestContext
from wall.models import WallPost,Message
from django.contrib import messages

from django.core.exceptions import ObjectDoesNotExist
from django.template import RequestContext
from wall.forms import MessageForm
from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response, get_object_or_404
from django.contrib.auth.models import User
from teams.models import Team
from wall.helpers import *
from dooms.models import Doom
from dooms.competitions.models import Competition
from dooms.forms import DoomForm,DoomChangeForm
from django.http import HttpResponse, HttpResponseForbidden, HttpResponseRedirect
from django.core.urlresolvers import reverse
from wall.models import WallPost
from dooms.forms import *
from django import forms
from django.utils.translation import ugettext_lazy as _, ugettext

from payments.helpers import DoomTransfer,DoomTransferDetail
from payments.views import json_pay
from dooms.helpers import *
from django.conf import settings
from django.utils import simplejson
from django.template.loader import render_to_string

import logging
log=logging.getLogger(__name__)

def preview(request,doomid=None):
    doom = get_object_or_404(Doom,id=doomid)
    return render_to_response("dooms/doom_preview_with_interaction.html", {"object":doom}, RequestContext(request))
    
def buy(request,doomid=None,template_name="dooms/doom_buy.html"):
    other_user = None
    group, bridge = group_and_bridge(request)
    
    if not request.user.is_authenticated():
        context={}
        if request.method == "POST":
            context.update({
                    "type":"error",
                    "description":"You are not logged in",
                    "transfer":DoomTransfer.TRANSACTION_FAILED_AND_REDIRECT,
                    "message_type":DoomTransferDetail.NOT_LOGGED_IN,
                    "redirect_url":reverse('auth_login', args=[])
                })
            return HttpResponse(simplejson.dumps(context),'application/javascript')
        else:
            return render_to_response("dooms/doom_buy_refused.html", {"types":DoomTransferDetail,"message_type":DoomTransferDetail.NOT_LOGGED_IN}, RequestContext(request))


    try:
        doom = Doom.objects.get(id=doomid,on_sale=True,is_active=True)
    except Doom.DoesNotExist:
        if request.method == "POST":
            return HttpResponse(simplejson.dumps({"transfer" : DoomTransfer.TRANSACTION_FAILED,"message_type":DoomTransferDetail.NOT_ON_SALE}),'application/javascript')
        else:
            return render_to_response("dooms/doom_buy_refused.html",{"message_type":DoomTransferDetail.NOT_ON_SALE}, RequestContext(request))

    if doom.owner==request.user: #you cannot buy from yourself
        if request.method == "POST":
            return HttpResponse(simplejson.dumps({"transfer" : DoomTransfer.TRANSACTION_FAILED,"message_type":DoomTransferDetail.CANNOT_BUY_FROM_SELF}),'application/javascript')
        else:
            return render_to_response("dooms/doom_buy_refused.html",{"message_type":DoomTransferDetail.CANNOT_BUY_FROM_SELF}, RequestContext(request))
        
    if doom.status!=Doom.PENDING: #only allowed to buy PENDING dooms
        if request.method == "POST":
            return HttpResponse(simplejson.dumps({"transfer" : DoomTransfer.TRANSACTION_FAILED,"message_type":DoomTransferDetail.FORBIDDEN_STATUS,"doom":doom.serilize()}),'application/javascript')
        else:
            return render_to_response("dooms/doom_buy_refused.html",{"message_type":DoomTransferDetail.FORBIDDEN_STATUS}, RequestContext(request))
    context = dict()
    buy_doom_redirect = request.GET.get("buy_doom_redirect",None)
    if buy_doom_redirect!=None:
        context.update({"buy_doom_redirect":buy_doom_redirect})
            
    
    #if not group:
    if request.method == "POST":        
        #payment_finished.connect(transfer_doom)
        payment_service = request.user.get_profile().paymentservice_active #get first payment service
        transaction = Transaction.objects.create(user_paying=request.user,user_paid=doom.owner,doom=doom,amount=doom.price,service=payment_service,redirect_url=buy_doom_redirect)
        return json_pay(request,transactionid=transaction.id)
        
    else:
        context.update({"doom" : doom})
        if request.GET.get("page",False):
            context.update({"buy_doom_redirect" : reverse("wall_detail",kwargs={"username" : request.user.username})})
            return render_to_response("dooms/doom_buy_page.html", context ,RequestContext(request,group_context(group,bridge)))
        else:
            return render_to_response(template_name, context ,RequestContext(request,group_context(group,bridge)))
        

@transaction.commit_on_success
def sell(request,doomid=None,template_name="dooms/doom_sell.html"):
    other_user = None    
    doom = None
    context={}
    
    if not request.user.is_authenticated():
        if request.method == "POST":
            context.update({
                    "type":"error",
                    "description":"You are not logged in",
                    "transfer":DoomTransfer.TRANSACTION_FAILED_AND_REDIRECT,
                    "message_type":DoomTransferDetail.NOT_LOGGED_IN,
                    "redirect_url":reverse('auth_login', args=[])
                })
            return HttpResponse(simplejson.dumps(context),'application/javascript')
        else:
            return render_to_response("dooms/doom_sell_refused.html", {"types":DoomTransferDetail,"message_type":DoomTransferDetail.NOT_LOGGED_IN}, RequestContext(request))
    
    try:
        doom = Doom.objects.get(id=doomid,on_sale=False,owner=request.user,is_active=True)
    except Doom.DoesNotExist:
        if request.method == "POST":
            context.update({
                    "type":"error",
                    "description":"doom does not exist or is not on sale",                    
                    "transfer":DoomTransfer.TRANSACTION_FAILED,
                    "message_type":DoomTransferDetail.DOES_NOT_EXIST_OR_ALREADY_ON_SALE                    
                })
            return HttpResponse(simplejson.dumps(context),'application/javascript')
        else:
            return render_to_response("dooms/doom_sell_refused.html", {"types":DoomTransferDetail,"message_type":DoomTransferDetail.DOES_NOT_EXIST_OR_ALREADY_ON_SALE}, RequestContext(request))
    
    if doom.status!=Doom.PENDING: #only allowed to buy PENDING dooms
        if request.method == "POST":
            return HttpResponse(simplejson.dumps({"transfer" : DoomTransfer.TRANSACTION_FAILED,"message_type":DoomTransferDetail.FORBIDDEN_STATUS,"doom":doom.serilize()}),'application/javascript')
        else:
            return render_to_response("dooms/doom_sell_refused.html",{"message_type":DoomTransferDetail.FORBIDDEN_STATUS}, RequestContext(request))
        
    if request.method == "POST":         
        doom_change_form = DoomChangeForm(request.POST,instance=doom)
        if doom_change_form.is_valid():            
            doom = doom_change_form.save(commit=False)
            doom.on_sale = True
            doom.save()   
            guarantee_transaction = Transaction.objects.get(doom=doom,user_paid=settings.doume)

            message_doom = MessageTransactionWallPost.objects.create(message=u"Selling doom " + unicode(doom) + u" for " + unicode(doom.price) + u"(potential win:" + unicode(doom.potential_win) + u")",transaction=guarantee_transaction)
            wallpost = create_wall_post(group=None,user=None,owner=doom.owner,content_object=message_doom,category=2) #2=selling            
            #publish salesman's channel (ie to all payers contacts, I am selling a doom!)            
            publish(u"/"+request.user.username+u"/friends",wallpost_doom_notification(wallpost,NotifTypes.DOOM_NEW_SALE))
            publish(u"/"+request.user.username+u"/wall",wallpost_doom_notification(wallpost,NotifTypes.DOOM_NEW_SALE))
            publish(u"/dooms/"+str(doom.id),wallpost_doom_notification(wallpost,NotifTypes.DOOM_NEW_SALE)) 
            notify_categories(doom,doom_notification(doom,NotifTypes.DOOM_NEW_SALE))            
            
            context.update({
                            "type":"success",
                            "doom":doom.serilize(),
                            "transfer":DoomTransfer.TRANSACTION_COMPLETE,
                            })
        else:
            context.update({
                            "type":"error",
                            "description" : "invalid form",
                            "transfer":DoomTransfer.TRANSACTION_FAILED,
                            "message_type":DoomTransferDetail.INVALID_DATA,
                            "doomid":doom.id,
                            "form_errors":doom_change_form._errors
                            })
        
        return HttpResponse(simplejson.dumps(context),'application/javascript')
    else:
        doom_change_form = DoomChangeForm(instance=doom)
        if request.GET.get("page",False):
            return render_to_response("dooms/doom_sell_page.html", {
                "doom_change_form" : doom_change_form,
                "doomid" : doom.id
                },
                RequestContext(request))
        else:
            return render_to_response(template_name, {
                "doom_change_form" : doom_change_form,
                "doomid" : doom.id
                },
                RequestContext(request))    
        
   
def insert_and(logic_string):
    if logic_string!= "":
        return logic_string+" & "
    else:
        return logic_string

    
@login_required
def general_create(request,template_name="dooms/doom_create_layout.html"):
    log.info("general_create")
    res,val = get_context_general_create_ajax(request)

    if val: #true if context
        return render_to_response(template_name, res, RequestContext(request))
    else: #response
        return res
  

def all_sales(request,template_name="dooms/all_sales.html"):
    dooms = Doom.objects.filter(is_active=True,status=Doom.PENDING,on_sale=True)
   
    return render_to_response(template_name, {
        "dooms" : dooms,
        "me" : request.user
        },
        RequestContext(request))

