from django.conf import settings
from dooms.models import Doom
from dooms.competitions.models import Competition

from haystack.query import SearchQuerySet
from haystack.inputs import AutoQuery
from categories.models import Category
from interface.utils import interface_for_user

def tlv(request):
    limit_competitions = interface_for_user(request.user).live_scores_limit
    
    
    doom_search_queryset = SearchQuerySet().all().models(Doom).filter(on_sale=1).order_by('-modified')

    limit_dooms = interface_for_user(request.user).popular_dooms_limit_frontpage
    
    context = dict()
    comp_lists = []
    
    try:
        category = Category.objects.get(id=1)
        categories = category.get_root().get_siblings(include_self=True)    
    
        for cat in categories:
            comp_results = SearchQuerySet().filter(content=AutoQuery(cat.get_tags())).models(Competition).filter(frozen=0,finished=0).order_by('-freeze_time')[:limit_competitions]
            
            comps_category = []
        
            for comp_result in comp_results:  
                if comp_result.object:
                    comps_category.append(comp_result.object)
                    
            comp_lists.append({cat:comps_category})
    except Category.DoesNotExist: #maybe there are no categories defined
        comp_results = SearchQuerySet().models(Competition).filter(frozen=0,finished=0).order_by('-freeze_time')[:limit_competitions]        
        
        cat_name = getattr(settings,"DEFAULT_CATEGORY_NAME_WHEN_NO_CATEGORIES_DEFINED","All categories")
        comp_lists = [{cat_name : [comp_result.object for comp_result in comp_results]}]
        context.update({"flags" : "no_categories_defined" })
    
    context.update(
                   {
                    "comp_lists": comp_lists,
                    "popular_dooms": doom_search_queryset[:limit_dooms]
                    }
                   )
    
    

        
    return context
