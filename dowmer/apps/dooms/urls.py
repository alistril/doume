from django.conf.urls.defaults import *


urlpatterns = patterns("",
    url(r"^(?P<doomid>\d+)/$", "dooms.views.preview", name="dooms_preview"),
    url(r"^buy/(?P<doomid>\d+)/$", "dooms.views.buy", name="dooms_buy"),
    url(r"^sell/(?P<doomid>\d+)/$", "dooms.views.sell", name="dooms_sell"),
    url(r"^create/$", "dooms.views.general_create",name="dooms_general_create"),
    url(r"^all_sales/$", "dooms.views.all_sales",name="dooms_all_sales"),
)
