from django.utils import simplejson
from dajaxice.decorators import dajaxice_register
from dajax.core import Dajax

from dooms.models import Doom
from dooms.helpers import *

from django.template.loader import render_to_string
from django.template import RequestContext

from bank.models import Account
import logging
from miscutils import js_context
from dooms.helpers import get_prefered_dooms_and_friendships
from wall.helpers import create_wall_post
from pubsub.utils import publish

doom_log = logging.getLogger(__name__)


@dajaxice_register
def preview_doom(request,doomid):
    doom_log.debug("AJAX: preview_doom")    
    
    context = {}
    try:        
        doom = Doom.objects.get(pk=doomid)
        doom_log.debug("AJAX: doom="+str(doom))
        html = render_to_string("dooms/doom_preview.html",{"object" : doom})    
        
        doom_log.debug("AJAX: preview_doom success")
        context.update({"type": "success"})
        context.update({"html": html})            
    except Doom.DoesNotExist:
        context.update({"type": "failed"})
     
    return simplejson.dumps(context)

@dajaxice_register
def preview_doom_victory(request,doomid):
    doom_log.debug("AJAX: preview_doom_victory")    
    
    context = {}
    try:        
        doom = Doom.objects.get(pk=doomid)
        doom_log.debug("AJAX: doom="+str(doom))
        html = render_to_string("dooms/doom_victory_preview.html",{"object" : doom})    
        
        doom_log.debug("AJAX: doom_victory_preview.html success")
        context.update({"type": "success"})
        context.update({"html": html})            
    except Doom.DoesNotExist:
        context.update({"type": "failed"})
     
    return simplejson.dumps(context)

    
@dajaxice_register
def prefered_dooms(request): 
    doom_log.debug("ajax: prefered_dooms")
    dooms,frienships = get_prefered_dooms_and_friendships(request)
    limit = request.user.get_profile().interface.prefered_dooms_limit
    doom_html = render_to_string("dooms/list_dooms.html",{"doom_list":dooms[:limit]})
    return doom_html

@dajaxice_register
def unsold_dooms(request): 
    doom_log.debug("ajax: unsold_dooms")
    dooms = Doom.objects.filter(owner=request.user,on_sale=False,is_active=True,status=Doom.PENDING)
    limit = request.user.get_profile().interface.unsold_dooms_limit    
    doom_html = render_to_string("dooms/list_dooms.html",{"doom_list":dooms[:limit]})
    
    return doom_html
