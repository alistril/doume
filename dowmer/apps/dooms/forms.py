from django import forms
from django.forms import ModelForm

from dooms.models import Doom
from dooms.competitions.models import Competition
from django.utils.translation import ugettext_lazy as _, ugettext
from django.conf import settings

from django.db.models import Q
from haystack.forms import SearchForm
from ajax_select.fields import AutoCompleteSelectMultipleField, AutoCompleteSelectField
from ajax_select import make_ajax_field
import logging
from django.core.exceptions import ValidationError
from luthor.lexing import Lexer
from luthor.parsing import Parser
from luthor.interpreting import Interpreter
from luthor.errors import LuthorException
from luthor.definitions import Constant 
from dooms.logic import link_groups,link_nodes
from dooms.competitions.forms import competition_form_dict

doomformslogger = logging.getLogger(__name__)
   
        
class DoomForm(forms.ModelForm): 
    competition = AutoCompleteSelectMultipleField('select_competition', required=False)
    
    price = forms.IntegerField(_("price"))
    potential_win = forms.IntegerField(_("potential win"))
    description = forms.CharField(_("description"))
    
    def __init__(self, *args, **kwargs):        
        self.owner = kwargs.pop("owner", None) 
        self.check_subforms = kwargs.pop("check_subforms", True)
        if self.owner == None:
            doomformslogger.error("No owner specified for form.")
            
        super(DoomForm, self).__init__(*args, **kwargs)

    def clean(self):        
        universe = {
                    "#link_groups#" : link_groups,
                    "#link_nodes#" : link_nodes,
                    "#constant#" : Constant()
                    }
        if "price" in self.cleaned_data and "potential_win" in self.cleaned_data:
            price_val = self.cleaned_data["price"]
            potential_win_val = self.cleaned_data["potential_win"]
            if price_val < getattr(settings,"MIN_DOOM_PRICE") or price_val > getattr(settings,"MAX_DOOM_PRICE"):
                raise forms.ValidationError(u"Doom prices must be between %d and %d credits" % (getattr(settings,"MIN_DOOM_PRICE"),getattr(settings,"MAX_DOOM_PRICE")))
            if potential_win_val <= price_val:
                raise forms.ValidationError(u"A bigger price than potential winnings does not make any sense.")
            
        competition_ids = self.cleaned_data["competition"]
        all_finished = True

        for competition_id in competition_ids:                
            try:
                competition = Competition.objects.get(pk=competition_id,reference=None)
            except Competition.DoesNotExist:                
                raise ValidationError(u"One of the competitions or contests you selected does not exist")
            all_finished = all_finished and competition.finished
            form_class = competition_form_dict[competition.competition_type.model]

            comp_form = form_class(self.data,prefix=competition.id,instance=competition.competition_object)
            if comp_form.is_valid() or (not self.check_subforms):
                universe.update(
                    {
                        competition.id_string : competition.competition_object.universe()
                    }
                )
            else:
                doomformslogger.error("Invalid competition: %s" % competition)                
                raise forms.ValidationError("You didn't configure the following competition/contest correctly: %s" % competition)
        if all_finished==True:
            if len(competition_ids)>0:
                raise forms.ValidationError(u"All of the competitions or contests you selected are already finished")
            else:
                raise forms.ValidationError(u"Your doom does not contain any competitions")
        else:
            self.cleaned_data["combination_logic"] = self.cleaned_data["combination_logic"].upper()
            lexer = Lexer()
            lexer.build()
            parser = Parser(lexer=lexer)
            parser.build()
            tree=None
            speech = None
            try:
                tree = parser.parse(self.cleaned_data["combination_logic"])
                inter = Interpreter(tree)
                self.inter = inter #to access interpreter from helpers (above) to see if grouping is successful
                value = inter.execute(universe)
                #check if all selected competitions are contained in logic:                               
                unused_competitions = [key for key in set(universe.keys()) - set(tree.map.keys()) if key[0] != '#' and key[-1] != '#']
                print set(universe.keys())
                print tree.map.keys()
                print set(universe.keys()) - set(tree.map.keys())
                if len(unused_competitions)>0:                    
                    raise forms.ValidationError("You didn't configure the following competition/contest(s) correctly: %s" % ",".join(unused_competitions))
                if inter.grouping_successful:
                    self.cleaned_data["speech"] = tree.speech
                    self.tree = tree
                else:
                    self.cleaned_data["speech"] = self.cleaned_data["combination_logic"]
                    self.tree = tree
            except LuthorException as e:
                doomformslogger.warning(e.value)
                doomformslogger.warning(universe)            
                raise forms.ValidationError(e.value)
            
        return self.cleaned_data

    def save(self, commit=True,force_insert=False, force_update=False):        
        print "save 1"
        if commit:
            print "save 1.5a"
            doom = super(DoomForm, self).save(commit=False)
            print "save 1.6a"
        else:
            print "save 1.5b"         
            doom = Doom(owner=self.owner,status=Doom.PENDING)
         
            print "save 1.6b"
        doom.owner = self.owner
        doom.is_active = False
        print "save 2"
        if doom.guarantor == None:
            doom.guarantor = self.owner #this better be a doom creation otherwise some poor guy will guarantee a dooms potential win
        
        if self.tree is not None: #if tree is not none then there is something in cleaned_data[speech]
            print "save 3a"            
            doom.speech = self.cleaned_data["speech"]
                            
        print "save 4"
        if commit:
            doom.save()
            
            competition_ids = self.cleaned_data["competition"]
            print competition_ids
            for competition_id in competition_ids:
                competition = Competition.objects.get(pk=competition_id)
                doom.competition_set.add(competition)
                print "adding", competition
            
            doom.set_id_string()
        print "save 5"
        return doom
        
    class Meta:
        model = Doom        
        fields = ('competition','price', 'potential_win','description','combination_logic')
        
class DoomChangeForm(forms.ModelForm): 

    class Meta:
        model = Doom
        fields = ('price', 'description')
    
class ChangeCompetitionForm(forms.Form):
    competition = forms.ChoiceField(label=_("Competition type"),choices=((u"soccercompetition" , u"Soccer"),(u"tenniscompetition" , u"Tennis"))) 
