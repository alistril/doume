from luthor.errors import SemanticException,RenderException
from luthor.tree import Node,NodeBELONGS,NodeIDENTIFIER,NodeEQUAL,NodeGREATER_THAN_OR_EQUAL,NodeGREATER_THAN,NodeLESS_THAN_OR_EQUAL,NodeLESS_THAN,NodeNUMBER,NodeSTRING,NodeAND,NodeOR,NodeBoolOp
from django.template.loader import render_to_string

def link_groups(node,groups):
    return render_to_string("dooms/competitions/link_groups.html",{"node":node, "groups":groups})
    
def link_nodes(master_node,slave_nodes):
    return render_to_string("dooms/competitions/link_nodes.html",{"master_node":master_node,"slave_nodes":slave_nodes})
