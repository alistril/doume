from django.db import models
from django.contrib.contenttypes import generic
from dooms.models import Doom

from interactive_notification.models import InteractiveNotification
from wall.models import WallPost

class DoomStatusChangedNotification(models.Model):
    notification = generic.GenericRelation(InteractiveNotification)
    doom = models.ForeignKey(Doom,null=True)
    wallpost = models.ForeignKey(WallPost,null=True)
    
    def serilize(self):
        context = {}
        if self.wallpost is not None and self.wallpost.content_object is not None:
            context.update({u"transaction" : self.wallpost.content_object.transaction.serilize()})
        if self.doom!=None:
            context.update({u"doom" : self.doom.serilize()})            
        if self.wallpost!=None:
            context.update({u"wallpost" : self.wallpost.serilize()})
        return context
    class Meta:        
        app_label = 'dooms'