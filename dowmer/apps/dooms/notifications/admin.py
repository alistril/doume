from django.contrib import admin
from dooms.notifications.models import DoomStatusChangedNotification

admin.site.register(DoomStatusChangedNotification)