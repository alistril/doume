from dooms.competitions.tennis.models import TennisCompetition
from django.contrib import admin

admin.site.register(TennisCompetition)