from dooms.competitions.two_sides_with_scores.models import TwoSidesWithScores
from dooms.competitions.tennis.logic import TennisCompetitionLogic

        
class TennisCompetition(TwoSidesWithScores):    
    def universe(self):                
        univ = super(TennisCompetition,self).universe()
        univ.update({"#object#" : TennisCompetitionLogic()})
        return univ

    class Meta:
        app_label = 'dooms'