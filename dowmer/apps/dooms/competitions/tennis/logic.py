from luthor.errors import SemanticException,RenderException
from luthor.tree import Node,NodeBELONGS,NodeIDENTIFIER,NodeEQUAL,NodeGREATER_THAN_OR_EQUAL,NodeGREATER_THAN,NodeLESS_THAN_OR_EQUAL,NodeLESS_THAN,NodeNUMBER,NodeSTRING,NodeAND,NodeOR,NodeBoolOp
from django.template.loader import render_to_string
from dooms.competitions.two_sides_with_scores.logic import TwoSidesWithScoresLogic

class TennisCompetitionLogic(TwoSidesWithScoresLogic):
    pass