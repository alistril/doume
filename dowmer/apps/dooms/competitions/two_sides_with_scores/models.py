from dooms.competitions.models import SpecializedCompetition
from dooms.competitions.helpers import process_id_string,process_member
from django.db import models
from django.utils.translation import ugettext_lazy as _, ugettext

class TwoSidesWithScores(SpecializedCompetition):
    side1 = models.CharField(_("side1"),max_length=255,null=True,blank=True)
    side2 = models.CharField(_("side2"),max_length=255,null=True,blank=True)      
    
    score1 = models.IntegerField(_("score1"),null=True, blank=True)
    score2 = models.IntegerField(_("score2"),null=True, blank=True)
    
    def universe(self):
        return {
                    "#name#" : "%s vs %s" % (self.side1, self.side2),
                    "#type#" : "competition",                    
                    process_member(str(self.side1)) : {
                        "#name#" : str(self.side1),
                        "#type#" : "side",                        
                        "SCORE" : {
                                   "#value#" : self.score1,
                                   "#type#" : "NUMBER",
                                   }
                        },
                    process_member(str(self.side2)) : {
                        "#name#" : str(self.side2),
                        "#type#" : "side",                        
                        "SCORE" : {
                                   "#value#" : self.score2,
                                   "#type#" : "NUMBER",
                                   }
                        },
                }
                
    def generate_id_string(self):
        return process_id_string(str(self.side1)) + process_id_string(str(self.side2))
        
    def copy_basic_fields(self):
        return TwoSidesWithScores(
            side1 = self.side1,
            side2 = self.side2
        )
        
    def __unicode__(self):
        return self.side1 + " vs " + self.side2
    
    def __eq__(self, other):
        if other is None:
            return False
        return self.score1==other.score1 and self.score2==other.score2

    class Meta:
        abstract = True
        app_label = 'dooms'