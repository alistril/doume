from django import forms

class TwoSidesWithScoresForm(forms.Form):
    score = forms.CharField(required=False,max_length=10,widget=forms.HiddenInput)
    other_score = forms.CharField(required=False,max_length=10)
    def __init__(self, *args, **kwargs):
        self.instance = kwargs.pop('instance',None)
        super(TwoSidesWithScoresForm,self).__init__(*args, **kwargs)
        CHOICES = (('1', self.instance.side1,), ('2', self.instance.side2,),('3', 'Tie',),)
        self.fields['side_choices'] = forms.ChoiceField(widget=forms.RadioSelect, choices=CHOICES,required=False)

        
    def clean(self):        
        if len(self.cleaned_data)>0:
            #if the user chooses a side he/she doesn't have to fill the score
            if not u'side_choices' in self.cleaned_data:
                if u'score' in self.cleaned_data:                 
                    score = self.cleaned_data[u"score"]
                    if score is None:
                        raise forms.ValidationError(u"You didn't specify either score or outcome")
                    score_split = score.strip().split('-')
                    if len(score_split)>1:
                        try:
                            self.score1 = int(score_split[0])
                            self.score2 = int(score_split[1])
                        except ValueError:
                            raise forms.ValidationError(u"Invalid score format. One of your score is not a number") 
                    else:
                        raise forms.ValidationError(u"Invalid score format. Score must be of the type xx - xx")
        else:
            raise forms.ValidationError(u"Unknown error.")
        
        return self.cleaned_data
    
    