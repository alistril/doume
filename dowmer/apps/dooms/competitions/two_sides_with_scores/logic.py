from luthor.errors import SemanticException,RenderException
from luthor.tree import Node,NodeBELONGS,NodeIDENTIFIER,NodeEQUAL,NodeGREATER_THAN_OR_EQUAL,NodeGREATER_THAN,NodeLESS_THAN_OR_EQUAL,NodeLESS_THAN,NodeNUMBER,NodeSTRING,NodeAND,NodeOR,NodeBoolOp
from django.template.loader import render_to_string

class TwoSidesWithScoresLogic(object):
    def luthor_equals(self,side1,side2):        
        if isinstance(side1,NodeBELONGS) and isinstance(side2,NodeBELONGS):
            if side1.master().element_name!=side2.master().element_name:
                raise RenderException(RenderException.COULD_NOT_GROUP,"could not resolve equality \\nstatement between two different objects\\n %s and %s" % (side1.selection().master().element_name,side2.selection().master().element_name))
                return "could not resolve equality \\nstatement between two different objects\\n %s and %s" % (side1.selection().master().element_name,side2.selection().master().element_name)
            else:
                if side1.data_type=="NUMBER" and side1.data_type=="NUMBER":
                    return "tie between %s and %s" % (side1.selection().master().element_name,side2.selection().master().element_name)
                else:
                    raise RenderException(RenderException.OTHER,"invalid types")
        elif isinstance(side1,NodeBELONGS) and isinstance(side2,NodeNUMBER):
            return "score of %s = %d" % (side1.selection().master().element_name,side2.leaf)
        elif isinstance(side1,NodeNUMBER) and isinstance(side2,NodeBELONGS):
            return "score of %s = %d" % (side2.selection().master().element_name,side1.leaf)
        else:
            raise RenderException(RenderException.OTHER,"invalid types")
    
    def luthor_less_than(self,side1,side2):        
        if isinstance(side1,NodeBELONGS) and isinstance(side2,NodeBELONGS):
            if side1.master().element_name!=side2.master().element_name:
                raise RenderException(RenderException.COULD_NOT_GROUP,"could not resolve comparison \\nstatement between two different objects\\n %s and %s" % (side1.selection().master().element_name,side2.selection().master().element_name))
                return "could not resolve comparison \\nstatement between two different objects\\n %s and %s" % (side1.selection().master().element_name,side2.selection().master().element_name)
            else:
                if side1.data_type=="NUMBER" and side1.data_type=="NUMBER":
                    return "%s loses against %s" % (side1.selection().master().element_name,side2.selection().master().element_name)
                elif side1.data_type=="STRING" and side1.data_type=="STRING":
                    raise RenderException(RenderException.OTHER,"invalid comparison")
                    
        elif isinstance(side1,NodeBELONGS) and isinstance(side2,NodeNUMBER):
            return "score of %s < %d" % (side1.selection().master().element_name,side2.leaf)
        elif isinstance(side1,NodeNUMBER) and isinstance(side2,NodeBELONGS):
            return "score of %s < %d" % (side2.selection().master().element_name,side1.leaf)        
        else:
            raise RenderException(RenderException.OTHER,"invalid types")
            
    def luthor_less_than_or_equal(self,side1,side2):        
        if isinstance(side1,NodeBELONGS) and isinstance(side2,NodeBELONGS):
            if side1.master().element_name!=side2.master().element_name:
                raise RenderException(RenderException.COULD_NOT_GROUP,"could not resolve comparison \\nstatement between two different objects\\n %s and %s" % (side1.selection().master().element_name,side2.selection().master().element_name))
                return "could not resolve comparison \\nstatement between two different objects\\n %s and %s" % (side1.selection().master().element_name,side2.selection().master().element_name)
            else:
                if side1.data_type=="NUMBER" and side1.data_type=="NUMBER":
                    return "%s loses or at most equalizes against %s" % (side1.selection().master().element_name,side2.selection().master().element_name)
                elif side1.data_type=="STRING" and side1.data_type=="STRING":
                    raise RenderException(RenderException.OTHER,"invalid comparison")
                    
        elif isinstance(side1,NodeBELONGS) and isinstance(side2,NodeNUMBER):
            return "score of %s <= %d" % (side1.selection().master().element_name,side2.leaf)
        elif isinstance(side1,NodeNUMBER) and isinstance(side2,NodeBELONGS):
            return "score of %s <= %d" % (side2.selection().master().element_name,side1.leaf)        
        else:
            raise RenderException(RenderException.OTHER,"invalid types")
            
    def luthor_greater_than(self,side1,side2):        
        if isinstance(side1,NodeBELONGS) and isinstance(side2,NodeBELONGS):
            if side1.master().element_name!=side2.master().element_name:
                raise RenderException(RenderException.COULD_NOT_GROUP,"could not resolve comparison \\nstatement between two different objects\\n %s and %s" % (side1.selection().master().element_name,side2.selection().master().element_name))
                return "could not resolve comparison \\nstatement between two different objects\\n %s and %s" % (side1.selection().master().element_name,side2.selection().master().element_name)
            else:
                if side1.data_type=="NUMBER" and side1.data_type=="NUMBER":
                    return "%s wins against %s" % (side1.selection().master().element_name,side2.selection().master().element_name)
                elif side1.data_type=="STRING" and side1.data_type=="STRING":
                    raise RenderException(RenderException.OTHER,"invalid comparison")
                    
        elif isinstance(side1,NodeBELONGS) and isinstance(side2,NodeNUMBER):
            return "score of %s > %d" % (side1.selection().master().element_name,side2.leaf)
        elif isinstance(side1,NodeNUMBER) and isinstance(side2,NodeBELONGS):
            return "score of %s > %d" % (side2.selection().master().element_name,side1.leaf)        
        else:
            raise RenderException(RenderException.OTHER,"invalid types")
            
    def luthor_greater_than_or_equal(self,side1,side2):        
        if isinstance(side1,NodeBELONGS) and isinstance(side2,NodeBELONGS):
            if side1.master().element_name!=side2.master().element_name:
                raise RenderException(RenderException.COULD_NOT_GROUP,"could not resolve comparison \\nstatement between two different objects\\n %s and %s" % (side1.selection().master().element_name,side2.selection().master().element_name))
                return "could not resolve comparison \\nstatement between two different objects\\n %s and %s" % (side1.selection().master().element_name,side2.selection().master().element_name)
            else:
                if side1.data_type=="NUMBER" and side1.data_type=="NUMBER":
                    return "%s wins or at least equalizes against %s" % (side1.selection().master().element_name,side2.selection().master().element_name)
                elif side1.data_type=="STRING" and side1.data_type=="STRING":
                    raise RenderException(RenderException.OTHER,"invalid comparison")
                    
        elif isinstance(side1,NodeBELONGS) and isinstance(side2,NodeNUMBER):
            return "score of %s >= %d" % (side1.selection().master().element_name,side2.leaf)
        elif isinstance(side1,NodeNUMBER) and isinstance(side2,NodeBELONGS):
            return "score of %s >= %d" % (side2.selection().master().element_name,side1.leaf)        
        else:
            raise RenderException(RenderException.OTHER,"invalid types")     
            
    def luthor_render_group(self,node,group):        
        return render_to_string("dooms/competitions/classic/luthor_render_group.html",{"group": group, "node":node})