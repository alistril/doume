from django.contrib.contenttypes import generic
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.conf import settings

from django.utils.translation import ugettext_lazy as _, ugettext
from dooms.models import Doom

from django.db.models.signals import post_save
from django.dispatch import receiver
from tagging.fields import TagField
from tagging.models import Tag
from interactive_notification.models import SystemNoticeNotification
from wall.helpers import create_wall_post
from pubsub.utils import publish
from wall.models import WallPost
from django.db import transaction
from errors.models import Error
from luthor.errors import LuthorException

from decimal import Decimal

import logging
log = logging.getLogger(__name__)

class Competition(models.Model):
    id_string = models.CharField(_("id_string"),max_length=15,null=True,blank=True)
    description = models.CharField(_("description"),max_length=255,null=True)
    
    start_time = models.DateTimeField(_("start_time"),null=True,blank=True)
    end_time = models.DateTimeField(_("end_time"),null=True,blank=True)
    
    finished = models.BooleanField(_("finished"))
    reference = models.ForeignKey("self",null=True,blank=True)
    #doom = models.ForeignKey(Doom,null=True,blank=True)
    dooms = models.ManyToManyField(Doom,blank=True)
    
    frozen = models.BooleanField(_("frozen"), default=False)
    freeze_time = models.DateTimeField(_("freeze_time"),null=True)
    
    competition_type = models.ForeignKey(ContentType)
    competition_id = models.PositiveIntegerField()
    competition_object = generic.GenericForeignKey('competition_type', 'competition_id')
    tags = TagField()
        
    def __unicode__(self):
        if self.competition_object is None:
            return "[Anomaly]"
        else:
            return self.competition_object.__unicode__()
        
    def __eq__(self, other):
        if other is None:
            return False;
        return self.competition_object==other.competition_object

    class Meta:
        app_label = 'dooms'

@receiver(post_save, sender=Competition)        
def set_id_string_competition(sender, instance, created=False,raw=False,**kwargs):
    if created and not raw:
        instance.id_string = instance.competition_object.generate_id_string() + (hex(instance.id)[2:]).upper()
        instance.save()

@receiver(post_save, sender=Competition,dispatch_uid="competition_handle_victories")
@transaction.commit_manually
def handle_victories(sender, instance, created=False,raw=False,**kwargs):    
    if created or raw:
        return
    log.info("handle_victories")
    from im.utils import generic_notification,NotifTypes
    from dooms.notifications.models import DoomStatusChangedNotification
    from payments.models import MessageTransactionWallPost,Transaction, SubTransaction
    
    transaction.commit() #this typically saves compeition finished since its a post_save signal
    dooms = instance.dooms.filter(is_active=True,status=Doom.PENDING)
    for doom in dooms:
        doom.update_status()
        
    dooms = instance.dooms.filter(is_active=True,status=Doom.FROZEN)
    try:
        log.info("dooms %r" % dooms)
        for doom in dooms:
            try:                
                doom.update_status()                
                if doom.status==Doom.WON:
                    #find corresponding guarantee and then update...
                    #now find transaction that shows the guarantor doing the guarantee, if such transaction exists
                    credit_transactions = Transaction.objects.filter(user_paid=settings.doume,doom=doom)
                    if doom.guarantor:
                        credit_transactions = credit_transactions.filter(user_paying=doom.guarantor)
                        if credit_transactions.count()==0: #not supported yet. Possible if we can auto-debit a credit card or whatever
                            transaction.rollback()
                            SystemNoticeNotification.objects_with_notification.create(user=doom.owner,type=SystemNoticeNotification.ERROR,content="The winnings for doom %s should be guaranteed by %s but are not.<br/> In other ways, we cannot find your money.<br/> But don't worry we'll find a solution." % (doom.id_string,doom.guarantor.username))                            
                            transaction.commit()
                            doom.is_active = False
                            doom.save()
                            transaction.commit()
                        elif credit_transactions.count()==1: 
                            credit_transaction = credit_transactions[0]
                            if doom.owner == doom.guarantor:
                                credit_transaction.is_active = False
                            else:
                                credit_transaction.status = Transaction.PAYMENT_SUCCESS_IMPLICIT
                                credit_transaction.user_paid = doom.owner                
                                credit_transaction.save(force_update=True)
                            #unguarantee
                            #doume debits the money and doom owner is credited
                            SubTransaction.objects.create(user=credit_transaction.doom.owner,debit=False,amount=Decimal(credit_transaction.doom.potential_win),parent=credit_transaction)                            
                            SubTransaction.objects.create(user=settings.doume,debit=True,amount=Decimal(credit_transaction.doom.potential_win),parent=credit_transaction)
                            
                            message_doom = MessageTransactionWallPost.objects.create(message=u"Doom " + unicode(doom) + u" is a winner! Credits won:" + unicode(doom.potential_win),transaction=credit_transaction)
                            wallpost = create_wall_post(group=None,user=None,owner=doom.owner,content_object=message_doom,category=WallPost.WINNER)
                            
                            doom_win_notif_object = DoomStatusChangedNotification.objects.create(doom=doom,wallpost=wallpost)
                            doom_win_notif = doom_win_notif_object.notification.create(reciever=doom.owner)
                            publish(u"/"+doom.owner.username+u"/friends",generic_notification(doom_win_notif,NotifTypes.DOOM_STATUS))
                            publish(u"/"+doom.owner.username+u"/wall",generic_notification(doom_win_notif,NotifTypes.DOOM_STATUS))
                            publish(u"/dooms/"+str(doom.id),generic_notification(doom_win_notif,NotifTypes.DOOM_STATUS))
                        else:
                            log.error(ugettext(u"Combination user_paying=%(guarantor)s,user_paid=doume,doom=%(doom)s should return at most one result, returned multiple.") % {
                                                    "guarantor": doom.guarantor,
                                                    "doom":doom
                                                }
                                            )            
                    else:
                        transaction.rollback()
                        SystemNoticeNotification.objects_with_notification.create(user=doom.owner,type=SystemNoticeNotification.ERROR,content="Cannot find the money backing the doom %s.<br/> No one is listed. But don't worry, we are on it!" % (doom.id_string))
                        transaction.commit()
                        doom.is_active = False
                        doom.save()
                        transaction.commit()
                elif doom.status==Doom.LOST:            
                    credit_transactions = Transaction.objects.filter(user_paid=settings.doume,doom=doom)
                    if doom.guarantor:
                        credit_transactions = credit_transactions.filter(user_paying=doom.guarantor)
                        if credit_transactions.count()==0: #not supported yet. Possible if we can auto-debit a credit card or whatever
                            transaction.rollback()
                            SystemNoticeNotification.objects_with_notification.create(user=doom.owner,type=SystemNoticeNotification.ERROR,content="The winnings for doom %s should be guaranteed by %s but are not.<br/> In other ways, we cannot find your money.<br/> But don't worry we'll find a solution." % (doom.id_string,doom.guarantor.username))                            
                            transaction.commit()
                            doom.is_active = False
                            doom.save()
                            transaction.commit()
                        elif credit_transactions.count()==1: 
                            credit_transaction = credit_transactions[0]
                            #delete transaction and all its subtransactions
                            credit_transaction.is_active = False
                            credit_transaction.save()
                            #unguarantee
                            #doume debits the money and doom guarantor is credited
                            SubTransaction.objects.create(user=credit_transaction.doom.guarantor,debit=False,amount=Decimal(credit_transaction.doom.potential_win),parent=credit_transaction)                            
                            SubTransaction.objects.create(user=settings.doume,debit=True,amount=Decimal(credit_transaction.doom.potential_win),parent=credit_transaction)
                            
                            doom_win_notif_object = DoomStatusChangedNotification.objects.create(doom=doom)
                            doom_win_notif = doom_win_notif_object.notification.create(reciever=doom.owner)
                            
                            publish(u"/"+doom.owner.username+u"/friends",generic_notification(doom_win_notif,NotifTypes.DOOM_STATUS))
                            publish(u"/"+doom.owner.username+u"/wall",generic_notification(doom_win_notif,NotifTypes.DOOM_STATUS))
                            publish(u"/dooms/"+str(doom.id),generic_notification(doom_win_notif,NotifTypes.DOOM_STATUS)) 
                        else:
                            transaction.rollback()
                            SystemNoticeNotification.objects_with_notification.create(user=doom.owner,type=SystemNoticeNotification.ERROR,content=ugettext(u"Combination user_paying=%(guarantor)s,user_paid=doume,doom=%(doom)s should return at most one result,<br/> returned multiple. Cannot transfer your money.<br/> We'll try to solve that shortly.") % {
                                                    "guarantor": doom.guarantor,
                                                    "doom":doom.id_string
                                                } % (doom.id_string,doom.guarantor.username))                            
                            transaction.commit()
                            doom.is_active = False
                            doom.save()
                            transaction.commit()
                    else:
                        transaction.rollback()
                        SystemNoticeNotification.objects_with_notification.create(user=doom.owner,type=SystemNoticeNotification.ERROR,content="Cannot find the money backing the doom %s. No one is listed. But don't worry, we are on it!" % (doom.id_string))                        
                        transaction.commit()
                        doom.is_active = False
                        doom.save()
                        transaction.commit()
            except LuthorException as e:
                transaction.rollback()
                SystemNoticeNotification.objects_with_notification.create(user=doom.owner,type=SystemNoticeNotification.ERROR,content="Cannot compile logic for doom %s.<br/> Error is: %s.<br/> In other words, the system cannot decide if the doom has won or lost.<br/> Logic is:<br/> %s.<br/> We'll address this issue as soon as we can." % (doom.id_string,e.value,doom.combination_logic))
                transaction.commit()
                doom.is_active = False
                doom.save()
                transaction.commit()
            except Exception as inst:
                transaction.rollback()
                SystemNoticeNotification.objects_with_notification.create(user=doom.owner,type=SystemNoticeNotification.ERROR,content="Error processing status of your doom %s.<br/> The system failed to decide if it has won or lost.<br/> We will try to resolve this issue as soon as possible. Error was %r" % (doom.id_string,inst))
                transaction.commit()
                doom.is_active = False
                doom.save()
                transaction.commit()
    except Exception as inst:
        transaction.rollback()        
        log.error("Competition %s has been saved (finish=%r,id=%d)).<br/> Exception was: %r" % (instance,instance.finished,instance.id,inst))
        Error.objects.create(message="Competition %s has been saved (finish=%r,id=%d))." % (instance,instance.finished,instance.id),exception=str(inst))        
        transaction.commit()
        raise
    else:
        transaction.commit()
    
class SpecializedCompetition(models.Model):
    parent = generic.GenericRelation(Competition,content_type_field='competition_type', object_id_field='competition_id')

    class Meta:
        abstract = True
        app_label = 'dooms'
