from dooms.competitions.models import Competition
from django.contrib import admin

admin.site.register(Competition)