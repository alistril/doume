from dooms.competitions.soccer.forms import DoomCreateSoccerForm
from dooms.competitions.tennis.forms import DoomCreateTennisForm

competition_form_dict = {u"soccercompetition" : DoomCreateSoccerForm, u"tenniscompetition" : DoomCreateTennisForm }