import re

def process_id_string(data):
    valid_strings_list = re.findall(r'[a-zA-Z]+[a-zA-Z0-9]*', data)
    return max(valid_strings_list, key=len)[:3].upper()
    
def process_member(data):
    valid_strings_list = re.findall(r'[a-zA-Z]+[a-zA-Z0-9]*', data)
    return (''.join(valid_strings_list)).upper()