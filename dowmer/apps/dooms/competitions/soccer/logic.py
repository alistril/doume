from luthor.errors import SemanticException,RenderException
from luthor.tree import Node,NodeBELONGS,NodeIDENTIFIER,NodeEQUAL,NodeGREATER_THAN_OR_EQUAL,NodeGREATER_THAN,NodeLESS_THAN_OR_EQUAL,NodeLESS_THAN,NodeNUMBER,NodeSTRING,NodeAND,NodeOR,NodeBoolOp
from dooms.competitions.two_sides_with_scores.logic import TwoSidesWithScoresLogic
from django.template.loader import render_to_string

class SoccerCompetitionLogic(TwoSidesWithScoresLogic):
    def luthor_equals(self,side1,side2):        
        if isinstance(side1,NodeBELONGS) and isinstance(side2,NodeBELONGS):
            if side1.master().element_name!=side2.master().element_name:
                raise RenderException(RenderException.COULD_NOT_GROUP,"could not resolve equality \\nstatement between two different objects\\n %s and %s" % (side1.selection().master().element_name,side2.selection().master().element_name))
                return "could not resolve equality \\nstatement between two different objects\\n %s and %s" % (side1.selection().master().element_name,side2.selection().master().element_name)
            else:
                if side1.data_type=="NUMBER" and side1.data_type=="NUMBER":
                    return "tie between %s and %s" % (side1.selection().master().element_name,side2.selection().master().element_name)
                elif side1.data_type=="STRING" and side1.data_type=="STRING":
                    #raise RenderException("not possible that to teams have the same best player") #this is probably not possible
                    return "%s and %s have the same best player" % (side1.master().element_name,side2.master().element_name)
        elif isinstance(side1,NodeBELONGS) and isinstance(side2,NodeNUMBER):
            return "score of %s = %d" % (side1.selection().master().element_name,side2.leaf)
        elif isinstance(side1,NodeNUMBER) and isinstance(side2,NodeBELONGS):
            return "score of %s = %d" % (side2.selection().master().element_name,side1.leaf)
        elif isinstance(side1,NodeBELONGS) and isinstance(side2,NodeSTRING):
            return "best player of %s is %s" % (side1.master().element_name,side2.leaf)
        elif isinstance(side1,NodeSTRING) and isinstance(side2,NodeBELONGS):
            return "best player of %s is %s" % (side2.master().element_name,side1.leaf)
        else:
            raise RenderException(RenderException.OTHER,"invalid types")
