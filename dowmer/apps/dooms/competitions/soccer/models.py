from dooms.competitions.helpers import process_id_string,process_member
from dooms.competitions.two_sides_with_scores.models import TwoSidesWithScores
from dooms.competitions.soccer.logic import SoccerCompetitionLogic
from django.db import models
from django.utils.translation import ugettext_lazy as _, ugettext

class SoccerCompetition(TwoSidesWithScores):
    PENDING = 1
    HALF_TIME = 2
    FINISHED = 3 
    SCHEDULED = 4
    
    best_player = models.CharField(_("best_player"),max_length=255,null=True,blank=True)
    league = models.CharField(_("league"),max_length=255,null=True,blank=True)
    
    score1_before_penalty = models.IntegerField(_("score1_before_penalty"),null=True,blank=True)    
    score2_before_penalty = models.IntegerField(_("score2_before_penalty"),null=True,blank=True)
    
    score1_before_extra_time = models.IntegerField(_("score1_before_extra_time"),null=True,blank=True)
    score2_before_extra_time = models.IntegerField(_("score2_before_extra_time"),null=True,blank=True)
    
    status = models.IntegerField(_("status"),null=True,blank=True)
    
    #useless, just for fun
    half_time_finished = models.NullBooleanField(_("half_time_finished"),null=True,blank=True)
    score1_half_time = models.IntegerField(_("score1_half_time"),null=True,blank=True)
    score2_half_time = models.IntegerField(_("score1_half_time"),null=True,blank=True)

    def universe(self):
        univ = super(SoccerCompetition,self).universe()
        univ.update(
            {
                "#object#" : SoccerCompetitionLogic(),
                "BEST_PLAYER" : {
                                 "#type#" : "STRING",
                                 "#value#" : self.best_player
                                 }
            }
        )
        return univ
        
    def generate_id_string(self):
        return process_id_string(str(self.side1)) + process_id_string(str(self.side2))
        
    def __unicode__(self):
        sides = self.side1 + u" vs " + self.side2
        if self.best_player:
            bestplayer = u"(Best player: " + self.best_player + u")"
        else:
            bestplayer = ""
        return sides + bestplayer

    class Meta:
        app_label = 'dooms'
        
