from django import forms
from dooms.competitions.two_sides_with_scores.forms import TwoSidesWithScoresForm

class DoomCreateSoccerForm(TwoSidesWithScoresForm):
    best_player = forms.CharField(required=False,max_length=255)
