from dooms.competitions.soccer.models import SoccerCompetition
from django.contrib import admin

admin.site.register(SoccerCompetition)