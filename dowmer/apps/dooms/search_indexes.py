import datetime
from haystack import indexes
from dooms.models import Doom
from dooms.competitions.models import Competition

class CompetitionEntryIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    finished = indexes.IntegerField(model_attr='finished')
    frozen = indexes.IntegerField(model_attr='frozen')
    freeze_time = indexes.DateTimeField(model_attr='freeze_time')
    is_reference = indexes.IntegerField()
    name = indexes.CharField()
    
    def get_model(self):
        return Competition
    
    def prepare_is_reference(self,obj):
        if obj.reference==None:
            return 1
        else:
            return 0
        
    def prepare_frozen(self,obj):
        if obj.frozen==None:
            return 1
        else:
            return 0
        
    def prepare_finished(self,obj):
        if obj.finished==None:
            return 1
        else:
            return 0
            
    def prepare_name(self,obj):
        return obj.competition_object.__unicode__()
        
    def index_queryset(self):
       """
       This is used when the entire index for model is updated, and should only include
       public entries
       """       
       return Competition.objects.filter(reference=None)

class DoomEntryIndex(indexes.RealTimeSearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    is_active = indexes.BooleanField(model_attr='is_active')
    on_sale = indexes.IntegerField()
    owner = indexes.CharField()
    created = indexes.DateTimeField(model_attr='created')
    modified = indexes.DateTimeField(model_attr='modified')
    
    def get_model(self):
        return Doom
    
    def prepare_on_sale(self,obj):                
        if obj.on_sale:
            return 1
        else:
            return 0
        
    def prepare_owner(self,obj): 
        try:       
            return obj.owner
        except:
            return "???"
    
    def index_queryset(self):
       return Doom.objects.filter(is_active=True,status=Doom.PENDING)
