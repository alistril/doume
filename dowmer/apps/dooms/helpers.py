from django.template import RequestContext
from wall.models import WallPost,Message
from django.contrib import messages

from django.core.exceptions import ObjectDoesNotExist
from django.template import RequestContext
from wall.forms import MessageForm
from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response, get_object_or_404
from django.contrib.auth.models import User
from teams.models import Team
from wall.helpers import *
from dooms.models import Doom
from dooms.competitions.models import Competition
from dooms.forms import DoomForm,DoomChangeForm
from dooms.competitions.forms import competition_form_dict
from django.http import HttpResponse, HttpResponseForbidden, HttpResponseRedirect
from django.core.urlresolvers import reverse
from wall.models import WallPost
from django.utils.translation import ugettext_lazy as _, ugettext
from payments.views import pay
from payments.models import *

from pubsub.utils import publish,create_node
from im.utils import *
from friends.models import Friendship
from django.conf import settings
from django.template.loader import render_to_string
from payments.helpers import DoomTransfer,DoomTransferDetail
from payments.views import json_pay
from django.utils import simplejson
from django.db.models.signals import post_save
from django.db import transaction
from django.dispatch import receiver
import miscutils.utils
from django.db import transaction

import logging
log = logging.getLogger(__name__)

def notify_categories(doom,notif):
    log.debug("notify_categories")
    
    for category in doom.categories():#broadcast to all concerned categories for live update
        publish(u"/categories/"+unicode(category.id),notif)
    log.debug("notify_categories done")
    
def notify_transaction_realtime(transaction):
    doom = transaction.doom
    buyer = transaction.user_paying
    if(transaction.user_paid==settings.doume): #Selling case: money transfer to account to guarantee potential win.        
        message_doom = MessageTransactionWallPost.objects.create(message=u"Selling doom " + unicode(doom) + u" for " + unicode(doom.price) + u"(potential win:" + unicode(doom.potential_win) + u")",transaction=transaction)
        wallpost = create_wall_post(group=None,user=None,owner=buyer,content_object=message_doom,category=WallPost.SELLING) #2=selling
        
        #publish on payer's channel (ie to all payers contacts, I have bought/sold a doom!)                
        create_node(u"/dooms/"+str(doom.id))              
        publish(u"/"+transaction.user_paying.username+u"/friends",wallpost_doom_notification(wallpost,NotifTypes.DOOM_NEW_SALE))
        publish(u"/"+transaction.user_paying.username+u"/wall",wallpost_doom_notification(wallpost,NotifTypes.DOOM_NEW_SALE))
        
        notify_categories(doom,doom_notification(doom,NotifTypes.DOOM_NEW_SALE))
        
    else:
        message_doom = MessageTransactionWallPost.objects.create(message=u"Bought doom " + unicode(doom) + u" for " + unicode(doom.price) + u"(potential win:" + unicode(doom.potential_win) + u")",transaction=transaction)
        wallpost = create_wall_post(group=None,user=None,owner=buyer,content_object=message_doom,category=1) #1=buying
        wallpost.notification.create(reciever=transaction.user_paid) #notify that my doom had been bought
        #publish on payer's channel (ie to all payers contacts, I have bought/sold a doom!)            
        publish(u"/"+transaction.user_paying.username+u"/friends",wallpost_doom_notification(wallpost,NotifTypes.DOOM_BOUGHT))
        publish(u"/"+transaction.user_paying.username+u"/wall",wallpost_doom_notification(wallpost,NotifTypes.DOOM_BOUGHT))
        publish(u"/dooms/"+str(doom.id),wallpost_doom_notification(wallpost,NotifTypes.DOOM_BOUGHT)) 
        
        
        #publish on sellers channel (ie to all sellers contacts, I have sold/acquired a doom!)
        publish(u"/"+transaction.user_paid.username+u"/friends",wallpost_doom_notification(wallpost,NotifTypes.DOOM_SOLD))
        publish(u"/"+transaction.user_paid.username+u"/wall",wallpost_doom_notification(wallpost,NotifTypes.DOOM_SOLD))
        publish(u"/dooms/"+str(doom.id),wallpost_doom_notification(wallpost,NotifTypes.DOOM_SOLD))
        
        notify_categories(doom,doom_notification(doom,NotifTypes.DOOM_BOUGHT))

@miscutils.utils.nested_commit_on_success
@receiver(post_save, sender=Transaction)
def transfer_doom(sender, instance=None,**kwargs):
    if instance.is_active and (instance.status == Transaction.PAYMENT_SUCCESS_THROUGH_ACCOUNT or instance.status == Transaction.PAYMENT_SUCCESS_THROUGH_SERVICE):
        log.debug("transfer_doom")        
        if instance.doom:
            instance.doom.update_status()
            if(instance.user_paid==settings.doume): #Selling case: money transfer to account to guarantee potential win. Possible conflicts: created a non-pending doom. A refund is expected                
                if instance.doom.status == Doom.PENDING:     
                    instance.doom.is_active = True
                    instance.doom.owner = instance.user_paying
                    instance.doom.on_sale = True                    
                    instance.doom.save()
                    notify_transaction_realtime(instance)
                    instance.status = Transaction.TRANSACTION_SUCCESS
                    instance.save()
                else: #refund
                    SubTransaction.objects.create(user=instance.user_paying,debit=False,amount=instance.amount,parent=instance)
                    SubTransaction.objects.create(user=instance.user_paid,debit=True,amount=instance.amount,parent=instance)
                    instance.is_active = False
                    instance.save()
                    print "---one"
                    print instance
                    print instance.status
                    print instance.doom.status
                    print instance.doom
                    instance.status = Transaction.PAYMENT_REFUNDED                    
                    instance.save()
            else: #buying case. Possible conflicts: 1) two users bought the same doom 2) bought a non-pending doom.
                #if instance.doom.owner == 
                if instance.doom.status == Doom.PENDING and instance.doom.on_sale:  
                    print "***two"
                    print instance
                    print instance.status
                    print instance.doom.status
                    print instance.doom                  
                    instance.doom.is_active = True
                    instance.doom.owner = instance.user_paying
                    instance.doom.on_sale = False
                    instance.doom.save()
                    notify_transaction_realtime(instance)                    
                else: #refund                    
                    SubTransaction.objects.create(user=instance.user_paying,debit=False,amount=instance.amount,parent=instance)
                    SubTransaction.objects.create(user=instance.user_paid,debit=True,amount=instance.amount,parent=instance)
                    instance.is_active = False
                    instance.save()
                    print "---two"
                    print instance
                    print instance.status
                    print instance.doom.status
                    print instance.doom
                    print instance.doom.owner
                    instance.status = Transaction.PAYMENT_REFUNDED
                    instance.save()

def form_list_from_request(request,doom):    
    i=0
    forms=[]
    #last_form_class = competition_form_dict[lastcompetition]
    if doom:
        for competition in doom.competition_set.all():
            form_class = competition_form_dict[competition.competition_type.model]
            form = form_class(request.POST,prefix=i,instance=competition.competition_object)
            forms.append(form)        
            i=i+1
        
    #if last_form_post_valid:
    #    forms.append(last_form_class(request.POST,prefix=0))
    #else:
    #    forms.append(last_form_class(prefix=0))
    return forms
        
def form_list_from_doom(doom,lastcompetition):    
    i=0
    forms=[]
    last_form_class = competition_form_dict[lastcompetition]
    if doom:
        for competition in doom.competition_set.all():
            form_class = competition_form_dict[competition.competition_type.model]
            form = form_class(prefix=i,instance=competition.competition_object)
            forms.append(form)        
            i=i+1
        
    forms.append(last_form_class(prefix=len(forms)),instance=competition.competition_object)
    return forms
    
def get_bought_nonsold_dooms_context(request):
    log = logging.getLogger(__name__)
    dooms = Doom.objects.filter(owner=request.user,on_sale=False,is_active=True,status=Doom.PENDING)    
    return {"dooms_bought" : dooms }
       
@transaction.commit_on_success
def get_context_general_create_ajax(request,**kwargs):
    log = logging.getLogger(__name__)
    context = {}
    if not request.user.is_authenticated():
        if request.method == "POST":
            context.update({
                    "type":"error",
                    "description":"You are not logged in",
                    "transfer":DoomTransfer.TRANSACTION_FAILED_AND_REDIRECT,
                    "message_type":DoomTransferDetail.NOT_LOGGED_IN,
                    "redirect_url":reverse('auth_login', args=[])
                })
            return HttpResponse(simplejson.dumps(context),'application/javascript'),False
        else:
            return render_to_response("dooms/doom_create_refused.html", 
                                      {"types":DoomTransferDetail,"message_type":DoomTransferDetail.NOT_LOGGED_IN}, RequestContext(request)),False
    if request.method=="POST":
        menu = request.GET.get("menu",False)

        check_subforms = request.GET.get("check_subforms","")
        print request.POST
        create_doom = False
        if "preview" in request.POST:
            create_doom = False
        elif "create" in request.POST:
            create_doom = True
        else:
            log.error("invalid submit")
            
        if check_subforms=="False":
            check_subforms=False
        else:
            check_subforms=True
        if menu:
            frm = DoomForm(request.POST,owner=request.user,check_subforms=check_subforms)
        else:
            frm = DoomForm(request.POST,owner=request.user,prefix="page",check_subforms=check_subforms)
        #log.info(frm_menu)
        #log.info(frm)
        if frm.is_valid():# or (not create_doom): #it is important to evaluate the form in every case
            print "1"
            doom = frm.save(commit=create_doom) #create doom
            print "2"
            
            if create_doom:                
                payment_service = request.user.get_profile().paymentservice_active      
                transaction = Transaction.objects.create(user_paying=request.user,user_paid=settings.doume,doom=doom,amount=doom.potential_win,service=payment_service,redirect_url=reverse("wall_detail",kwargs={"username":request.user.username}))
                return json_pay(request,transactionid=transaction.id),False
            else:
                print "3"
                doom_preview_context = {"transfer":DoomTransfer.PREVIEW,"html":doom.speech}
                
                if not frm.inter.grouping_successful:
                    doom_preview_context.update({"warning" : frm.tree.speech})
                    
                return HttpResponse(simplejson.dumps(doom_preview_context),'application/javascript'),False
            
        else:
            log.error("DoomForm not valid")
            context.update({
                            "type":"error",
                            "description" : "invalid form",
                            "transfer":DoomTransfer.TRANSACTION_FAILED,
                            "message_type":DoomTransferDetail.INVALID_DATA,                            
                            "form_errors":frm._errors
                            })
            return HttpResponse(simplejson.dumps(context),'application/javascript'),False
            
    elif request.method=="GET":
        competition_ids = request.GET.getlist("competition")     
        if competition_ids!=None:
            competition_list = []
            for c_id in competition_ids:
                competition= Competition.objects.get(id=c_id)
                form_class = competition_form_dict[competition.competition_type.model]
                the_form = form_class(prefix=competition.id,instance=competition.competition_object)
                config = render_to_string('dooms/competitions/competition_configure.html', { 'competition': competition,'competition_form':the_form,"page": True},context_instance=RequestContext(request)).replace("\n","")
                competition_list.append({"competition": competition,"config": config})                
            context.update({"get_competitions": competition_list})
        
    context.update({"doom_form" : DoomForm(owner=request.user,prefix="page")})
    
    return context,True

def get_prefered_dooms_and_friendships(request):
    log = logging.getLogger(__name__)
    prefered_dooms = Doom.objects.none()
    friendships = Friendship.objects.friends_for_user(request.user)
    for friendship in friendships:
        if prefered_dooms:
            prefered_dooms = prefered_dooms | Doom.objects.filter(owner=friendship["friend"],on_sale=True,is_active=True,status=Doom.PENDING)
        else:
            prefered_dooms = Doom.objects.filter(owner=friendship["friend"],on_sale=True,is_active=True,status=Doom.PENDING)
        
    log.debug(prefered_dooms)
    return prefered_dooms,friendships