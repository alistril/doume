from dooms.models import Doom
from dooms.competitions.models import Competition
from haystack.query import SearchQuerySet
from haystack.inputs import AutoQuery
from django.template.loader import render_to_string
from dooms.competitions.forms import competition_form_dict
import logging
from ajax_select import LookupChannel
from django.template import RequestContext 
from interface.utils import interface_for_user

competitionlookuplog = logging.getLogger(__name__)

class CompetitionLookup(LookupChannel):
    model = Competition
        
    def get_query(self,q,request):
        competitionlookuplog.debug("get_query")
        
        sqs = SearchQuerySet().filter(content=AutoQuery(q)).models(Competition)
        competitionlookuplog.debug(sqs)
        
        page = int(request.GET.get("page",0))
        if page ==1:
            self.page = True
        else:
            self.page = False
        self.request = request
        
        limit = interface_for_user(request.user).competition_autocomeplete_limit
        return sqs[:limit]

    def get_result(self,obj):
        u""" result is the simple text that is the completion of what the person typed """
        return unicode(obj.object)

    def format_match(self,obj):
        """ (HTML) formatted item for display in the dropdown """
        return unicode(obj.object)

    def format_item_display(self,res):
        """ (HTML) formatted item for displaying item in the selected deck area """
        competitionlookuplog.debug("format_item_display")
        form_class = competition_form_dict[res.object.competition_type.model]
        the_form = form_class(prefix=res.object.id,instance=res.object.competition_object)
        return render_to_string('dooms/competitions/competition_configure.html', { 'competition': res.object, "competition_form" :  the_form,"page":self.page},context_instance=RequestContext(self.request))
        
    def check_auth(self,request):
        return True
        
class DoomLookup(object):

    def get_query(self,q,request):
        """ return a query set.  you also have access to request.user if needed """
        competitionlookuplog.debug("get_query")
        sqs = SearchQuerySet().filter(content=AutoQuery(q)).models(Doom).exclude(owner=request.user).filter(on_sale=1)
        competitionlookuplog.debug(sqs)
                
        for item in sqs:
            competitionlookuplog.debug("item:" + str(item.description) + " -- " + str(item.on_sale) + " -- " + str(item.owner) )
        
        limit = interface_for_user(request.user).doom_autocomeplete_limit
        return sqs[:limit]


    def get_result(self,res):
        competitionlookuplog.debug("get_result")
        """ the search results display in the dropdown menu.  may contain html and multiple-lines. will remove any |  """
        return render_to_string('dooms/doom_preview_menu.html', { 'object': res.object})
        
    def format_match(self,obj):
        """ (HTML) formatted item for display in the dropdown """
        return render_to_string('dooms/doom_preview_menu.html', { 'object': obj.object})

    def format_item_display(self,res):
        competitionlookuplog.debug("format_item_display")
        """ the display of a currently selected object in the area below the search box. html is OK """        
        return render_to_string('dooms/doom_preview_selectable.html', { 'object': res.object})

    def check_auth(self,request):
        return True
        
