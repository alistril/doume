from django.db import models
from django.contrib.auth.models import User

from django.utils.translation import ugettext_lazy as _

from tagging.models import Tag
from categories.models import Category
from django.utils.html import escape


from luthor.lexing import Lexer
from luthor.parsing import Parser
from luthor.interpreting import Interpreter
from luthor.errors import LuthorException
from luthor.definitions import Constant 
from dooms.logic import link_groups,link_nodes
from betting_sites.models import BettingSite

import logging
log = logging.getLogger(__name__)
        
class Doom(models.Model):
    PENDING = 0
    WON = 1
    LOST = 2
    FROZEN = 3
    STATUS_CHOICES = (
        (PENDING, u'Pending'),
        (WON, u'Won'),
        (LOST, u'Lost'),
        (FROZEN, u'Frozen'),
    )
    id_string = models.CharField(_("id_string"),max_length=15,null=True,blank=True)
    owner = models.ForeignKey(User,null=False,related_name="doom_owner_related")
    guarantor = models.ForeignKey(User,null=True,related_name="doom_guarantor_related")
    
    price = models.IntegerField(_("price"),null=True,blank=False)
    potential_win = models.IntegerField(_("potential win"),null=True,blank=False)
    description = models.CharField(_("description"),max_length=255,null=True,blank=True)
    
    combination_logic = models.TextField(_("combination_logic"),max_length=500,null=True,blank=True)
    speech = models.TextField(_('speech'), blank=True,null=True)
    has_won = models.BooleanField(_("has_won"))
    
    created = models.DateTimeField(_("created"), auto_now_add=True)
    modified = models.DateTimeField(_("modified"), auto_now=True)
    is_active = models.BooleanField(_("is_active"),default=True)
    on_sale = models.BooleanField(_("on_sale"),default=True)
    
    status = models.IntegerField(_("status"), null=False, blank=False,choices=STATUS_CHOICES,default=PENDING)
    
    source_site = models.ForeignKey(BettingSite,null=True,blank=True)
    
    def universe(self):
        competitions = self.competition_set.all()
        univ = {
            "#link_groups#" : link_groups,
            "#link_nodes#" : link_nodes,
            "#constant#" : Constant()
            }
        for competition in competitions:
            univ.update({competition.id_string : competition.competition_object.universe()})
        return univ
    #wallpost = generic.GenericRelation(WallPost)
    #message_transaction_wallpost = generic.GenericRelation(MessageTransactionWallPost)    
    def serilize(self):
        try:
            competitions = self.competition_set.all()[:4]
            
            comp_str = ""
            for competition in competitions:
                comp_str+=unicode(competition)+","
            log.debug("Competitions: %s" % comp_str)
        except Exception as e:
            log.error(e)
            raise
        
        return {
            "id":self.id,
            "id_string":self.id_string,
            "owner": self.owner.username,
            "status": self.status,
            "on_sale": self.on_sale,
            "onsale": self.on_sale, #compatibility
            "price": self.price,
            "potential_win": self.potential_win,
            "signature": comp_str[:len(comp_str)-1],
            "description": escape(self.description),
            "speech" : self.speech
            }
    
    def update_status(self):
        log.info("Parsing doom %s" % (self.id_string))
        if self.competition_set.count()==0:
            return
        if self.status == self.FROZEN:
            all_finished = (len(self.competition_set.filter(finished=False))==0)
            log.info("all finished: %r" % all_finished)
            if all_finished:
                log.info("Parsing doom %s" % (self.id_string))
                lexer = Lexer()
                lexer.build()
                parser = Parser(lexer=lexer)
                parser.build()
                try:
                    log.info("Parsing %s on: %s" % (self.combination_logic,self.universe()))
                    tree = parser.parse(self.combination_logic)
                    all_won = Interpreter(tree).execute(self.universe())
                except LuthorException as e:
                    log.error("Compiler error: %s" % e.value)
                    all_won = None
                    raise
                if all_won == True:
                    self.status = self.WON
                elif all_won == False:
                    self.status = self.LOST
                else:
                    log.error("Could not compute doom status")                    
        else:
            all_frozen = (len(self.competition_set.filter(frozen=False))==0)
            log.info("all frozen: %r" % all_frozen)
            if all_frozen:
                self.status = self.FROZEN
        
        self.save()
        
    def set_id_string(self):
        competitions = self.competition_set.all()
        ret = ""
        
        for competition in competitions:
            ret+=competition.id_string
            
        self.id_string = ret[:8] + (hex(self.id)[2:]).upper()
        self.save()
    
    def __unicode__(self):
        if self.description:
            return self.description
        else:
            return u"[Doom]"        
        
    def __hash__(self):
        return self.pk%100
        
    def __eq__(self, other):
        if other is None:
            return False
        return self.pk==other.pk
        
    def __cmp__(self, other):
        return self.pk-other.pk
        
    def categories(self):
        #try to return a unique set of competitions
        competitions_using_my_tags = self.competition_set.all()
        category_set = set()
        for competition in competitions_using_my_tags:
            comp_tags = Tag.objects.get_for_object(competition)
            for comp_tag in comp_tags:
                categories_to_tag = Category.objects.filter(name__contains=comp_tag)
                for cat in categories_to_tag:            
                    category_set.add(cat)

        return category_set
    
    class Meta:
        app_label = 'dooms'


    
