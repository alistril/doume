from django.template import RequestContext
from django.core.urlresolvers import reverse

from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response
from home.helpers import get_home_context

import logging

@login_required
def detail(request,template_name="home/home.html"):    
    log = logging.getLogger(__name__)
    context = dict()
    context.update(get_home_context(request))
    context.update({'buy_doom_redirect':reverse("home_detail",args=[])})
    
    return render_to_response(template_name,
                              context,
                              context_instance=RequestContext(request))

    