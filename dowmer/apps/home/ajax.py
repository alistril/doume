from django.utils import simplejson
from dajaxice.core import dajaxice_functions
from dajax.core import Dajax

from categories.models import Category
from tagging.models import Tag,TaggedItem
from haystack.query import SearchQuerySet
from dooms.models import Doom
from helpers import *

from django.template.loader import render_to_string
from django.template import RequestContext
from miscutils import js_context
from django.core.urlresolvers import reverse
from miscutils import js_context
import logging
from dajaxice.decorators import dajaxice_register     

@dajaxice_register
def home_global_display(request):     
    log = logging.getLogger(__name__)
    log.debug("ajax: home_global_display")
    
    dajax = Dajax()
    
    context = get_home_context(request)
    context.update({"buy_doom_redirect":reverse("home_detail")})    
    home_html = render_to_string("home/home_paginated.html",context,context_instance=RequestContext(request))
    
    dajax.assign('#work-panel','innerHTML',home_html)
    
    js_context.add_all(dajax)
    return dajax.json()
    