from wall.models import WallPost
from dooms.models import Doom
from django.db.models import F

from friends.models import Friendship
from django.contrib.contenttypes.models import ContentType
from django.db.models.query import QuerySet
from django.core.paginator import Paginator, InvalidPage, EmptyPage
import logging

hhlog = logging.getLogger(__name__)

def get_home_wallposts_and_dooms(user):
    #for friendship related wall posts
    #only select wall posts with distinct invitation
    #in that case when two of my friends become friends
    #I don't have two messages:
    #1) A and B are friends
    #2) B and A are friends
    #but only one message
    # 1) xor 2)   
    hhlog.debug("get_home_wallposts_and_dooms")
    friendships = Friendship.objects.friends_for_user(user)
    wallposts = None
    friend_invitation_wallposts = None
    non_friend_invitation_wallposts = None
    dooms = None
    finished_dooms = Doom.objects.get_empty_query_set()
    friendshipinvitation_content_type = ContentType.objects.get(model="friendshipinvitation")
    for friendship in friendships:
        if dooms:
            dooms = dooms | Doom.objects.filter(owner=friendship["friend"]).filter(on_sale=True,is_active=True,status=Doom.PENDING)
        else:
            dooms = Doom.objects.filter(owner=friendship["friend"]).filter(on_sale=True,is_active=True,status=Doom.PENDING)
        
        finished_dooms = finished_dooms | Doom.objects.filter(owner=friendship["friend"],is_active=True).filter(competition__finished=True).exclude(competition__finished=False).distinct()
        
            
        hhlog.debug(friendship["friend"])
        
        if friend_invitation_wallposts:
            _friend_invitation_wallposts = WallPost.objects.filter(owner=friendship["friend"]).filter(content_type=friendshipinvitation_content_type)            
            _friend_invitation_wallposts = _friend_invitation_wallposts.exclude(object_id__in = friend_invitation_wallposts.values_list('object_id',flat=True))            
            friend_invitation_wallposts = friend_invitation_wallposts | _friend_invitation_wallposts            
            hhlog.debug(_friend_invitation_wallposts)
        else:            
            friend_invitation_wallposts = WallPost.objects.filter(owner=friendship["friend"]).filter(content_type=friendshipinvitation_content_type)            
        
                
        
        if non_friend_invitation_wallposts:            
            non_friend_invitation_wallposts = non_friend_invitation_wallposts | WallPost.objects.filter(owner=friendship["friend"]).exclude(content_type=friendshipinvitation_content_type)
        else:
            non_friend_invitation_wallposts = WallPost.objects.filter(owner=friendship["friend"]).exclude(content_type=friendshipinvitation_content_type)
    
    if friend_invitation_wallposts==None and non_friend_invitation_wallposts==None:
        wallposts = WallPost.objects.get_empty_query_set()
    if friend_invitation_wallposts==None and non_friend_invitation_wallposts!=None:
        wallposts = non_friend_invitation_wallposts.order_by("-moment")
    if friend_invitation_wallposts!=None and non_friend_invitation_wallposts==None:
        wallposts = friend_invitation_wallposts.order_by("-moment")
    if friend_invitation_wallposts!=None and non_friend_invitation_wallposts!=None:        
        wallposts = (friend_invitation_wallposts | non_friend_invitation_wallposts).exclude(writer=user).exclude(messagetransactionwallpost__transaction__user_paid=user).order_by("-moment")
    
    if dooms==None:
        dooms = Doom.objects.get_empty_query_set()
    else:
        dooms = dooms.order_by("-modified")
    hhlog.debug(friend_invitation_wallposts)
     
    return wallposts,dooms,finished_dooms
    
def get_home_context(request):
    wallposts,dooms,finished_dooms = get_home_wallposts_and_dooms(request.user)
    limit = request.user.get_profile().interface.home_limit
    paginator = Paginator(wallposts, limit)
    
    try:
        page = int(request.GET.get('page', '1'))
    except ValueError:
        page = 1
        
    try:
        paginator_page = paginator.page(page)
    except (EmptyPage, InvalidPage):
        paginator_page = paginator.page(paginator.num_pages)
    
    return { u'paginator_page': paginator_page,
            u'dooms': list(dooms),
            u'finished_dooms':finished_dooms,
            u'Doom' : Doom
            }