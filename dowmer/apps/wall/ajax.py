from django.utils import simplejson
from dajaxice.core import dajaxice_functions
from dajax.core import Dajax

from categories.models import Category
from tagging.models import Tag,TaggedItem
from haystack.query import SearchQuerySet
from dooms.models import Doom
from helpers import *

from django.template.loader import render_to_string
from django.template import RequestContext
from miscutils import js_context
from django.core.urlresolvers import reverse
import logging
from wall.helpers import get_wall_context
from miscutils import js_context   
from dajaxice.decorators import dajaxice_register 
from im.utils import subscribe_to_wall
from django.contrib.auth.models import User

@dajaxice_register
def wall_global_display_and_subscrible(request,username,jid):     
    log = logging.getLogger(__name__)    
    log.debug("ajax: wall_global_display_and_subscrible")
    
    who = request.user
    user = User.objects.get(username=username)
    
    subscribe_to_wall(who,jid,user)
    dajax = Dajax()
    
    context = get_wall_context(request,username)
    context.update({ 'buy_doom_redirect':reverse("wall_detail",args=[username])})
    wall_html = render_to_string("wall/wall_paginated.html",context,context_instance=RequestContext(request))
    
    dajax.assign('#work-panel','innerHTML',wall_html)
    
    js_context.add_all(dajax)    
    return dajax.json()

    
