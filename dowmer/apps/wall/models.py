import datetime

from django.conf import settings
from django.core.urlresolvers import reverse
from django.db import models
from django.db.models import signals
from django.contrib.auth.models import User


from interactive_notification.models import *
from django.contrib.contenttypes import generic
from django.utils.translation import ugettext_lazy as _
from django.utils.html import escape

class WallPost(models.Model):
    NEWS = 0
    BUYING = 1
    SELLING = 2
    WINNER = 3
    LOSER = 4
    STATUS_CHOICES = (
        (NEWS, u'News'),
        (BUYING, u'Buying'),
        (SELLING, u'Selling'),
        (WINNER, u'Winner'),
        (LOSER, u'Loser')
    )
    writer = models.ForeignKey(User,null=True,related_name="writer_related") #a wall post is written by one user or no user
    owner = models.ForeignKey(User,null=True,related_name="owner_related") #a wall post belongs to one user or no user
    #team = models.ForeignKey(Team,null=True) #a wall post belongs to one team or no team
    notification = generic.GenericRelation(InteractiveNotification)
    
    moment = models.DateTimeField(_("moment"),auto_now=True,null=True)    
    category = models.IntegerField(_("category"),choices=STATUS_CHOICES,default=NEWS) #0 News, 1 Buy, 2 Sell
    
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = generic.GenericForeignKey('content_type', 'object_id')

    group_type = models.ForeignKey(ContentType, null=True,related_name="wallpost_group_type_related")
    group_id = models.PositiveIntegerField(null=True)
    group = generic.GenericForeignKey('group_type', 'group_id')
    
    def serilize(self):
        ret = {}
        if self.writer!=None:
            ret.update({"writer":self.writer.username})
        if self.owner!=None:
            ret.update({"owner":self.owner.username})
            ret.update({"wallpost_owner":self.owner.username}) #compatibility
        ret.update( {
                "category":self.category,
                "content_model":self.content_type.model,
                "content_object":self.content_object.serilize()
                })
        return ret
    def __unicode__(self):
        return "writer:" + unicode(self.writer) + " owner: " + unicode(self.owner) + " object: " + unicode(self.content_object)


class Message(models.Model):
    message = models.CharField(_("message"),max_length=255,null=True)
    def serilize(self):
        return {"message":self.message }
        
    def __unicode__(self):
        return self.message
    
