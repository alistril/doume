from django.conf.urls.defaults import *


urlpatterns = patterns("",    
    url(r"^(?P<username>[\w\._-]+)$", "wall.views.detail", name="wall_detail"),
    url(r"$", "wall.views.mywall", name="wall_mywall"),    
    
)
