from interactive_friends.selenium.interactive_friends_accept_friendship import InteractiveFriendsAcceptFriendShip
from common_test_dependencies.selenium.settings import TIMEOUT,DOMAIN,SCREENSHOT_DIR
from selenium.webdriver.support.ui import WebDriverWait 
from common_test_dependencies.selenium.auth import login
from common_test_dependencies.selenium.wall import check_news_number,is_a_friend_news

class FriendshipWall(InteractiveFriendsAcceptFriendShip):    
    def init(self):
        InteractiveFriendsAcceptFriendShip.init(self)
        
    
    def execute(self):
        self.lewis_wall_lewis = login("lewis","lewis","http://%s/walls/lewis" % DOMAIN)
        self.lewis_wall_alice = login("lewis","lewis","http://%s/walls/alice" % DOMAIN)
        self.lewis_wall_motar = login("lewis","lewis","http://%s/walls/motar" % DOMAIN)
                
        InteractiveFriendsAcceptFriendShip.execute(self)
        
        news_lewis = check_news_number(self.lewis_wall_lewis,2)
        news_alice = check_news_number(self.lewis_wall_alice,1)
        news_motar = check_news_number(self.lewis_wall_motar,1)
        self.lewis_wall_lewis.get_screenshot_as_file('%s/lewis_wall_lewis.png' % SCREENSHOT_DIR)
        self.lewis_wall_alice.get_screenshot_as_file('%s/lewis_wall_alice.png' % SCREENSHOT_DIR)
        self.lewis_wall_motar.get_screenshot_as_file('%s/lewis_wall_motar.png' % SCREENSHOT_DIR)
        
        if not (is_a_friend_news(news_lewis,"alice","lewis")): raise Exception('could not verify friends')
        if not (is_a_friend_news(news_lewis,"motar","lewis")): raise Exception('could not verify friends')                
        if not (is_a_friend_news(news_alice,"alice","lewis")): raise Exception('could not verify friends')                
        if not (is_a_friend_news(news_motar,"motar","lewis")): raise Exception('could not verify friends')
                

        
    def cleanup(self):
        InteractiveFriendsAcceptFriendShip.cleanup(self)
        
        self.lewis_wall_lewis.quit()
        self.lewis_wall_alice.quit()
        self.lewis_wall_motar.quit()