from django.template import RequestContext
from django.contrib import messages

from wall.forms import MessageForm
from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response, get_object_or_404
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from wall.helpers import get_wall_context,group_and_bridge,group_context


import logging
from django.http import HttpResponseRedirect

@login_required
def detail(request,username=None,template_name="wall/wall.html"):
    if username:
        other_user = get_object_or_404(User,username=username)
    group, bridge = group_and_bridge(request)
    
    context = dict()    
    
    if request.method == "POST":
        print "wall.detail.POST"
        message_form = MessageForm(request.POST,group=group,user=request.user,owner=other_user,request=request)
        if message_form.is_valid():
            message_form.save()
    
    context.update(get_wall_context(request,username))
    context.update({
                "message_form": MessageForm(user=request.user,group=group,owner=other_user)
                })
    
    return render_to_response(template_name,context,context_instance=RequestContext(request, group_context(group, bridge)))

@login_required
def mywall(request):
    return HttpResponseRedirect(reverse("wall_detail",args=[request.user.username] ))