from django import forms
from django.utils.translation import ugettext_lazy as _

from wall.models import WallPost,Message
from wall.helpers import *
from im.utils import wallpost_doom_notification,NotifTypes
from pubsub.utils import publish
from dooms.models import Doom
from django.template import RequestContext
from django.template.loader import render_to_string
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.utils.html import escape
import re


class MessageForm(forms.Form):
    message = forms.CharField(label="Message:", required=False, widget=forms.Textarea(attrs = {'class': 'write-on-wall-text'}))
    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop("user", None)
        self.group = kwargs.pop("group", None)
        self.owner = kwargs.pop("owner", None)
        self.request = kwargs.pop("request", None)

        super(MessageForm, self).__init__(*args, **kwargs)

    def save(self):
        wallpost = create_wall_post_message(group=self.group,user=self.user,owner=self.owner,message=self.cleaned_data["message"])
        wallpost.notification.create(reciever=self.owner)        
        publish(u"/"+self.user.username+u"/friends",wallpost_doom_notification(wallpost,NotifTypes.NEWS_SENT))
        publish(u"/"+self.owner.username+u"/friends",wallpost_doom_notification(wallpost,NotifTypes.NEWS_RECIEVED))
        
        publish(u"/"+self.owner.username+u"/wall",wallpost_doom_notification(wallpost,NotifTypes.NEWS_RECIEVED))
    
    def clean(self):
        self.check_group_membership()
        message_raw = self.cleaned_data["message"]
        self.cleaned_data["message"] = escape(message_raw)
        
        def repl(m):
            doom = Doom.objects.filter(id_string=m.group(1),is_active=True,status=Doom.PENDING)
            
            if len(doom)==1:                
                return render_to_string("chat/chat_doom.html",{"doom":doom[0]},RequestContext(self.request))                
            else:
                users = User.objects.filter(username=m.group(1))
                if len(users)==1:
                    return '<a href="%s">%s</a>' % (reverse("wall_detail",args=[users[0].username]),users[0].username)
                else:
                    return m.group(1)
        
        self.cleaned_data["message"] = re.sub(r"\#([a-zA-Z0-9]+)", repl, self.cleaned_data["message"])        

        return self.cleaned_data

    def check_group_membership(self):
        group = self.group
        if group and not self.group.user_is_member(self.user):
            raise forms.ValidationError(_("You must be a member to create new posts"))
                    
    
