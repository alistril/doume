from wall.models import WallPost,Message

from friends.models import Friendship, FriendshipInvitation
from django.shortcuts import get_object_or_404
from django.contrib.auth.models import User
from teams.models import Team
from dooms.models import Doom
from django.core.paginator import Paginator, InvalidPage, EmptyPage
import logging

walllogger = logging.getLogger(__name__)

def group_and_bridge(request):
    """
    Given the request we can depend on the GroupMiddleware to provide the
    group and bridge.
    """
    
    # be group aware
    group = getattr(request, "group", None)
    if group:
        bridge = request.bridge
    else:
        bridge = None
    
    return group, bridge

def group_context(group, bridge):
    # @@@ use bridge
    ctx = {
        "group": group,
    }
    if group:
        ctx["group_base"] = bridge.group_base_template()
    return ctx
    
  
def get_wall_posts(request,owner=None):

    group, bridge = group_and_bridge(request)
    wallposts = None    
    if group:
        if group.user_is_member(request.user):
            wallposts = group.content_objects(WallPost)        
    else:            
        wallposts = WallPost.objects.filter(owner=owner).select_related().all().order_by("-moment")
    
    return wallposts

def create_wall_post(group=None,user=None,owner=None,content_object=None,category=0):
    if group:
        post = WallPost.objects.create(writer=user,content_object=content_object,category=category)
        group.associate(post)
    else:
        post = WallPost.objects.create(writer=user,owner=owner,content_object=content_object,category=category)
        
    return post
    
def create_wall_post_from_request(request,owner=None,content_object=None,category=0):
    group, bridge = group_and_bridge(request)
    create_wall_post(group=group,user=request.user,owner=owner,content_object=content_object,category=category)
    
def create_wall_post_message(group=None,user=None,owner=None,message=None):
    message = Message.objects.create(message=message)
    post = create_wall_post(group=group,user=user,owner=owner,content_object=message,category=0)
    return post

def create_wall_post_message_from_request(request=None,owner=None,message=None):    #most basic post        
    group, bridge = group_and_bridge(request)
    
    return create_wall_post_message(group=group,user=request.user,owner=owner,message=message)
    
def get_wall_context(request,username):
    other_user = get_object_or_404(User, username=username)

    is_friend = Friendship.objects.are_friends(request.user, other_user)
    is_invited = FriendshipInvitation.objects.invitations_pending(from_user=request.user,to_user=other_user).count()>0
    has_invited = FriendshipInvitation.objects.invitations_pending(from_user=other_user,to_user=request.user).count()>0
    other_friends = Friendship.objects.friends_for_user(other_user)
    other_teams = Team.objects.filter(members=other_user).order_by("name")
    if request.user == other_user:
        is_me = True
    else:
        is_me = False
    
    selling_dooms = Doom.objects.filter(owner=other_user,on_sale=True,is_active=True,status=Doom.PENDING).order_by("-modified")
    finished_dooms = Doom.objects.filter(owner=other_user,is_active=True).filter(competition__finished=True).exclude(competition__finished=False).distinct()
    wallposts = get_wall_posts(request,owner=other_user)
    limit = request.user.get_profile().interface.wall_limit
    paginator = Paginator(wallposts, limit)
    try:
        page = int(request.GET.get('page', '1'))
    except ValueError:
        page = 1
        
    try:
        paginator_page = paginator.page(page)
    except (EmptyPage, InvalidPage):
        paginator_page = paginator.page(paginator.num_pages)
    return {
                "Doom": Doom,
                "selling_dooms": selling_dooms,
                "finished_dooms": finished_dooms,
                "is_friend" : is_friend,
                "is_invited": is_invited,
                "has_invited": has_invited,
                "other_user" : other_user,
                "other_friends" : other_friends,
                "other_teams" : other_teams,
                "is_me" : is_me,
                "paginator_page" : paginator_page
                }