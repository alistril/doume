from django.conf.urls.defaults import *


urlpatterns = patterns("",
    url(r"^fake_pay/(?P<amount>\d+)/$", "payments.views.fake_add_funds", name="payments_fake_add_funds"),    
    url(r"^pay/(?P<transactionid>\d+)/$", "payments.views.pay", name="payments_pay"),
    url(r"^json_pay/(?P<transactionid>\d+)/$", "payments.views.json_pay", name="payments_json_pay"),
    url(r"^finished/(?P<transactionid>\d+)/$", "payments.views.payments_finished",name="payments_finished"),
    url(r"^canceled/(?P<transactionid>\d+)/$", "payments.views.payments_canceled",name="payments_canceled"),
)

