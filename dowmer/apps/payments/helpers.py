from django.template import RequestContext

from django.core.exceptions import ObjectDoesNotExist
from django.template import RequestContext

from django.shortcuts import render_to_response
from django.contrib.auth.models import User

from django.http import HttpResponse, HttpResponseForbidden, HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _, ugettext
from payments.models import Transaction,SubTransaction
from django.conf import settings
from bank.models import Account

from decimal import Decimal
from payment_services.forms import ObtainServiceForm
from django.utils import simplejson


from django.db import transaction
import payment_services.forms

import logging

paylog = logging.getLogger(__name__)

SITE_URL = getattr(settings, 'SITE_URL')

class DoomTransferStatus:
    TRANSFERED_TO_PAYMENTS = 1
    SELECT_PAYMENT_SERVICE = 2
    PAYING_THROUGH_SERVICE = 3
    
class DoomTransfer:
    TRANSACTION_COMPLETE = 1
    TRANSACTION_REDIRECT = 2
    TRANSACTION_FAILED = 3
    TRANSACTION_FAILED_AND_REDIRECT = 4
    PREVIEW = 5
    TRANSACTION_COMPLETE_AND_REDIRECT = 6
    
class DoomTransferDetail:
    NOT_ON_SALE = 1
    CANNOT_BUY_FROM_SELF = 2
    PAYMENT_FAILED = 3
    NOT_LOGGED_IN = 4
    ALREADY_ON_SALE = 5
    DOES_NOT_EXIST = 6
    DOES_NOT_EXIST_OR_ALREADY_ON_SALE = 7
    INVALID_DATA = 8
    NO_PAYMENT_SERVICE = 9
    FORBIDDEN_STATUS = 10

@transaction.commit_manually
def transaction_done_update_status(trans,request):
    try:
        if trans.status != Transaction.PAYING_THROUGH_SERVICE:
            transaction.rollback()
            return
        trans.status = Transaction.PAYMENT_FINISHED
        trans.save()
        
        #cases for checkout are:
        #paying: void, paid: doume: artificial add to doume's account -> no checkout            
        #paying: void, paid: user: add funds-> checkout
        #paying: user, paid: void: retrieve funds -> no checkout
        #paying: user, paid: doume: guarantee funds -> checkout
        #paying: user, paid: user: regular trans, account debited -> checkout  
        #paying: doume, paid: void: artificial retrieve from doume's account -> no checkout
        #paying: doume, paid: user: cancel guarantee (never happens since trans gets updated) -> no checkout        
        if (trans.user_paying==settings.void and trans.user_paid!=settings.void and trans.user_paid!=settings.doume) or (trans.user_paying!=settings.void and trans.user_paying!=settings.doume and trans.user_paid==settings.doume) or (trans.user_paying!=settings.void and trans.user_paying!=settings.doume and trans.user_paid!=settings.void and trans.user_paid!=settings.doume):
            if trans.handle_payments_to_doume(request):
                if trans.user_paying!=settings.void:
                    #implicit add funds (skip in regular add funds case)
                    implicit_transaction = Transaction.objects.create(user_paying=settings.void,user_paid=trans.user_paying,doom=trans.doom,amount=trans.amount,status=Transaction.PAYMENT_SUCCESS_IMPLICIT)
                    SubTransaction.objects.create(user=trans.user_paying,debit=False,amount=trans.amount,parent=implicit_transaction)
                    trans.save() #modify the date of trans to now
                #buyer pays something    
                SubTransaction.objects.create(user=trans.user_paying,debit=True,amount=trans.amount,parent=trans)
                #seller is paid
                SubTransaction.objects.create(user=trans.user_paid,debit=False,amount=trans.amount,parent=trans)
                #payment_finished.send(sender=service,trans=trans)
                trans.status = Transaction.PAYMENT_SUCCESS_THROUGH_SERVICE
                trans.save()
            else:
                #payment_failed.send(sender=service,trans=trans)
                if trans.status == Transaction.PAYING_THROUGH_SERVICE:
                    trans.status = Transaction.PAYMENT_FAILED
                    trans.save()
                else: #a transaction is considered a success when at least one call to handle_payments_to_doume per transaction is a success               
                    transaction.rollback()
                    paylog.warning("Transaction %d was expected to be in status PAYING_THROUGH_SERVICE but was in status %s. This request is considered faulty and will be ignored. This can happen from time to time." % (trans.id,trans.get_status_display()) )
                    return
                
    except:
        transaction.rollback()
    else:
        transaction.commit()

            
def transaction_canceled_update_status(transaction,request):
    transaction.status = Transaction.PAYMENT_FAILED_CANCELED
    transaction.save()
    
    #cases for checkout are:
    #paying: void, paid: doume: artificial add to doume's account -> no checkout            
    #paying: void, paid: user: add funds-> checkout
    #paying: user, paid: void: retrieve funds -> no checkout
    #paying: user, paid: doume: guarantee funds -> checkout
    #paying: user, paid: user: regular transaction, account debited -> checkout  
    #paying: doume, paid: void: artificial retrieve from doume's account -> no checkout
    #paying: doume, paid: user: cancel guarantee (never happens since transaction gets updated) -> no checkout        
    if (transaction.user_paying==settings.void and transaction.user_paid!=settings.void and transaction.user_paid!=settings.doume) or (transaction.user_paying!=settings.void and transaction.user_paying!=settings.doume and transaction.user_paid==settings.doume) or (transaction.user_paying!=settings.void and transaction.user_paying!=settings.doume and transaction.user_paid!=settings.void and transaction.user_paid!=settings.doume):
        if transaction.handle_payments_to_doume(request):
            if transaction.user_paying!=settings.void:
                #implicit add funds (skip in regular add funds case)
                implicit_transaction = Transaction.objects.create(user_paying=settings.void,user_paid=transaction.user_paying,doom=transaction.doom,amount=transaction.amount,status=Transaction.PAYMENT_FAILED_CANCELED)
                transaction.save() #modify the date of transaction to now


@transaction.commit_manually
def transfer_funds_internally(trans):
    transaction.commit()
    sub_trans = SubTransaction.objects.create(user=trans.user_paying,debit=True,amount=trans.amount,parent=trans)            
    paylog.debug("Created subtransaction %s" % sub_trans)
    sub_transaction_doume_to_seller = SubTransaction.objects.create(user=trans.user_paid,debit=False,amount=trans.amount,parent=trans)    
    paylog.debug("Created subtransaction %s" % sub_transaction_doume_to_seller)        
    #payment_finished.send(sender=service,trans=trans)
    
    trans.status = Transaction.PAYMENT_FINISHED
    trans.save() #btw this forces a write-write conflict on Transaction table
    account = Account(trans.user_paying)
    if account.amount_usable()<Decimal(0):
        transaction.rollback()
        if trans.user_paid==settings.void:
            trans.status = Transaction.PAYMENT_FAILED
        else:
            if trans.service is None:
                trans.status = Transaction.SELECT_PAYMENT_SERVICE
            else:
                trans.status = Transaction.PAYING_THROUGH_SERVICE
            
        trans.save()
    else:
        if trans.handle_payments_to_void()==False: #payments to void should take place but failed
            transaction.rollback()
            trans.status = Transaction.ACTIVATE_CREDIT_ABILITY
            trans.save()
    
    transaction.commit()
    
def update_transaction_status(transaction,request):
    account = Account(transaction.user_paying)
    service = None    
    
    #if transaction.service is None and request.user.get_profile().paymentservice_active is not None:
    #    transaction.service = request.user.get_profile().paymentservice_active
    #    transaction.save()
    
    if transaction.status==Transaction.TRANSFERED_TO_PAYMENTS:
        if transaction.user_paid==settings.void: #retrieve funds
            if transaction.service is None or not transaction.service.credit_ability:
                transaction.status=Transaction.SELECT_PAYMENT_SERVICE_WITH_CREDIT
                transaction.save()
            elif not transaction.service.credit_activated:
                transaction.status=Transaction.ACTIVATE_CREDIT_ABILITY
                transaction.save()
            else:
                transfer_funds_internally(transaction)                
        elif transaction.user_paying==settings.void: #add funds, always pay through service
            if transaction.service is None:
                transaction.status=Transaction.SELECT_PAYMENT_SERVICE
            else:
                transaction.status=Transaction.PAYING_THROUGH_SERVICE
            transaction.save()
        else:
            if transaction.service is None:
                if account.funds_available(transaction.amount):
                    #transaction.status=Transaction.PAYING_THROUGH_ACCOUNT
                    transfer_funds_internally(transaction)
                    service = None
                else:
                    transaction.status=Transaction.SELECT_PAYMENT_SERVICE
                    transaction.save()
            else:
                transaction.status=Transaction.PAYING_THROUGH_SERVICE
                transaction.save()            
                service = transaction.service
            
    elif transaction.status==Transaction.SELECT_PAYMENT_SERVICE:
        if request.method == "POST":
            form = payment_services.forms.ObtainServiceForm(request.POST,user=request.user)
            if form.is_valid():
                transaction.status=Transaction.PAYING_THROUGH_SERVICE
                transaction.service = form.save()                
                transaction.save()
                #request.user.get_profile().paymentservice_active = transaction.service
                #request.user.get_profile().save()
                
    elif transaction.status==Transaction.SELECT_PAYMENT_SERVICE_WITH_CREDIT:
        if request.method == "POST":
            form = payment_services.forms.ObtainServiceForm(request.POST,user=request.user)
            if form.is_valid():
                form_details = form.get_detailed_form_class()(request.POST) #instantiation just to validate
                if form_details.is_valid():
                    #transaction.status=Transaction.PAYING_THROUGH_SERVICE
                    transaction.service = form.save()
                    form_details = form.get_detailed_form_class()(request.POST,instance=transaction.service.content_object)
                    form_details.save()
                    transaction.save()                    
                    transfer_funds_internally(transaction)
                    #request.user.get_profile().paymentservice_active = transaction.service
                    #request.user.get_profile().save()
                    
                
    elif transaction.status==Transaction.ACTIVATE_CREDIT_ABILITY:
        if request.method == "POST":
            form = transaction.service.get_detailed_form_class()(request.POST,instance=transaction.service.content_object)
            if form.is_valid():
                form.save()
                #transaction.status=Transaction.PAYING_THROUGH_SERVICE                
                transaction.save()
                transfer_funds_internally(transaction)
                #request.user.get_profile().paymentservice_active = transaction.service
                #request.user.get_profile().save()
