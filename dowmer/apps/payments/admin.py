from payments.models import *
from django.contrib import admin

admin.site.register(Transaction)
admin.site.register(SubTransaction)
