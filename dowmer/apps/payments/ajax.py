from django.utils import simplejson
from dajaxice.decorators import dajaxice_register

from payments.models import Transaction

from django.template.loader import render_to_string
from django.template import RequestContext

from bank.models import Account
import logging

log = logging.getLogger(__name__)


@dajaxice_register
def get_transaction(request,transactionid):
    log.debug("AJAX: get_transaction")    
    try:
        transaction = Transaction.objects.get(pk=transactionid)
        log.debug("AJAX: transaction="+str(transaction))
        if transaction.user_paid == request.user or transaction.user_paying == request.user:                            
            return simplejson.dumps({"type":"success","transaction":transaction.serilize()},use_decimal=True)        
        else:
            return simplejson.dumps({"type":"failed"})
    except Transaction.DoesNotExist:
        return simplejson.dumps({"type":"failed"})