from django.template import RequestContext
from django.contrib import messages

from django.core.exceptions import ObjectDoesNotExist
from wall.forms import MessageForm
from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response, get_object_or_404
from django.contrib.auth.models import User
from dooms.competitions.models import Competition
from django.http import HttpResponse, HttpResponseForbidden, HttpResponseRedirect
from django.core.urlresolvers import reverse
from payments.forms import *
from django import forms
from django.utils.translation import ugettext_lazy as _, ugettext
from models import Transaction,SubTransaction
from django.conf import settings
from bank.models import Account
from django.contrib import messages
from django.utils import simplejson
import logging

paymentslogger = logging.getLogger(__name__)


import django.dispatch

payment_finished = django.dispatch.Signal(providing_args=["transaction"])
payment_canceled = django.dispatch.Signal(providing_args=["transaction"])
payment_failed = django.dispatch.Signal(providing_args=["transaction"])
from helpers import *
from payments.helpers import DoomTransfer,DoomTransferDetail,DoomTransferStatus,update_transaction_status,transaction_done_update_status,transaction_canceled_update_status

      
            
@login_required
def pay(request,transactionid=None):
    print "pay"
    transactionid = int(transactionid)    
    transaction = Transaction.objects.get(id=transactionid)
    print "before status update:",transaction
    update_transaction_status(transaction,request)
    print "after status update:",transaction
    if transaction.status==Transaction.SELECT_PAYMENT_SERVICE:
        return render_to_response("payment_services/select_service.html", {"form" : ObtainServiceForm(user=request.user),"transaction": transaction}, RequestContext(request))            
    elif transaction.status==Transaction.SELECT_PAYMENT_SERVICE_WITH_CREDIT:
        return render_to_response("payment_services/select_and_configure_service.html", {"form" : ObtainServiceForm(user=request.user),"transaction": transaction}, RequestContext(request))            
    elif transaction.status==Transaction.ACTIVATE_CREDIT_ABILITY:
        print "transaction.service.content_object",transaction.service.content_object
        return render_to_response("payment_services/change_service.html", {"form" : transaction.service.get_detailed_form_class()(instance=transaction.service.content_object),"transaction": transaction}, RequestContext(request))
    elif transaction.status==Transaction.PAYING_THROUGH_SERVICE:
        url = transaction.service.preprocess(transaction)
        return HttpResponseRedirect(url)
    elif transaction.status==Transaction.PAYMENT_FINISHED: #in this case subtransactions were be implcitly handeled by update_transaction_status
        transaction.status=Transaction.PAYMENT_SUCCESS_THROUGH_ACCOUNT
        transaction.save()
        return HttpResponseRedirect("%s?transactionid=%d" % (transaction.redirect_url,transaction.id))
    elif transaction.status==Transaction.PAYMENT_FAILED:
        return HttpResponseRedirect("%s?transactionid=%d" % (transaction.redirect_url,transaction.id))
                                  
def json_pay(request,transactionid=None):
    transactionid = int(transactionid)    
    transaction = Transaction.objects.get(id=transactionid)
    update_transaction_status(transaction,request)
    if transaction.status==Transaction.PAYING_THROUGH_SERVICE:
        url = transaction.service.preprocess(transaction)
        
        return HttpResponse(simplejson.dumps(
                                                 {
                                                    "type":"success",
                                                    "description":"Not enough money on current account",
                                                    "transfer":DoomTransfer.TRANSACTION_REDIRECT,                                                
                                                    "redirect_url":url
                                                    }                    
                                                    ),'application/javascript')
    elif transaction.status==Transaction.PAYMENT_FINISHED: #in this case subtransactions were be implcitly handeled by update_transaction_status
        transaction.status=Transaction.PAYMENT_SUCCESS_THROUGH_ACCOUNT
        transaction.save()
        #all signals will be synchronously processed here
        if transaction.status==Transaction.PAYMENT_REFUNDED:
            return HttpResponse(simplejson.dumps(
                                                     {
                                                        "type":"failed",
                                                        "description":"Payment refunded",
                                                        "transfer":DoomTransfer.TRANSACTION_FAILED_AND_REDIRECT,
                                                        "redirect_url":"%s?transactionid=%d" % (transaction.redirect_url,transaction.id)
                                                        }
                                                        ),'application/javascript')

        else:
            return HttpResponse(simplejson.dumps(
                                                 {
                                                    "type":"success",
                                                    "description":"Money has been transfered from your local account",
                                                    "transfer":DoomTransfer.TRANSACTION_COMPLETE,
                                                    "redirect_url":"%s?transactionid=%d" % (transaction.redirect_url,transaction.id)
                                                    }
                                                    ),'application/javascript')
    elif transaction.status==Transaction.PAYMENT_FAILED:        
        return HttpResponse(simplejson.dumps(
                                                 {
                                                    "type":"failed",
                                                    "description":"Payment failed",
                                                    "transfer":DoomTransfer.TRANSACTION_FAILED_AND_REDIRECT,
                                                    "transaction" : transaction.serilize(),
                                                    "redirect_url":"%s?transactionid=%d" % (transaction.redirect_url,transaction.id)
                                                    }
                                                    ),'application/javascript')
    else:
        return HttpResponse(simplejson.dumps({
                                    "type":"success",
                                    "description":"Not enough money on current account",
                                    "transfer":DoomTransfer.TRANSACTION_REDIRECT,                                                
                                    "redirect_url":reverse("payments_pay",kwargs={"transactionid":transaction.id})
                                    }),'application/javascript')


def payments_finished(request,transactionid=None,template_name="payments/payment_finished.html"):
    transaction = Transaction.objects.get(id=transactionid)
    if transaction.status == Transaction.PAYING_THROUGH_SERVICE:
        transaction_done_update_status(transaction,request)
    
    if transaction.redirect_url is None:
        if transaction.status == Transaction.PAYMENT_SUCCESS_THROUGH_SERVICE:        
            return render_to_response("payments/payment_finished.html", RequestContext(request))        
        else:    
            return render_to_response("payments/payment_failed.html", RequestContext(request))
    else:
        redirect_url = "%s?transactionid=%d" % (transaction.redirect_url,transaction.id)        
        return HttpResponseRedirect(redirect_url)
    
            
def payments_canceled(request,service=None,transactionid=None,template_name="payments/payment_canceled.html"):    
    transaction = Transaction.objects.get(id=transactionid)
    transaction_canceled_update_status(transaction)
    return HttpResponseRedirect("%s?transactionid=%d" % (transaction.redirect_url,transaction.id))
    
def fake_add_funds(request,amount=None):
    transaction = Transaction.objects.create(user_paying=settings.void,user_paid=request.user,doom=None,amount=amount,status=Transaction.PAYMENT_SUCCESS_IMPLICIT)
    SubTransaction.objects.create(user=transaction.user_paid,debit=False,amount=transaction.amount,parent=transaction)
    return render_to_response("payments/payment_finished.html", RequestContext(request))