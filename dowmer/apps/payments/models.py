import datetime

from django.conf import settings
from django.core.urlresolvers import reverse
from django.db import models
from django.db.models import signals
from django.contrib.auth.models import User


from interactive_notification.models import *
from django.contrib.contenttypes.models import ContentType

from django.contrib.contenttypes import generic
from django.utils.translation import ugettext_lazy as _

from wall.models import WallPost
from decimal import *
from django.db.utils import DatabaseError
from dooms.models import Doom
from payment_services.models import PaymentService

class Transaction(models.Model):
    TRANSFERED_TO_PAYMENTS  = 1
    SELECT_PAYMENT_SERVICE = 2
    SELECT_PAYMENT_SERVICE_WITH_CREDIT = 3
    ACTIVATE_CREDIT_ABILITY = 4
    PAYING_THROUGH_SERVICE = 5
    PAYING_THROUGH_ACCOUNT  = 6
    WAITING_FOR_REFUND  = 7
    PAYMENT_FINISHED = 8
    PAYMENT_SUCCESS_THROUGH_ACCOUNT = 9
    PAYMENT_SUCCESS_THROUGH_SERVICE = 10
    PAYMENT_SUCCESS_IMPLICIT = 11
    PAYMENT_FAILED  = 12
    PAYMENT_FAILED_CANCELED = 13
    PAYMENT_REFUNDED = 14
    TRANSACTION_SUCCESS = 15
    
    STATUS_CHOICES = (
        (TRANSFERED_TO_PAYMENTS, u"TRANSFERED_TO_PAYMENTS"),
        (SELECT_PAYMENT_SERVICE, u"SELECT_PAYMENT_SERVICE"),
        (SELECT_PAYMENT_SERVICE_WITH_CREDIT, u"SELECT_PAYMENT_SERVICE_WITH_CREDIT"),
        (ACTIVATE_CREDIT_ABILITY, u"ACTIVATE_CREDIT_ABILITY"),
        (PAYING_THROUGH_SERVICE, u"PAYING_THROUGH_SERVICE"),
        (PAYING_THROUGH_ACCOUNT, u"PAYING_THROUGH_ACCOUNT"),
        (WAITING_FOR_REFUND, u"WAITING_FOR_REFUND"),
        (PAYMENT_FINISHED, u"PAYMENT_FINISHED"),
        (PAYMENT_SUCCESS_THROUGH_ACCOUNT, u"PAYMENT_SUCCESS_THROUGH_ACCOUNT"),
        (PAYMENT_SUCCESS_THROUGH_SERVICE, u"PAYMENT_SUCCESS_THROUGH_SERVICE"),
        (PAYMENT_SUCCESS_IMPLICIT, u"PAYMENT_SUCCESS_IMPLICIT"),
        (PAYMENT_FAILED, u"PAYMENT_FAILED"),
        (PAYMENT_FAILED_CANCELED, u"PAYMENT_FAILED_CANCELED"),
        (PAYMENT_REFUNDED, u"PAYMENT_REFUNDED"),
        (TRANSACTION_SUCCESS, u"TRANSACTION_SUCCESS")
    )
    doom = models.ForeignKey(Doom, null=True,unique=False,verbose_name=_("doom"),blank=True)
    amount = models.DecimalField(_("amount"), null=False, blank=False,decimal_places=4,max_digits=15)  
    user_paying = models.ForeignKey(User, unique=False,related_name=_("transaction_user_paying_related"))
    user_paid = models.ForeignKey(User, unique=False,related_name=_("transaction_user_paid_related"))    
    datetime = models.DateTimeField(_("date_time"), auto_now=True)
    is_active = models.BooleanField(_("is_active"),null=False,default=True)
    status = models.IntegerField(_("status"), null=False, blank=False,choices=STATUS_CHOICES,default=TRANSFERED_TO_PAYMENTS)
    service = models.ForeignKey(PaymentService, unique=False,null=True,blank=True)
    redirect_url = models.URLField(null=True,blank=True)

    def serilize(self):
        ret = {                
                "amount" : self.amount,             
                "datetime" : unicode(self.datetime),
                "status" : self.status,
                "is_active" : self.is_active
                }
        if self.doom is not None:
             ret.update({"doom" : self.doom.serilize()})
        if self.user_paying is not None:
            ret.update({"user_paying" : self.user_paying.username})
        if self.user_paid is not None:
            ret.update({"user_paid" : self.user_paid.username})
        if self.redirect_url is not None:
            ret.update({"redirect_url" : self.redirect_url})
        return ret
    
    def preprocess(self):
        return self.service.preprocess(self)
        
    def is_success(self):
        return self.status==Transaction.PAYMENT_SUCCESS_THROUGH_ACCOUNT or self.status==Transaction.PAYMENT_SUCCESS_THROUGH_SERVICE or self.status==Transaction.PAYMENT_SUCCESS_IMPLICIT or self.status==Transaction.TRANSACTION_SUCCESS
        
    def is_fail(self):
        return self.status==Transaction.PAYMENT_FAILED or self.status==Transaction.PAYMENT_FAILED_CANCELED or self.status==Transaction.PAYMENT_REFUNDED
        
    def handle_payments_to_void(self):
        if self.user_paid==settings.void:
            return self.service.handle_payments_to_void(self)
        else:
            return None
            
    def handle_payments_to_doume(self,request):
        if self.user_paid!=settings.void:
            return self.service.handle_payments_to_doume(request,self)
        else:
            return None
            
    def __unicode__(self):
        return u"[" + unicode(self.status) + u"]" + unicode(self.user_paying) + u"---(" + unicode(self.amount) + u")--->" + unicode(self.user_paid)  
    
    
class SubTransaction(models.Model):    
    amount = models.DecimalField(_("amount"), null=False, blank=False,decimal_places=4,max_digits=15)  
    datetime = models.DateTimeField(_("date_time"), auto_now_add=True)
    user = models.ForeignKey(User, unique=False,related_name=_("transaction_user_related"))
    debit = models.BooleanField()
    
    parent = models.ForeignKey(Transaction, null=False, unique=False,verbose_name=_("transaction"))
    
    def __unicode__(self):        
        return unicode(self.user) + u":" + (u"debit(" if self.debit else u"credit(") + unicode(self.amount) + u") linked to [" + unicode(self.parent) + u"]"  
       
    
class MessageTransactionWallPost(models.Model):
    message = models.CharField(_("message"),max_length=255,null=True)
    transaction = models.ForeignKey(Transaction,null=False)
    wallpost = generic.GenericRelation(WallPost)
    def serilize(self):
        return {
                "message":self.message,
                "transaction":self.transaction.serilize()
                }

    def __unicode__(self):
        return "[message %s,transaction:%s]" % (unicode(self.message),unicode(self.transaction))
