from common_test_dependencies.selenium.settings import *
from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait # available since 2.4.0
from common_test_dependencies.selenium.settings import TIMEOUT,DOMAIN,SCREENSHOT_DIR

def news_number(driver,number):
   WebDriverWait(driver,TIMEOUT ).until(lambda driver: driver.find_elements_by_class_name("news"))
   elements = driver.find_elements_by_class_name("news")
   if len(elements)==number:
       return True
   else:
       return False

def check_news_number(driver,number):
   WebDriverWait(driver,TIMEOUT ).until(lambda driver: news_number(driver,number))
   elements = driver.find_elements_by_class_name("news")
   
   return elements

def is_a_friend_news(elements,user1,user2):
    found=False
    for element in elements:
        if not (element.text.find(user1)==-1 or element.text.find(user2)==-1 or element.text.find("friends")==-1):
            found=True
            
    return found