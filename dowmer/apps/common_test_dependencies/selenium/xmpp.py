from pubsub.utils import delete,create_node

def reset_node(node):
    try:        
        create_node(node)
    except:
        pass
        
def delete_node(node):
    try:        
        delete(node)
    except:
        pass
        
def reset_user(user):
    reset_node("/%s/friends" % user)
    reset_node("/%s/wall" % user)
    reset_node("/%s/only" % user)
    
def delete_user(user):
    delete_node("/%s/friends" % user)
    delete_node("/%s/wall" % user)
    delete_node("/%s/only" % user)
    
def delete_doom(id):
    delete_node("/dooms/%d" % id)