from common_test_dependencies.selenium.settings import *
from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait # available since 2.4.0
from selenium.common.exceptions import NoSuchElementException
import time


def login(user,passwd,home_page):
   # Create a new instance of the Firefox driver
   driver = webdriver.Firefox()

   try:
       driver.get("http://%s/accounts/login/" % DOMAIN)
       WebDriverWait(driver, TIMEOUT)

       username = driver.find_element_by_name("username")
       password = driver.find_element_by_name("password")


       username.send_keys(user)
       password.send_keys(passwd)

       password.submit()
       WebDriverWait(driver,TIMEOUT )
       # the page is ajaxy so the title is originally this:
       driver.get(home_page)
       WebDriverWait(driver,TIMEOUT )
   finally:
       pass

   return driver
