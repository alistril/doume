from subprocess import call

def reset_db(fixture):
    call(['cp', '_dev.db', 'dev.db'])
    call(['python','manage.py','loaddata',fixture])
