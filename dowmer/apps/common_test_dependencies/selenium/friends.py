from common_test_dependencies.selenium.settings import *
from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait # available since 2.4.0
from common_test_dependencies.selenium.auth import login

def add_friend(me,friend):
   driver = login(me,me,"http://%s/walls/%s" % (DOMAIN,friend))
   add_as_friend = driver.find_element_by_id("add-as-a-friend")
   add_as_friend.click()
   WebDriverWait(driver,TIMEOUT ).until(lambda driver : driver.find_element_by_id("aafsubmit"))
   add_as_friend_submit = driver.find_element_by_id("aafsubmit")
   add_as_friend_submit.click()
   WebDriverWait(driver,TIMEOUT )
   driver.get("http://%s/interactive_notifications/all/" % DOMAIN)
   WebDriverWait(driver,TIMEOUT )
   return driver
   
def friendship_accepted(driver,friend):
   invitations = driver.find_elements_by_class_name("if-invitation-item")
   for invitation in invitations:
       if invitation.text.find("You are now friends with %s" % friend)!=-1:
           return True

   return False

def check_friendship_accepted(driver,friend):
    WebDriverWait(driver,TIMEOUT ).until(lambda driver: friendship_accepted(driver,friend))

