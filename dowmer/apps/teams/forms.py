from django import forms
from django.utils.translation import ugettext_lazy as _

from teams.models import Team
from django.template.defaultfilters import slugify
from django.contrib import messages

class CreateTeamForm(forms.ModelForm):
    
    def save(self,commit=True):
        name = self.cleaned_data["name"]
        description = self.cleaned_data["description"]
        slug = slugify(name)
        
        if Team.objects.filter(slug__iexact=slug).exists():
            messages.add_message(request, messages.WARNING,"Same or similar name already exists")
            
            raise forms.ValidationError(_("Same or similar name already exists"))
        else:
            team = Team(name=name,description=description,slug=slug)
            if(commit):
                team.save()
        
        return team;
        
    class Meta:
        model = Team
        fields = ["name", "description"]
        
class InviteUserForm(forms.Form):
    username = forms.CharField(
        label = _("Username"),
        max_length = 30,
        widget = forms.TextInput()
    )
        
