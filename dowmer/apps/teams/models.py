from django.core.urlresolvers import reverse
from django.db import models
from django.utils.translation import ugettext_lazy as _

from django.contrib.auth.models import  User

from django.contrib.contenttypes.models import ContentType

from django.contrib.contenttypes import generic

from groups.base import Group


class Team(Group):

    members = models.ManyToManyField(User,
        related_name = "team_members",
        verbose_name=_("members")
    )

    def get_url_kwargs(self):
        return {"team_slug": self.slug}

    def get_absolute_url(self):
        return reverse("teams_team_detail",kwargs={"team_slug":self.slug})
    
    def captain(self):
        return self.creator
     

class TeamJoinNotification(models.Model):
    
    user = models.ForeignKey(User)

    #notification = generic.GenericRelation(InteractiveNotification)
    message = models.CharField(max_length=255,null=True,blank=True)
    team = models.ForeignKey(Team)

    #0 user wants to join team
    #1 team wants user to join

    #3 join request was been refused (either by user or team)
    #4 join request was accepted (either by user or team)
    status = models.IntegerField() 
