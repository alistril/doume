from django.conf import settings
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django.utils.datastructures import SortedDict
from django.utils.translation import ugettext

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from teams.forms import CreateTeamForm,InviteUserForm
from teams.models import Team,TeamJoinNotification
from interactive_notification.models import InteractiveNotification
from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404
from groups.bridge import ContentBridge
from wall.forms import MessageForm
from wall.helpers import *

@login_required
def create(request, form_class=CreateTeamForm, template_name="teams/create.html"):
    team_form = form_class(request.POST or None)
    if request.method == "POST":        
        
        if team_form.is_valid():
            team = team_form.save(commit=False)
            team.creator = request.user
            team.save()
            team.members.add(request.user)
                
            return HttpResponseRedirect(reverse("teams_team_detail",kwargs={"team_slug":team.slug}))
                            
        
        else:
            return render_to_response(template_name, {
                "teamcreate_form": team_form,
                }, context_instance=RequestContext(request))

    else:
        
        return render_to_response(template_name, {
            "teamcreate_form": team_form,
        }, context_instance=RequestContext(request))
    
def teams(request, template_name="teams/teams.html"):
    teams = Team.objects.all()
    return render_to_response(template_name, {
            "teams": teams,
        }, context_instance=RequestContext(request))

@login_required
def my_teams(request, template_name="teams/teams.html"):
    return render_to_response(template_name, {
        "teams": Team.objects.filter(members=request.user).order_by("name"),
    }, context_instance=RequestContext(request))
        
def try_join(request,team_slug=None):
    team = get_object_or_404(Team, slug=team_slug)

    if request.user.is_authenticated():
        if not team.user_is_member(request.user):
            messages.add_message(request, messages.SUCCESS, ugettext("Team %(team_name)s has been notified that you want to join them. They will either accept or decline your request.") % {"team_name": team.name})
            teamjoin, created = TeamJoinNotification.objects.get_or_create (user=request.user,team=team,status=0) #0 = user wants to join team
            
            if created: 
                notification = teamjoin.notification.create(sender=request.user,reciever_team=team)
                notification.associate_group(team)
            else: #a notification exists, that's a double join
                messages.add_message(request, messages.WARNING,
                                     ugettext("You have already requested to join team %(team_name)s") % {
                        "team_name": team.name
                        }
                                     )
        else:
            messages.add_message(request, messages.WARNING,
                                 ugettext("You have already joined team %(team_name)s") % {
                    "team_name": team.name
                    }
                                 )

    else:
        messages.add_message(request, messages.WARNING,"Please log in first.")
    
    return HttpResponseRedirect(reverse("teams_team_detail",kwargs={"team_slug":team.slug}))

def legitimity(request,notif,team_slug):
    team = get_object_or_404(Team, slug=team_slug)
    is_member = False
    if not request.user.is_authenticated():
        is_member = False
    else:
        is_member = team.user_is_member(notif.user)
    return request.user.is_authenticated() and notif.team.slug==team_slug and (not is_member) and (
        (
            #user wants to join team -> the captain should handle
            #check if current user is team captain
            notif.status == 0 and request.user == notif.team.captain()
            )
        or
        (
            #team wants user to join -> the user should handle
            #check if current user is the request.user
            notif.status == 1 and request.user == notif.user
            )
        )
"""
This function is url-mapped, so theoretically, anyone can call it. 
Therefore, we have to check wheather the caller is allowed to confirm membership (either the team's captain or the requested user).
In other cases, it is a hacking attempt.
"""
@login_required
def handle_membership(request,team_slug=None):
    notif_id = request.POST.get("notif", None)
    notif=TeamJoinNotification.objects.get(id=notif_id)
    accept=(request.POST.get("response", None)=="accept")
    if legitimity(request,notif,team_slug):
        notif.save()
        str_status=""
        last_status = notif.status
        if accept:
            notif.team.members.add(notif.user)
            str_status="accepted"
            notif.status = 4 #accepted
        else:
            str_status="declined"
            notif.status = 3 #declined
   
        notif.save()
        if(last_status == 0): #join: user -> team. Team handles. User is notified. No association with group: we don't want this notification to show on the group's profile since it's for the user.
            notification = notif.notification.create(sender_team=notif.team,reciever=notif.user)            
        else: #join: team -> user. User handles. Team is notified. We do an association with the group because we want the notification (user accepted/refused) to show on the group's profile.
            notification = notif.notification.create(sender=notif.user,reciever_team=notif.team)
            notification.associate_group(notif.team)
            
        messages.add_message(request, messages.SUCCESS,
                             ugettext("User %(user)s has been %(str_status)s in the team %(team)s") % {
                "user": notif.user,
                "str_status":str_status,
                "team": notif.team
                }
                             )
    else:
        messages.add_message(request, messages.WARNING,"Please stop hacking the site")

    return HttpResponseRedirect(reverse("interactive_notification_list"))

def team(request, team_slug=None,form_class=InviteUserForm,template_name="teams/team.html"):
    team = get_object_or_404(Team, slug=team_slug)

    if not request.user.is_authenticated():
        is_member = False
    else:
        is_member = team.user_is_member(request.user)

    invite_form = form_class(request.POST or None)
    if request.method == "POST":
        if request.user.is_authenticated():
            if request.user==team.creator:
                if invite_form.is_valid():
                    username = invite_form.cleaned_data["username"]
                    try:
                        user = User.objects.get(username=username)
                        teamjoin, created = TeamJoinNotification.objects.get_or_create (user=user,team=team,status=1) #1 = team wants user to join 
            
                        if created: 
                            notification = teamjoin.notification.create(sender_team=team,reciever=user)
                        #do not associate this to group, it is the user's job to accept
                        else: #a notification exists, that's a double invite
                            messages.add_message(request, messages.WARNING,
                                                 ugettext("You have already invited user %(user_name)s") % {
                                    "user_name": user.username
                                    }
                                                 )

                    except ObjectDoesNotExist:
                        messages.add_message(request, messages.WARNING,"The user you specified doesn't exist")

    return render_to_response(template_name, {
        "team": team,
        "invite_form": invite_form,
        "group" : team,
        "is_member" : is_member,
    }, context_instance=RequestContext(request))

def delete(request,team_slug=None):
    team = get_object_or_404(Team, slug=team_slug)
    if not redirect_url:
        redirect_url = reverse("team_list")
    
    # @@@ eventually, we'll remove restriction that tribe.creator can't leave
    # tribe but we'll still require tribe.members.all().count() == 1
    if (request.user.is_authenticated() and team.method == "POST" and
            team.user == team.creator and team.members.all().count() == 1):
        team.delete()
        messages.add_message(request, messages.SUCCESS,
            ugettext("Team %(team_name)s deleted.") % {
                "team_name":team.name
            }
        )
        # no notification required as the deleter must be the only member
    
    return HttpResponseRedirect(redirect_url)

