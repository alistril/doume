from django.conf.urls.defaults import *

from teams.models import Team

from groups.bridge import ContentBridge


bridge = ContentBridge(Team,"teams")



urlpatterns = patterns("teams.views",
    url(r"^$", "teams", name="team_list"),
    url(r"^create/$", "create", name="team_create"),    
    url(r"^my_teams/$", "my_teams", name="team_my_teams"),
    
    # team-specific    
    url(r"^team/(?P<team_slug>[-\w]+)/$", "team", name="teams_team_detail"),
    url(r"^team/(?P<team_slug>[-\w]+)/try_join/$", "try_join", name="teams_team_try_join"),
    url(r"^team/(?P<team_slug>[-\w]+)/handle_membership/$", "handle_membership", name="teams_team_handle_membership"),
    url(r"^team/(?P<team_slug>[-\w]+)/delete/$", "delete", name="teams_team_delete"),
)

urlpatterns += bridge.include_urls("interactive_notification.urls", r"^team/(?P<team_slug>[-\w]+)/interactive_notification/")
urlpatterns += bridge.include_urls("wall.urls", r"^walls/(?P<team_slug>[-\w]+)/wall/")
urlpatterns += bridge.include_urls("dooms.urls", r"^dooms/(?P<team_slug>[-\w]+)/doom/")

