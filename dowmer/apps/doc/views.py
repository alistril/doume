from django.template import RequestContext
from django.shortcuts import render_to_response


import logging
log=logging.getLogger(__name__)

def main(request):
    return render_to_response("doc/main.html", {}, RequestContext(request))