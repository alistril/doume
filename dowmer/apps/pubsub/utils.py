from django.core import serializers
from django.conf import settings
from django.contrib.auth.models import User
from pubsub.core import xmpp_link
from sleekxmpp.xmlstream.stanzabase import ET
import sleekxmpp.plugins.stanza_pubsub as pubsub
import xml.dom.minidom
import uuid

import logging

pubsubutilslog = logging.getLogger(__name__)

def get_node_info(node):
#    <iq type='get'
#        from='francisco@denmark.lit/barracks'
#        to='pubsub.shakespeare.lit'
#        id='meta1'>
#      <query xmlns='http://jabber.org/protocol/disco#info'
#             node='princely_musings'/>
#    </iq>
    return xmpp_link()['xep_0030'].get_info(jid=getattr(settings, 'XMPP_PUBSUB_HOST'),node=node)

def list_nodes():
    return xmpp_link()['xep_0030'].get_items(jid=getattr(settings, 'XMPP_PUBSUB_HOST'))

def publish(node, payload):
    #<iq type='set'
        # from='hamlet@denmark.lit/blogbot'
        # to='pubsub.shakespeare.lit'
        # id='publish1'>
      # <pubsub xmlns='http://jabber.org/protocol/pubsub'>
        # <publish node='princely_musings'>
          # <item id='bnd81g37d61f49fgn581'>
            # <entry xmlns='http://www.w3.org/2005/Atom'>
              # <title>Soliloquy</title>
              # <summary>
    # To be, or not to be: that is the question:
    # Whether 'tis nobler in the mind to suffer
    # The slings and arrows of outrageous fortune,
    # Or to take arms against a sea of troubles,
    # And by opposing end them?
              # </summary>
              # <link rel='alternate' type='text/html'
                    # href='http://denmark.lit/2003/12/13/atom03'/>
              # <id>tag:denmark.lit,2003:entry-32397</id>
              # <published>2003-12-13T18:30:02Z</published>
              # <updated>2003-12-13T18:30:02Z</updated>
            # </entry>
          # </item>
        # </publish>
      # </pubsub>
    # </iq>
    

    iq = xmpp_link().makeIqSet()
    
    packet = pubsub.Publish()
    packet['node'] = node
    item = pubsub.Item()
    item['id'] = uuid.uuid1().urn
    
    entry = ET.fromstring('<entry xmlns="http://www.w3.org/2005/Atom"></entry>')
    entry.text = str(payload)
    item['payload'] = entry
    packet.append(item)
    iq['pubsub'].append(packet)
    return iq.send()

def get_nodes_config_default():
    # <iq type='get'
        # from='hamlet@denmark.lit/elsinore'
        # to='pubsub.shakespeare.lit'
        # id='def1'>
      # <pubsub xmlns='http://jabber.org/protocol/pubsub#owner'>
        # <default/>
      # </pubsub>
    # </iq>    
    packet = pubsub.PubsubOwner()
    packet.append(ET.Element('default'))
    iq = xmpp_link().makeIqGet()
    iq.append(packet)

    return iq.send()
    
def create_node(node):
    pubsubutilslog.info("creating node " + node);
    # <iq type='set'
        # from='hamlet@denmark.lit/elsinore'
        # to='pubsub.shakespeare.lit'
        # id='create1'>
        # <pubsub xmlns='http://jabber.org/protocol/pubsub#owner'>
            # <create node='princely_musings'/>
        # </pubsub>
    # </iq>
    pubsub_host = getattr(settings, 'XMPP_PUBSUB_HOST',"pubsub.localhost")
    xmpp_link()['xep_0060'].create_node(pubsub_host,node)
    
def subscribe(user,node):
    pubsub_host = getattr(settings, 'XMPP_PUBSUB_HOST',"pubsub.localhost")
    xmpp_host = getattr(settings,"XMPP_HOST","localhost")
    xmpp_link()['xep_0060'].subscribe(pubsub_host,node,subscribee="%s/%s" % (user,xmpp_host))

def get_subscriptions_list():
    packet = pubsub.Pubsub()
    subscriptions = pubsub.Subscriptions()
    packet.append(subscriptions)
    
    iq = xmpp_link().makeIqGet()
    
    iq.append(packet)
    
    return iq.send()
    
def get_subscriber_list_for_node(node):
    # <iq type='get'
    # from='hamlet@denmark.lit/elsinore'
    # to='pubsub.shakespeare.lit'
    # id='subman1'>
  # <pubsub xmlns='http://jabber.org/protocol/pubsub#owner'>
    # <subscriptions node='princely_musings'/>
  # </pubsub>
# </iq>
    
    packet = pubsub.PubsubOwner()
    subscriptions = pubsub.OwnerSubscriptions()
    subscriptions.attrib['node'] = node 
    packet.append(subscriptions)
    
    iq = xmpp_link().makeIqGet()
    
    iq.append(packet) 
    
    subs = iq.send()
    dom = xml.dom.minidom.parseString(str(subs['pubsub_owner']['subscriptions']))
    ret = []
    for elem in dom.getElementsByTagName("subscription"):
        ret.append(pubsub.Subscription(ET.fromstring(elem.toxml())))
        
    return ret
    
def subscribe_manual(jid,node):
    # <iq type='set'
    # from='hamlet@denmark.lit/elsinore'
    # to='pubsub.shakespeare.lit'
    # id='subman2'>
  # <pubsub xmlns='http://jabber.org/protocol/pubsub#owner'>
    # <subscriptions node='princely_musings'>
      # <subscription jid='bard@shakespeare.lit' subscription='subscribed'/>
    # </subscriptions>
  # </pubsub>
# </iq>
    packet = pubsub.PubsubOwner()
    subscriptions = ET.fromstring("""<subscriptions node='%s'>
<subscription jid='%s' subscription='subscribed'/>
</subscriptions>""" % (node,jid) )    
    packet.append(subscriptions)
    
    iq = xmpp_link().makeIqSet()
    
    iq.append(packet)
    
    return iq.send()

   
def delete(node):
    # <iq type='set'
        # from='hamlet@denmark.lit/elsinore'
        # to='pubsub.shakespeare.lit'
        # id='delete1'>
        # <pubsub xmlns='http://jabber.org/protocol/pubsub#owner'>
            # <delete node='princely_musings'/>
        # </pubsub>
    # </iq>
    packet = ET.Element('{http://jabber.org/protocol/pubsub#owner}pubsub')    
    packet.append(ET.fromstring('<delete node="%s"/>' % node))
    iq = xmpp_link().makeIqSet()
    iq.append(packet)    
    
    return iq.send()


def join_room(nick):    
    xmpp_link()['xep_0045'].joinMUC("room@%s" % getattr(settings, 'MUC_XMPP_HOST'),nick)    
    
