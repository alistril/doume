from django.utils import simplejson
from pubsub.utils import subscribe_manual
from dajaxice.decorators import dajaxice_register
from friends.models import FriendshipInvitation,Friendship
from django.contrib.auth.models import User


import logging
pubsublogger = logging.getLogger(__name__)

@dajaxice_register
def subscribe_to_friend(request,jid,friend): 
    pubsublogger.debug("AJAX: subscribe_to_friend")
    pubsublogger.debug("AJAX: jid=" + jid + ", friend=" + friend)
    ret = {
            "jid" : jid,
            "friend" : friend
        }    
    if request.user.is_authenticated():
        pubsublogger.debug("AJAX: auth OK")
        friendships = Friendship.objects.friends_for_user(request.user)        
        try:
            for friendship in friendships:  
                pubsublogger.debug(friendship["friend"].username)
                if friendship["friend"].username == friend:
                    pubsublogger.info("subscribing to " + friend + "/friends...")            
                    subscribe_manual(jid,"/"+friend+"/friends")
        
            ret.update({"type" : "success"})
        except Exception as inst:
            pubsublogger.error("Error: %s",str(inst))
            ret.update({"type" : "error_internal"})
    else:
        ret.update({"type" : "error"})
        
    return simplejson.dumps(ret)

@dajaxice_register
def subscribe_to_doom(request,jid,doomid):
    pubsublogger.debug("AJAX: subscribe_to_doom")
    
    ret = {
        "jid" : jid,
        "doomid" : doomid
        }    
    if request.user.is_authenticated():
        try:            
            pubsublogger.info("subscribing to doom #" + str(doomid))        
            subscribe_manual(jid,"/dooms/" + str(doomid))
            ret.update({"type" : "success"})
        except Exception as inst:
            pubsublogger.error("Error: %s",str(inst))
            ret.update({"type" : "error_internal"})
    else:
        ret.update({"type" : "error"})

    return simplejson.dumps(ret)

@dajaxice_register
def subscribe_to_category(request,jid,catid):
    pubsublogger.debug("AJAX: subscribe_to_category")
    
    ret = {
            "jid" : jid,
            "catid" : catid
            }    
    if request.user.is_authenticated():
        try:            
            pubsublogger.info("subscribing to category #" + str(catid))        
            subscribe_manual(jid,"/categories/" + str(catid))
            ret.update({"type" : "success"})
        except Exception as inst:
            pubsublogger.error("Error: %s",str(inst))
            ret.update({"type" : "error_internal"})
    else:
        ret.update({"type" : "error"})

    return simplejson.dumps(ret)

