from django.conf import settings
import logging

from sleekxmpp import ClientXMPP
#from sleekxmpp.exceptions import IqError, IqTimeout

log = logging.getLogger(__name__)

class DoumeClient(ClientXMPP):

    def __init__(self, jid, password):
        ClientXMPP.__init__(self, jid, password)

        self.add_event_handler("session_start", self.session_start)
        self.add_event_handler("pubsub", self.pubsub)

        self.register_plugin('xep_0060') # pubsub
        self.register_plugin('xep_0045') # muc
        self.register_plugin('xep_0030') # Service Discovery

        # If you are working with an OpenFire server, you will
        # need to use a different SSL version:
        # import ssl
        # self.ssl_version = ssl.PROTOCOL_SSLv3

    def session_start(self, event):
        log.info( "session_start")
        self.send_presence()
        self.get_roster()

        #self.send_presence(
        #                      pto='room@%s/%s' % (getattr(settings, 'MUC_XMPP_HOST'),"doume"), pfrom='room@%s/%s' % (getattr(settings, 'XMPP_HOST'),"doume")                              
        #           
    
    def makeIqGet(self,*args,**kwargs):
        iq = super(DoumeClient,self).makeIqGet(*args,**kwargs)
        iq['to'] = getattr(settings, 'XMPP_PUBSUB_HOST')
        iq['from'] = self.boundjid.full
        return iq
    
    def makeIqSet(self,*args,**kwargs):
        iq = super(DoumeClient,self).makeIqSet(*args,**kwargs)
        iq['to'] = getattr(settings, 'XMPP_PUBSUB_HOST')
        iq['from'] = self.boundjid.full
        return iq
             
    def pubsub(self, data):
        log.info("RECIEVED PUBSUB MSG!!")
        log.info(data)

def establish_xmpp_link():
    # Ideally use optparse or argparse to get JID,
    # password, and log level.
    log.info( "establish_xmpp_link")
    from_jid = getattr(settings, 'XMPP_JID')
    passwd = getattr(settings, 'XMPP_PASSWORD')

    xmpp_client = DoumeClient(from_jid, passwd)

    if xmpp_client.connect():
        xmpp_client.process(threaded=True)

    settings.xmpp_client = xmpp_client
    return xmpp_client
    
            
def xmpp_link():
    #establish_xmpp_link() 
    xmpp_client = getattr(settings,"xmpp_client",None)
    if xmpp_client is None:
        return establish_xmpp_link()        
    else:    
        return settings.xmpp_client
    

