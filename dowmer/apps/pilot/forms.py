from django import forms
from django.forms import Form,ModelForm

from dooms.models import Doom
from django.utils.translation import ugettext_lazy as _, ugettext
from django.conf import settings
from dooms.competitions.models import Competition
from pubsub.utils import create_node,delete

class XMPPAttributesForm(forms.Form):
    host = forms.CharField()    
    pubsub_host = forms.CharField()
    resource = forms.CharField()        
    bosh_service = forms.CharField()
    bosh_port = forms.IntegerField()
    
class XMPPConnectForm(forms.Form):
    jid = forms.CharField()
    password = forms.CharField()
    
class PubsubNodeOpForm(forms.Form):
    node = forms.CharField(initial="/everyone")
    def save(self):
        if "create" in self.data:
            create_node(self.cleaned_data['node'])
        elif "delete" in self.data:
            delete(self.cleaned_data['node'])


class PubsubNodeForm(forms.Form):
    node_count = forms.CharField(widget=forms.HiddenInput())
    def __init__(self, *args, **kwargs):
        nodes = kwargs.pop('nodes', 0)

        super(PubsubNodeForm, self).__init__(*args, **kwargs)
        self.fields['node_count'].initial = len(nodes)

        for node in nodes:            
            self.fields[node] = forms.BooleanField(label=node,required=False)

    def save(self,method="create"):
        for key, value in self.fields.items():            
            if self.cleaned_data[key]==True:
                if method == "create":
                    create_node(key)
                elif method == "delete":
                    delete(key)

class CompetitionConfigForm(ModelForm):
    class Meta:
        model = Competition
        fields = ('finished','frozen')