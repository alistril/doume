from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response

from pubsub.utils import get_nodes_config_default,list_nodes
from pubsub.core import establish_xmpp_link

from pilot.forms import XMPPConnectForm,XMPPAttributesForm,CompetitionConfigForm,PubsubNodeForm,PubsubNodeOpForm
from pilot.helpers import is_connected
from dooms.competitions.models import Competition
import xml.dom.minidom 
from django.core.urlresolvers import reverse
from django.conf import settings

import logging
from django.http import HttpResponseRedirect
from django.contrib.auth.models import User
from dooms.models import Doom

log = logging.getLogger(__name__)

@login_required
def home(request,template_name="pilot/base.html"): 
    if request.user.username!="doume":
        return render_to_response("pilot/error.html", {},  RequestContext(request))
     
    jid = getattr(settings, 'XMPP_JID')
    passwd = getattr(settings, 'XMPP_PASSWORD')
    
    if request.method == "POST":
        xmpp_connect_form = XMPPConnectForm(request.POST)
        xmpp_attributes_form = XMPPAttributesForm(request.POST)
        if xmpp_connect_form.is_valid():
            jid = xmpp_connect_form.cleaned_data["jid"]
            passwd = xmpp_connect_form.cleaned_data["password"]
            setattr(settings, 'XMPP_JID',jid)
            setattr(settings, 'XMPP_PASSWORD',passwd)
            establish_xmpp_link()   

        if xmpp_attributes_form.is_valid():
            setattr(settings, 'XMPP_HOST',xmpp_attributes_form.cleaned_data["host"])
            setattr(settings, 'XMPP_BOSH_PORT',xmpp_attributes_form.cleaned_data["bosh_port"])
            setattr(settings, 'XMPP_PUBSUB_HOST',xmpp_attributes_form.cleaned_data["pubsub_host"])
            setattr(settings, 'XMPP_RESOURCE',xmpp_attributes_form.cleaned_data["resource"])
            setattr(settings, 'BOSH_SERVICE',xmpp_attributes_form.cleaned_data["bosh_service"])
            
            
    context = {"connected":is_connected()}
    context.update({"xmpp_connect_form":XMPPConnectForm(initial={"jid": getattr(settings, 'XMPP_JID'),"password":getattr(settings, 'XMPP_PASSWORD')})})
    context.update({"xmpp_attributes_form":
                        XMPPAttributesForm(initial={
                                                        "host":         getattr(settings, 'XMPP_HOST'),
                                                        "bosh_port":    getattr(settings, 'XMPP_BOSH_PORT'),
                                                        "pubsub_host":  getattr(settings, 'XMPP_PUBSUB_HOST'),
                                                        "resource":     getattr(settings, 'XMPP_RESOURCE'),
                                                        "bosh_service": getattr(settings, 'BOSH_SERVICE')
                                                    })
                    })
    return render_to_response(template_name, context,  RequestContext(request))

@login_required
def pubsub_nodes(request,template_name="pilot/pubsub_nodes.html"):
    node_config_str = str( get_nodes_config_default() )    
    node_config = xml.dom.minidom.parseString(node_config_str)
    nodes_raw = list_nodes()    
    node_items = []
    nodes = []
    
    for item in nodes_raw['disco_items']['items']:
        node_items.append({"host":item[0],"node":item[1]})
        nodes.append(item[1])

    frm_nodes = PubsubNodeForm(nodes=nodes)
    return render_to_response(template_name, {"node_config":node_config_str,"node_items":node_items, "frm_nodes":frm_nodes,"frm_op_node":PubsubNodeOpForm()},  RequestContext(request))

def op_node(request):
    if request.method == "POST":
        frm = PubsubNodeOpForm(request.POST)
        if frm.is_valid():
            frm.save()
        return HttpResponseRedirect(reverse('pilot_pubsub_nodes'))
    else:
        log.error("request method should be POST")
        log.error(request)

@login_required
def missing_nodes(request,template_name="pilot/missing_nodes.html"):    
    node_items = list_nodes()
    patterns = ["/%s/friends","/%s/wall","/%s/only"]
    missing = []
    nodes = [item[1] for item in node_items['disco_items']['items']]    
    
    if not "/everyone" in nodes:
        missing.append("/everyone")
                
    for user in User.objects.all():
        for item in patterns:
            if not ((item % user.username) in nodes):
                missing.append(item % user.username)
                
    for doom in Doom.objects.all():        
        if not (("/dooms/%d" % doom.id) in nodes):
            missing.append("/dooms/%d" % doom.id)
    
    if request.method == "POST":
        frm_nodes = PubsubNodeForm(request.POST,nodes=missing)
        if frm_nodes.is_valid():
            frm_nodes.save(method="create")
            return HttpResponseRedirect(reverse('pilot_pubsub_nodes'))
        else:
            log.error("Missing nodes: Form errors")
            log.error(frm_nodes._errors)
    else:
        frm_nodes = PubsubNodeForm(nodes=missing)
        return render_to_response(template_name, {"frm":frm_nodes},  RequestContext(request))
    
@login_required
def delete_nodes(request):    
    nodes_raw = list_nodes()
    nodes = []
    
    for item in nodes_raw['disco_items']['items']:
        nodes.append(item[1])
    
    if request.method == "POST":
        frm_nodes = PubsubNodeForm(request.POST,nodes=nodes)
        if frm_nodes.is_valid():
            frm_nodes.save(method="delete")
            return HttpResponseRedirect(reverse('pilot_pubsub_nodes'))
        else:
            log.error("Delete nodes: Form errors")
            log.error(frm_nodes._errors)

@login_required
def competitions(request,template_name="pilot/competitions.html"):
    comp_object = Competition.objects.filter(reference=None).order_by('finished')
    competitions = []   
    if request.method=="POST":
        for comp in comp_object:
            frm = CompetitionConfigForm(request.POST,instance=comp,prefix=comp.id)
            if frm.is_valid():
                frm.save()
        
    for object in comp_object:
        competitions.append({"object":object,"form":CompetitionConfigForm(instance=object,prefix=object.id)})
        
    return render_to_response(template_name, {"competitions":competitions},  RequestContext(request))