from django.conf.urls.defaults import *


urlpatterns = patterns("",
    url(r"^$", "pilot.views.home", name="pilot_home"),    
    url(r"^competitions/$", "pilot.views.competitions", name="pilot_competitions"),    
    url(r'^errors/', include('errors.urls')),
    url(r'^pubsub_nodes/$', "pilot.views.pubsub_nodes", name="pilot_pubsub_nodes"),
    url(r'^pubsub_nodes/missing_nodes$', "pilot.views.missing_nodes", name="pilot_missing_nodes"),
    url(r'^pubsub_nodes/delete_nodes$', "pilot.views.delete_nodes", name="pilot_delete_nodes"),
    url(r'^pubsub_nodes/op_node', "pilot.views.op_node", name="pilot_op_node"),
)
