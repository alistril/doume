from dooms.models import Doom
from dajaxice.decorators import dajaxice_register

from django.template.loader import render_to_string
from django.template import RequestContext
from pubsub.utils import get_subscriber_list_for_node,get_subscriptions_list,publish

from django.utils import simplejson

import logging


@dajaxice_register
def subscriber_list_for_node(request,node):    
    return render_to_string('pilot/sub_list.html', { 'sub_items': get_subscriber_list_for_node(node)},RequestContext(request))

@dajaxice_register
def subscriptions_list(request):    
    return render_to_string('pilot/sub_list_nodes.html', { 'sub_items': get_subscriptions_list()},RequestContext(request))


@dajaxice_register
def dooms_related(request,competition_id):
    dooms = Doom.objects.filter(competition__id=competition_id).order_by('-modified')
    
    return render_to_string('pilot/doom_list.html', { 'dooms': dooms},RequestContext(request))

@dajaxice_register
def publish_json(request,node,payload):
    return publish(node, payload)