from categories.models import Category
from haystack.query import SearchQuerySet
from dooms.competitions.models import Competition
from dajaxice.decorators import dajaxice_register

from django.template.loader import render_to_string
from django.template import RequestContext
from miscutils import js_context
from django.core.urlresolvers import reverse
from dooms.competitions.forms import competition_form_dict
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from interface.utils import interface_for_user

import logging

log = logging.getLogger(__name__)

@dajaxice_register
def execute(request,search_val,page):
    sqs = SearchQuerySet().filter(content=search_val).models(Competition).filter(finished=0) #end filtering is important otherwise paginator_page.object_list is empty for some reason
    limit = interface_for_user(request.user).competition_search_results_per_page
    paginator = Paginator(sqs, limit)

    try:
        paginator_page = paginator.page(page)
    except (EmptyPage, InvalidPage):
        paginator_page = paginator.page(paginator.num_pages)
    
    return render_to_string('competition_search/competition_search_contents.html', { 'paginator_page': paginator_page, 
                                                                                    'query' : search_val,
                                                                                    'search_count' : sqs.count() 
                                                                                        },RequestContext(request))

@dajaxice_register
def execute_for_specific_competition(request,competition_id):
    id = int(competition_id)
    competition_qs = Competition.objects.filter(id=id)
    
    paginator = Paginator(competition_qs, 5) #the number doesnt matter since there is a single result

    try:
        paginator_page = paginator.page(1)
    except (EmptyPage, InvalidPage):
        paginator_page = paginator.page(paginator.num_pages)

    log.info(paginator_page)
    return render_to_string('competition_search/competition_pseudosearch_contents.html', { 'paginator_page': paginator_page},RequestContext(request))


@dajaxice_register
def execute_as_doom_create_page(request,search_val,page):    
    sqs = SearchQuerySet().filter(content=search_val).models(Competition).filter(finished=0) #end filtering is important otherwise paginator_page.object_list is empty for some reason
    limit = interface_for_user(request.user).competition_search_results_per_page
    paginator = Paginator(sqs, limit)

    try:
        paginator_page = paginator.page(page)
    except (EmptyPage, InvalidPage):
        paginator_page = paginator.page(paginator.num_pages)

    return render_to_string('competition_search/competition_search_as_doom_create_page.html', { 'paginator_page': paginator_page},RequestContext(request))

@dajaxice_register
def competition_configure_html(request,id):
    competition = Competition.objects.get(id=id)
    form_class = competition_form_dict[competition.competition_type.model]
    the_form = form_class(prefix=competition.id,instance=competition.competition_object)
    config = render_to_string('dooms/competitions/competition_configure.html', { 'competition': competition,'competition_form':the_form,"page": True },context_instance=RequestContext(request)).replace("\n","")
    return config

#find doom related to a competition
@dajaxice_register
def related(request,competition_id,page):
    try:
        competition = Competition.objects.get(id=competition_id)
    except Competition.DoesNotExist:
        render_to_string('competition_search/doom_search_contents_by_competition.html', { 'error': True},RequestContext(request))
        
    limit = interface_for_user(request.user).doom_search_results_per_page
    dooms = competition.dooms.filter(on_sale=True,is_active=True).order_by('-modified')
    paginator = Paginator(dooms, limit)
    
    try:
        paginator_page = paginator.page(page)
    except (EmptyPage, InvalidPage):
        paginator_page = paginator.page(paginator.num_pages)
    
    return render_to_string('competition_search/doom_search_contents_by_competition.html', { 
                                                                                            'competition': competition, 
                                                                                            'paginator_page': paginator_page, 
                                                                                            'search_count' : dooms.count() 
                                                                                            },RequestContext(request))