from dooms.competitions.models import Competition
from haystack.query import SearchQuerySet
from haystack.inputs import AutoQuery
from django.template.loader import render_to_string
import logging
from ajax_select import LookupChannel
from interface.utils import interface_for_user

log = logging.getLogger(__name__)

class CompetitionLookup(LookupChannel):
    model = Competition
        
    def get_query(self,q,request):
        log.debug("get_query")                
        sqs = SearchQuerySet().filter(content=AutoQuery(q)).models(Competition)
        
        log.debug(sqs)
                        
        for item in sqs:
            log.debug("item:" + str(item.description) + " -- " + str(item.on_sale) + " -- " + str(item.owner) )
        
        limit = interface_for_user(request.user).competition_autocomeplete_limit
        return sqs[:limit]

    def get_result(self,obj):
        u""" result is the simple text that is the completion of what the person typed """
        return unicode(obj.object)

    def format_match(self,obj):
        """ (HTML) formatted item for display in the dropdown """
        return unicode(obj.object)
        
    def check_auth(self,request):
        return True