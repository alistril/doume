from django import forms
from ajax_select.fields import AutoCompleteSelectField
from django.utils.translation import ugettext_lazy as _, ugettext

class SearchCompetitionForm(forms.Form):
    search_field_competitions = AutoCompleteSelectField('search_competition',label=_("Search competition"), required=False)
