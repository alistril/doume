from competition_search.forms import SearchCompetitionForm

def search_bar(request):
    context = {}
    context.update({"search_bar_competition_form" : SearchCompetitionForm()})
        
    return context
