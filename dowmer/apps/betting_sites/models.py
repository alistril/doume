from django.db import models

class BettingSite(models.Model):
    name = models.CharField(max_length=30)
    url = models.URLField()
    
    def __unicode__(self):
        return self.name