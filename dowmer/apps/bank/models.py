from payments.models import Transaction,SubTransaction
from django.conf import settings
from decimal import Decimal
from django.db.models import Q, Sum 
from django.db.models import Count
from django.core.urlresolvers import reverse

class Account:
    def __init__(self,user=None):
        self.user = user
        self.transactions = Transaction.objects.filter(Q(user_paying=user) | Q(user_paid=user) ).filter(is_active=True).annotate(subtransactions_count=Count("subtransaction"))
        
    def transactions(self):    
        return self.transactions
    
    def transactions_winning(self):
        transactions_users = self.transactions.filter(~Q(user_paid=settings.doume),~Q(user_paying=settings.doume))
        return transactions_users.filter(user_paid = self.user).exclude(user_paying = self.user)
    
    def transactions_losing(self):
        transactions_users = self.transactions.filter(~Q(user_paid=settings.doume),~Q(user_paying=settings.doume))
        return transactions_users.filter(user_paying = self.user).exclude(user_paid = self.user)
    
    def amount_guaranteed(self):
        subtransactions = SubTransaction.objects.filter(Q(parent__user_paying=self.user) | Q(parent__user_paid=self.user))
        transactions_guarantees = subtransactions.filter(user=settings.doume,debit=False).aggregate(Sum("amount"))
        transactions_guarantees_payback = subtransactions.filter(user=settings.doume,debit=True).aggregate(Sum("amount"))
        money_guarantee = (transactions_guarantees["amount__sum"] or Decimal(0)) - (transactions_guarantees_payback["amount__sum"] or Decimal(0))
        return money_guarantee
    
    def amount_usable(self):
        subtransactions_debit = SubTransaction.objects.filter(user=self.user,debit=True).aggregate(Sum("amount"))
        subtransactions_credit = SubTransaction.objects.filter(user=self.user,debit=False).aggregate(Sum("amount")) 
                                                                                                     
        money_remaining =  (subtransactions_credit["amount__sum"] or Decimal(0)) - (subtransactions_debit["amount__sum"]  or Decimal(0))
        return money_remaining

    def amount_total(self):
        return self.amount_usable()+self.amount_guaranteed()
        
    def transactions_for_user(self):    
        transactions_users = self.transactions.filter(~Q(user_paid=settings.doume),~Q(user_paying=settings.doume)).order_by('-datetime')
        return transactions_users
    
    def funds_available(self,amount):
        return amount<=self.amount_usable()
        
    def get_absolute_url(self):
        return reverse("bank_account")