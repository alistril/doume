from django import forms
from django.forms import Form

from dooms.models import Doom
from django.utils.translation import ugettext_lazy as _, ugettext
from django.conf import settings
from decimal import *

class AddFundsForm(forms.Form):
    amount = forms.DecimalField(_("amount"))

class RetrieveFundsForm(forms.Form):
    amount = forms.DecimalField(_("amount"))
