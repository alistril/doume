from django.conf.urls.defaults import *


urlpatterns = patterns("",
    url(r"^account/$", "bank.views.account", name="bank_account"),
    url(r"^add_funds/$", "bank.views.add_funds", name="bank_add_funds"),
    url(r"^retrieve_funds/$", "bank.views.retrieve_funds", name="bank_retrieve_funds"),
)
