from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response
from payments.models import Transaction

from payments.views import pay
from bank.models import Account
from bank.forms import AddFundsForm,RetrieveFundsForm
from django.contrib import messages
from decimal import *
from django.core.urlresolvers import reverse
from django.conf import settings

   
@login_required
def account(request,template_name="bank/account.html"):
    account = Account(request.user)
    status = request.GET.get("status",None)
    if status is None:
        transactions = account.transactions_for_user()
    elif status == "winning":
        transactions = account.transactions_winning()
    elif status == "losing":
        transactions = account.transactions_losing()
    
    return render_to_response(template_name, {
            "transactions" : transactions,
            "money_guarantee" : account.amount_guaranteed(),
            "money_remaining" : account.amount_total(),
            "void" : settings.void
            },
            RequestContext(request))

def add_funds(request,template_name="bank/add_funds.html"):    
    add_funds_form = AddFundsForm()
    
    if request.method == "POST":        
        add_funds_form = AddFundsForm(request.POST)
        if add_funds_form.is_valid():              
            payment_service = request.user.get_profile().paymentservice_active
            transaction = Transaction.objects.create(user_paying=settings.void,user_paid=request.user,doom=None,amount=add_funds_form.cleaned_data["amount"],service=payment_service,redirect_url=reverse("bank_account"))
            return pay(request,transactionid=transaction.id)
        else:
            messages.add_message(request, messages.ERROR, 'Invalid data.')

    
    return render_to_response(template_name, {
            "add_funds_form" : add_funds_form
            },
            RequestContext(request))
    
    
def retrieve_funds(request,template_name="bank/retrieve_funds.html"):
    retrieve_funds_form = RetrieveFundsForm()
    
    if request.method == "POST":        
        retrieve_funds_form = RetrieveFundsForm(request.POST)
        if retrieve_funds_form.is_valid():
            payment_service = request.user.get_profile().paymentservice_active
            transaction = Transaction.objects.create(user_paying=request.user,user_paid=settings.void,doom=None,amount=retrieve_funds_form.cleaned_data["amount"],service=payment_service,redirect_url=reverse("bank_account"))  
                  
            return pay(request,transactionid=transaction.id)
        else:
            messages.add_message(request, messages.ERROR, 'Invalid data.')

    
    return render_to_response(template_name, {
            "retrieve_funds_form" : retrieve_funds_form
            },
            RequestContext(request))