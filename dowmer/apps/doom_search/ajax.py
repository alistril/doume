from categories.models import Category
from haystack.query import SearchQuerySet
from haystack.inputs import AutoQuery
from dooms.models import Doom
from dajaxice.decorators import dajaxice_register

from django.template.loader import render_to_string
from django.template import RequestContext
from miscutils import js_context
from django.core.urlresolvers import reverse
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from interface.utils import interface_for_user


import logging

@dajaxice_register
def execute(request,search_val,page):
    sqs = SearchQuerySet().filter(content=AutoQuery(search_val)).models(Doom).filter(on_sale=1).order_by('-modified')
    limit = interface_for_user(request.user).doom_search_results_per_page    
    
    paginator = Paginator(sqs, limit)
        
    try:
        paginator_page = paginator.page(page)
    except (EmptyPage, InvalidPage):
        paginator_page = paginator.page(paginator.num_pages)
        
    return render_to_string('doom_search/doom_search_contents.html', { 'paginator_page': paginator_page,
                                                                      'query':search_val,
                                                                      'search_count' : sqs.count()},RequestContext(request))

    