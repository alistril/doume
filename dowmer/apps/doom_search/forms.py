from django import forms
from ajax_select.fields import AutoCompleteSelectField
from django.utils.translation import ugettext_lazy as _, ugettext

class SearchDoomForm(forms.Form):
    search_field_dooms = AutoCompleteSelectField('search_doom',label=_("Search doom"), required=False)
