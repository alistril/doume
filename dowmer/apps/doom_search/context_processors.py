from doom_search.forms import SearchDoomForm

def search_bar(request):
    context = {}
    
    context.update({"search_bar_doom_form" : SearchDoomForm()})
    
    return context
