from dooms.models import Doom
from haystack.query import SearchQuerySet
from haystack.inputs import AutoQuery
from django.template.loader import render_to_string
import logging
from ajax_select import LookupChannel
from interface.utils import interface_for_user


log = logging.getLogger(__name__)

class DoomLookup(LookupChannel):

    def get_query(self,q,request):
        """ return a query set.  you also have access to request.user if needed """
        log.debug("get_query")
        if request.user.is_authenticated():
            sqs = SearchQuerySet().filter(content=AutoQuery(q)).models(Doom).exclude(owner=request.user).filter(on_sale=1)
        else:
            sqs = SearchQuerySet().filter(content=AutoQuery(q)).models(Doom).filter(on_sale=1)
        log.debug(sqs)
                        
        for item in sqs:
            log.debug("item:" + str(item.description) + " -- " + str(item.on_sale) + " -- " + str(item.owner) )
        
        limit = interface_for_user(request.user).doom_autocomeplete_limit
        return sqs[:limit]


    def get_result(self,res):
        log.debug("get_result")
        """ the search results display in the dropdown menu.  may contain html and multiple-lines. will remove any |  """        
        return render_to_string('dooms/doom_preview_menu.html', { 'object': res.object})
        
    def format_match(self,obj):
        """ (HTML) formatted item for display in the dropdown """
        return render_to_string('dooms/doom_preview_menu.html', { 'object': obj.object})

    def format_item_display(self,res):
        log.debug("format_item_display")
        """ the display of a currently selected object in the area below the search box. html is OK """        
        return render_to_string('dooms/doom_preview_selectable.html', { 'object': res.object, 'hide_text': True})

    def check_auth(self,request):
        return True