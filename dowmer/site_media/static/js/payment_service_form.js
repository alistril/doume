define(["jquery",
        "constants", 
        "jqote2/jquery.jqote2.min",
        'jquery.form/jquery.form'
        ],function($,constants){
	$(document).ready(function() {     
		$.facebox.settings.closeImage = '/site_media/static/js/facebox/src/closelabel.png';
		$.facebox.settings.loadingImage = '/site_media/static/js/facebox/src/loading.gif';
		$('.payment-service-form').ajaxForm(
				{
					dataType:  'json',
					beforeSubmit: function(){
						$.facebox.settings.closeOnClick = false;
						$.facebox(function(){});
					},
					success:
						function(data) {
						$.facebox.settings.closeOnClick = true;
						switch(data.transfer){
						case constants.DoomTransfer.TRANSACTION_REDIRECT:
							$.facebox($("#transaction_redirect").jqote(data));                    	
							window.location.href = data.redirect_url;
							break;
						case constants.DoomTransfer.TRANSACTION_COMPLETE:
							$.facebox($("#transaction_complete_redirect").jqote(data));
							window.location.href = data.redirect_url;
							break;
						case constants.Doomconstants.Transfer.TRANSACTION_FAILED_AND_REDIRECT:
							$.facebox($("#transaction_failed_insufficient_funds_redirect").jqote(data));                    	
							setTimeout(function(){
								window.location.href = data.redirect_url;
							},3000);
							break;
						}
					},
					error:function(){
						$.facebox.settings.closeOnClick = true;
						$.facebox($("#select_payment_service_config_failed").jqote());                
					}
				});     

	}); 
});