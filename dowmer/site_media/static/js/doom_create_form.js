define(['jquery', 
        'page_state',
        'console',
        'constants',
        'init_js_common_functions_static',
        'competition_configure/config',
        'ajax_select/ajax_select',
        'urls',
        'jqote2/jquery.jqote2.min',//for jqote templates AND adapt function
        'jquery.form/jquery.form'
        ], function($,page_state,myconsole,constants,common_fn,config,ajax_select,urls){
	 function adapt_forms_to_status(url_general_create){
	    if(config.competitions_page.status){
	    	$("#work-panel form #luthor-logic").show();
	        $("#work-panel form").attr("action",url_general_create + "?check_subforms=False");
	    }else{
	    	$("#work-panel form #luthor-logic").hide();
	        $("#work-panel form").attr("action",url_general_create);
	    }
	    if(config.competitions_menu.status){
	    	$("#title-bar form #luthor-logic").show();
	        $("#title-bar form").attr("action",url_general_create + "?menu=True&check_subforms=False");
	    }else{
	    	$("#title-bar form #luthor-logic").hide();
	        $("#title-bar form").attr("action",url_general_create + "?menu=True");
	    }
	}

	return { 
		ajaxify_form : 
		function(cls){
			$(document).ready(function() {
				$.facebox.settings.closeImage = '/site_media/static/js/facebox/src/closelabel.png';
				$.facebox.settings.loadingImage = '/site_media/static/js/facebox/src/loading.gif';
				$('.create_doom_form.' + cls).each(function(){
					var theform = $(this);
					$(this).ajaxForm(
							{
								dataType:  'json',
								beforeSubmit : function(){
									$('input[type="submit"][name="create"]',theform).attr('disabled', 'disabled');
								},
								beforeSerialize :
									function(truc){
									$.facebox.settings.closeOnClick = false;
									$.facebox(function(){});
									if( (theform.parents("#work-panel")).length ){
										config.choose_competitions(true).update_logic();
									}else{
										config.choose_competitions(false).update_logic();
									}
									if($(document.activeElement).attr("id")=="id_page-competition_text"){
										$.facebox.settings.closeOnClick = true;
										var search_val = $("#work-panel .ui-autocomplete-input").val();
										page_state.page=1;
										page_state.search.init(Dajaxice.competition_search.execute_as_doom_create_page,
												function(data){
											$("#id_page-search_field_competitions_text").autocomplete('close');
											$("#doomsearch-panel").html(data);
											$('#work-panel #doomsearch-panel').show();
											common_fn.setup_facebox();
											$("span.timeago").timeago();
										},
										{'search_val':search_val});
	
										Dajaxice.competition_search.execute_as_doom_create_page(
												function(data){
													page_state.search.callback(data);
													$(document).trigger('close.facebox');
												},
												{'search_val':search_val,'page':page_state.page}
										);
										return false;
									}									
								},
								success:
									function(data) {
									$.facebox.settings.closeOnClick = true;
									switch(data.transfer){
									case constants.DoomTransfer.TRANSACTION_COMPLETE:
										$.facebox($("#doom_create_success_facebox").jqote(data));
										$('.create_doom_form').resetForm();
										
										if(this.url.match(/([a-zA-Z0-9]|\-|\/)+\?([a-zA-Z0-9]|\-|\/|\&)*menu=True/gi)){
											$("#title-bar .create_doom_form  #luthor-preview  #luthor-preview-content").html("");
										}else{
											$("#work-panel .create_doom_form  #luthor-preview  #luthor-preview-content").html("");
										}
										if( (theform.parents("#work-panel")).length ){
											config.choose_competitions(true).reset();
											adapt_forms_to_status(urls.resolve("doom_create"));
											ajax_select.clear("#id_page-competition");											
										}else{
											config.choose_competitions(false).reset();
											adapt_forms_to_status(urls.resolve("doom_create"));
											ajax_select.clear("#id_competition");
										}
										break;
									case constants.DoomTransfer.TRANSACTION_FAILED_AND_REDIRECT:
										if(data.message_type==constants.DoomTransferDetail.NOT_LOGGED_IN){
											$.facebox($("#redirect_to_login_facebox").jqote(data));
											window.location.href = urls.resolve("login",{next:common_fn.prefix_slash(window.location.pathname)});
										}else if(data.message_type==constants.DoomTransferDetail.NO_PAYMENT_SERVICE){
											$.facebox($("#redirect_to_payment_config_facebox").jqote(data));
											window.location.href = data.redirect_url;
										}                        
										break;
									case constants.DoomTransfer.TRANSACTION_FAILED:
										if(data.message_type==constants.DoomTransferDetail.INVALID_DATA){
											$.facebox($("#form_error_facebox").jqote(data.form_errors));
										}else{
											$.facebox($("#doom_create_fail_page").jqote(data));
										}
										break;
									case constants.DoomTransfer.TRANSACTION_REDIRECT:
										$.facebox($("#doom_create_transaction_redirect_facebox").jqote(data));                    	
										window.location.href = data.redirect_url;
										break;
									case constants.DoomTransfer.PREVIEW:
										if(this.url.match(/([a-zA-Z0-9]|\-|\/)+\?([a-zA-Z0-9]|\-|\/|\&)*menu=True/gi)){
											$("#title-bar .create_doom_form  #luthor-preview  #luthor-preview-content").html(data.html);
										}else{
											$("#work-panel .create_doom_form  #luthor-preview  #luthor-preview-content").html(data.html);
										}	
										
										if(data.warning){
											$.facebox($("#doom_create_compiler_warnings").jqote({data:data.warning}));
											$.facebox.settings.closeOnClick = true;
										}else{
											$(document).trigger('close.facebox');
										}
										break;
									}
									$('input[type="submit"][name="create"]',theform).removeAttr('disabled');									
								},
								error : function(data){
									$.facebox.settings.closeOnClick = true;
									$('input[type="submit"][name="create"]',theform).removeAttr('disabled');
									
									$.facebox($("#doom_sell_unknown_fail_page").jqote({data:data.responseText}));
								}
							});
				});
				
			});
		},
		adapt_forms_to_status : adapt_forms_to_status
	};
});
