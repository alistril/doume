define(["jquery","competition_configure/common"],function($,comp_common){
	function update_accordeon(competition,object){
		if(competition.use_score){
			comp_common.accordeon_unset(".outcome",object);
			comp_common.accordeon_subselect(".score-detail",object).text("[" + competition.side1 + " " + competition.score1 + " - " + competition.score2 + " " + competition.side2 + "]");
		}else{
			comp_common.accordeon_unset(".score-detail",object);
			if(competition.winner && competition.loser)
				comp_common.accordeon_subselect(".outcome",object).text("[" + competition.winner + " wins]");
			else if(competition.tie)
				comp_common.accordeon_subselect(".outcome",object).text("[Tie]");
			else
				comp_common.accordeon_subselect(".score-detail",object).text("[Competition error]");
		}
		
		competition.update_logic();
	}
	
	function parse_score(score){
		var parts = score.match(/[0-9]+/gi);
		if (parts==undefined || parts.length!=2) throw "bad format";
		var score1 = parseInt(parts[0]);
		var score2 = parseInt(parts[1]);
		return [score1, score2];
	}

	function TwoSidesWithScores(){		
		
	}
	
	TwoSidesWithScores.prototype = new comp_common.Competition();
	
	TwoSidesWithScores.prototype.setup_env = function(comp_dict,competition_form_prefix){
		var member1_name_parts = comp_dict.side1.match(/[a-zA-Z]+[a-zA-Z0-9]*/gi);
		var member2_name_parts = comp_dict.side2.match(/[a-zA-Z]+[a-zA-Z0-9]*/gi);
		
		this.side1 = member1_name_parts.join("").toUpperCase();
		this.side2 = member2_name_parts.join("").toUpperCase();
		
		this.form.score = "#id_"+competition_form_prefix+"-score";
		this.form.other_score = "#id_"+competition_form_prefix+"-other_score";
		
		$(".side1_id_string",this.container).text(member1_name_parts.join("").toUpperCase());
		$(".side2_id_string",this.container).text(member2_name_parts.join("").toUpperCase());
	}
	
	TwoSidesWithScores.prototype.init = function(master,comp_dict,competition_form_prefix,page){		
		comp_common.Competition.prototype.init.call(this,master,competition_form_prefix,page);
		
		this.side1 = undefined;
		this.side2 = undefined;
		
		this.score1 = undefined;
		this.score2 = undefined;
		
		this.use_score = false;	
		
		this.form = {
			score: undefined,
			other_score : undefined,
			best_player : undefined
		};
		
		this.setup_env(comp_dict, competition_form_prefix)
		
		var competition = this;
		
		competition.container.on("keyup",this.form.other_score,function(event){
			try{						
				var parts = parse_score($(competition.form.other_score,competition.container).val());
				competition.score1 = parts[0];
				competition.score2 = parts[1];				
				competition.use_score = true;
				$(competition.form.score,competition.container).val(competition.score1 + "-"+competition.score2);
				update_accordeon(competition,this);
			}catch(err){}
		});
		
		competition.container.on("click",".jqui-parent.score-detail #quick-scores a",function(event){
			try{
				event.preventDefault();
				var parts = parse_score($(this).attr("id"));
				
				competition.score1 = parts[0];
				competition.score2 = parts[1];
				competition.use_score = true;
				$(competition.form.score,competition.container).val(competition.score1 + "-"+competition.score2);
				update_accordeon(competition,this);
			}catch(err){}
		});
		
		competition.container.on("click",".qualitative-list li",function(event){
			//event.preventDefault();
			if(parseInt($(this).attr("id"))==4){ //specify score
				competition.use_score = true;						
			}else{
				competition.use_score = false;
				switch( parseInt($(this).attr("id"))){
					case 0: //side 1 wins
						competition.winner = competition.side1;
						competition.loser = competition.side2;
						competition.tie = undefined;
						update_accordeon(competition,this);
						break;
					case 1: //side 2 wins
						competition.winner = competition.side2;
						competition.loser = competition.side1;
						competition.tie = undefined;
						update_accordeon(competition,this);
						break;
					case 2: //tie
						competition.tie = true;
						competition.winner = undefined;
						competition.loser = undefined;
						update_accordeon(competition,this);
						break;
				}
			}
		});
	}
	
	TwoSidesWithScores.prototype.update_logic = function(){
		if(this.use_score){
			var score1_name = this.master+"."+this.side1+".score";
			var score2_name = this.master+"."+this.side2+".score";
			
			this.logic = score1_name+"="+this.score1 + " and " + score2_name+"="+this.score2;
		}else{			
			if(this.winner && this.loser)
				this.logic = this.master+"."+this.winner+".score" + ">" + this.master+"."+this.loser+".score";
			else if(this.tie)
				this.logic = this.master+"."+this.side1+".score" + "=" + this.master+"."+this.side2+".score";
			else
				this.logic = undefined ;
		}
		
		return this.logic;		
	}
	
	TwoSidesWithScores.prototype.help_dictionary = function(comp_dict){
		return {
			side1_name:comp_dict.side1,
			side2_name:comp_dict.side2,
			id_master:this.master,
			side1_code:this.side1,
			side2_code:this.side2
		};
	}
	
	
	return {
			TwoSidesWithScores : TwoSidesWithScores,
			parse_score : parse_score,
			update_accordeon : update_accordeon
		};
});