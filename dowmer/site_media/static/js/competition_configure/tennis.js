define(["jquery",
        "competition_configure/twosideswithscores",
        "competition_configure/config"],
        function($,twosideswithscores,config){	
	function Tennis(){
		
	}
	
	Tennis.prototype = new twosideswithscores.TwoSidesWithScores();
	
	//no need for update_logic
	
	return {
		configure : function(id_master,competition,page,competition_form_prefix){    
			$(document).ready(function(){				
				//init and configure object (automatic configure from config package)
				var tennis = new Tennis();
				config.configure(tennis,id_master,competition,page,competition_form_prefix);

				//configure help using the competition dictionary
				tennis.help = $("#tennis_competition_config_help",tennis.container).jqote( tennis.help_dictionary(competition) );
				
				//toggle help or config, whichever is active
				tennis.refresh();
			});
		}
	};
});