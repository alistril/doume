define(["jquery","console"],function($,myconsole){	

	var competition_element = function(){
		this.id = undefined;
	}
	
	competition_element.prototype.init = function(id,page){
		this.id = id;		

		if(page){				
			this.container = $(".competition-item#" + id, $("#work-panel #id_page-competition_on_deck"));					
		}else{				
			this.container = $(".competition-item#" + id, $("#title-bar #id_competition_on_deck"));
		}
		this.config = $("#competition-content",this.container).html();
		//this.status = false;
	}
	
	competition_element.prototype.update_config = function(){		
		this.config = $("#competition-content",this.container).html();
	}
	
	competition_element.prototype.show_help = function(){		
		$("#competition-content",this.container).html(this.help);		
	}
	
	competition_element.prototype.show_config = function(){		
		$("#competition-content",this.container).html(this.config);		
	}
	
	competition_element.prototype.refresh = function(){		
		if(this.status)
			this.show_help();
		else
			this.show_config();
	}
	
	competition_element.prototype.toggle_help_config = function(){
		this.status = !this.status;
		this.refresh();
	}
	
	competition_element.prototype.update_logic = function(){
		myconsole.log("NOT IMPLEMENTED");
	}
	
	var competition_config = function(logic_field){
		this.list = []; //list of competition_element
		this.logic_field = logic_field;
		this.status = false;
	}
	
	competition_config.prototype.reset = function(){
		this.status = false;
		this.list = [];				
	}

	competition_config.prototype.update_logic = function(){ 
		if(this.status) return;
		var list=[];
		$.each( this.list, function(){			
			this.update_logic();
			if(this.logic!=undefined && this.logic.length>0) list.push(this.logic);
		});
		$(this.logic_field).val(list.join(" and "));		
	}

	competition_config.prototype.add = function(element){		
		$.each( this.list, function(){
			if(this.id==element.id)
				throw "element already exists!";
		});
		if(!element.id)
			throw "element has no id!";
		this.list.push(element);
	}
	
	competition_config.prototype.remove = function(id){		
		for(var i in this.list){
			if(parseInt(this.list[i].id) == parseInt(id)){
				this.list.splice(i,1);
				return;
			}
		}
		throw "could not find id " + id;
	}
	
	competition_config.prototype.show_help = function(){		
		$.each( this.list, function(){
			this.show_help();			
		});		
	}
	
	competition_config.prototype.show_config = function(){		
		$.each( this.list, function(){
			this.show_config();
		});		
	}
	
	competition_config.prototype.refresh = function(){
		$.each( this.list, function(){
			this.refresh();
		});
	}
	
	competition_config.prototype.toggle_help_config = function(){		
		if(!this.status) this.update_logic();
		this.status = !this.status;
		
		$.each( this.list, function(){
			this.toggle_help_config();
		});
	}
	
	competition_config.prototype.set_status = function(status){
		this.status = status;
		if(!this.status) this.update_logic();
		$.each( this.list, function(){
			this.status = status;
			this.refresh();
		});
	}
	
	var competitions_page = new competition_config("#id_page-combination_logic");
	var competitions_menu = new competition_config("#id_combination_logic");
	function choose_competitions(page){ return page ? competitions_page : competitions_menu;}
	return {
		competitions_page : competitions_page,
		competitions_menu : competitions_menu,
		choose_competitions : choose_competitions,
		competition_element : competition_element,
		configure : function(competition,id_master,comp_dict,page,competition_form_prefix){
			var competitions = choose_competitions(page);
			competition.init(id_master,comp_dict,competition_form_prefix,page);
						
			competition.update_config();

			competition.status = competitions.status;
			competitions.add(competition);				

		}
	}
});
