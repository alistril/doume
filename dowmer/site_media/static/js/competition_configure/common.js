define(["jquery",
        "competition_configure/config",
        "jqueryui/jquery.ui.accordion"],function($,config){
	function clear_form_elements(ele){
		
	    $(ele).find(':input').each(function() {
	        switch(this.type) {
	            case 'password':
	            case 'select-multiple':
	            case 'select-one':
	            case 'text':
	            case 'textarea':
	                $(this).val('');
	                break;
	            case 'checkbox':
	            case 'radio':
	                this.checked = false;
	        }
	    });
	}
	
	function accordeon_subselect(selector,container){
		return $(selector+" .status",$(container).parents('.jqui-accordion'));
	}
	
	function accordeon_select(selector,container){
		return $(selector,$(container).parents('.jqui-accordion'));
	}
	
	function Competition(){
		
	}
	
	Competition.prototype = new config.competition_element();
	
	Competition.prototype.init = function(master,id,page){
		config.competition_element.prototype.init.call(this,id,page);
		this.master = master;
	}
	
	Competition.prototype.show_config = function(){
		config.competition_element.prototype.show_config.call(this);
		$(".jqui-accordion",this.container).accordion();
	}
	
	return {
		Competition : Competition,
		
		clear_form_elements : clear_form_elements,
		
		accordeon_subselect : accordeon_subselect,
		
		accordeon_select : accordeon_subselect,
		
		accordeon_unset : function (selector,container){
			clear_form_elements(accordeon_select(selector,container));
			accordeon_subselect(selector,container).text("[Not set]");
		}
	};
});