define(["jquery",
        "competition_configure/common",
        "competition_configure/twosideswithscores",
        "competition_configure/config"],
        function($,comp_common,twosideswithscores,config){
	
	function update_accordeon(competition,object){		
		if(competition.best_player!=undefined){
			comp_common.accordeon_subselect(".other",object).text("[Best player is: " + competition.best_player + "]");
			
			if(competition.best_player.length==0 || !competition.best_player.match(/[a-zA-Z]+/gi)){
				competition.best_player = undefined;
				comp_common.accordeon_unset(".other",object);
			}
		}
		
		//no need to update logic each time we type a letter (since this is linked to keyup)
		//competition.update_logic();
	}

	
	function Soccer(){
		
	}
	Soccer.prototype = new twosideswithscores.TwoSidesWithScores();
		
	Soccer.prototype.update_logic = function(){
		twosideswithscores.TwoSidesWithScores.prototype.update_logic.call(this);
		
		if(this.logic==undefined){
			var best_player_value = $(this.form.best_player,this.container).val();

			if(best_player_value.length>0){
				this.logic = this.master +".best_player="+'"'+$(this.form.best_player,this.container).val()+'"';
			}else
				alert('competition ' + this.master + ' is not configured');
		}else{
			var best_player_value = $(this.form.best_player,this.container).val();

			if(best_player_value.length>0){
				this.logic += " and " + this.master +".best_player="+'"'+$(this.form.best_player,this.container).val()+'"';
			}
		}
		console.log(this.logic);
		return this.logic;		
	}
	
	return {
		configure : function(id_master,competition,page,competition_form_prefix){    
			$(document).ready(function(){
				var soccer = new Soccer();
				config.configure(soccer,id_master,competition,page,competition_form_prefix);
				
				soccer.form.best_player = "#id_"+competition_form_prefix+"-best_player";				
				soccer.help = $("#soccer_competition_config_help",soccer.container).jqote( soccer.help_dictionary(competition) );
				
				soccer.refresh();

				soccer.container.on("keyup",soccer.form.best_player,function(event){						
					soccer.best_player = $(soccer.form.best_player,soccer.container).val();
					update_accordeon(soccer,this);
				});
			});
		}
	}
});