define(
		[ 'jquery', 'page_state_initialized','qTip2' ],
		function($,page_state) {
			page_state.help.sync = function(){
				Dajaxice.help.sync(function(data){},{'config':page_state.help});
			};
			return {
				display : function(){			
					$(document).ready(function(){
						if(page_state.help.general.notifications)
							$('#menu-panel #notifications').qtip({
								content: {
									text: 	'Here you are notified if' +  
											'<ul>' +
											'<li>Someone bought your doom</li>' +
											'<li>Someone wrote on your wall</li>' +
											'<li>You have a new friend</li>' +
											'<li>And more!</li>' +
											'</ul>'
											, 
									title: {
										text: 'Notification button',
										button: true
									}
								},
								position: {
									my: "top left", // Use the corner...
									at: "bottom left" // ...and opposite corner
								},
								show: {
									event: false,
									ready: true, // ... but show the tooltip when ready
								},
								hide : false,
								events: {
									hide: function(){
										page_state.help.general.notifications = 0;
										page_state.help.sync();
									}
								},								
								effect: function() { $(this).show('slide', null, 100); }
			
							});
						if(page_state.help.general.transactions)
							$('#menu-panel #transactions').qtip({
								content: {
									text: 	'Here you create dooms (sell doom submenu) ' +  
											'Or you can buy them (buy doom submenu) ' +
											'in a few clicks.', 
									title: {
										text: 'Quick transactions',
										button: true
									}
								},
								position: {
									my: "top left", // Use the corner...
									at: "bottom right" // ...and opposite corner
								},
								show: {
									event: false, // Don't specify a show event...
									ready: true // ... but show the tooltip when ready
								},
								hide : false,
								events: {
									hide: function(){
										page_state.help.general.transactions = 0;
										page_state.help.sync();
									}
								}			
							});
						if(page_state.help.general.doom_create)
							$('#quickdoom-panel').qtip({
								content: {
									text: 	'Here you create a new doom ' +  
											'from a bet you made on another site. ' +
											'You specify price, potential winnings and can combine compettions.', 
									title: {
										text: 'Sell a new doom',
										button: true
									}
								},
								position: {
									my: "center right", // Use the corner...
									at: "center left" // ...and opposite corner
								},
								show: {
									event: false, // Don't specify a show event...
									ready: true // ... but show the tooltip when ready
								},
								hide : false,
								events: {
									hide: function(){
										page_state.help.general.doom_create = 0;
										page_state.help.sync();
									}
								}
			
							});
						if(page_state.help.general.help_display)
							$('#help-panel').qtip({
								content: {
									text: 	'Do these help bubbles annoy you? ' +  
											'Click on disable to get rid of them. ' +
											'You can enable them again by clicking <strong>enable</strong>.<br/>' +
											'Note that the bubbles you close will never be displayed again.'
											, 
									title: {
										text: 'Help bubbles',
										button: true
									}
								},
								position: {
									my: "bottom right", // Use the corner...
									at: "top left" // ...and opposite corner
								},
								show: {
									event: false, // Don't specify a show event...
									ready: true // ... but show the tooltip when ready
								},
								hide : false,
								events: {
									hide: function(){
										page_state.help.general.help_display = 0;
										page_state.help.sync();
									}
								},
								style: {
									classes: 'ui-tooltip-red ui-tooltip-shadow'
								}
							});
						
							
					}); //dom ready
				} //function
			}; // return
		});