define(function(){
	return {
	        personal_panel : {
	            user : ''
	        },        
	        page : 1,        
	        static_notifications : {
	            count : 0
	        },
	        interactive_notifications : {            
	            add : function(data){
	            	for(var notif in this.list){
	            		if(this.list[notif].id==data.id && this.list[notif].type==data.type)
	            			return false;
	            	}
	                this.list.push(data);
	                return true;
	            },    
	            clear : function(){
	                this.list = [];
	            },    
	            visibility : {
	                show : false
	            },
	            verbose : false,
	            list : []
	        },
	        search : {
	            params : {},            
	            command : function(){},
	            callback : function(){},
	            init : function(_command,_callback,_params){
	                this.params = _params;                
	                this.command = _command;
	                this.callback = _callback;
	            },
	            execute : function(settings){
	                this.params['page']=settings.page;
	                this.command(this.callback,this.params);
	            }
	        }
	    };
});