define(['jquery', 
        'console',
        'init_js_common_functions_static',
        'urls'
        ], function($,myconsole,common_fn,urls){
	
	$(document).ready(function($){
	    $("#trans-accordeon").bind('douaccordeon_closed',
	    	    function(event,element){
	    			switch($(element).attr("id")){
	    			case "buy-doom":
	    				$("#id_doom_text").autocomplete('close');
	    	        	break;
	    			case "sell-doom":
	    				$("#id_competition_text").autocomplete('close');
	    				break;
	    			}
	    	        //$("#").autocomplete('close');
	    	    }
	    	);
	    if(django_username!=""){
		   	$("#trans-accordeon").bind('douaccordeon_opened',
		   		function(event,element){	
		   			switch($(element).attr("id")){
		       			case "buy-doom":
		       	        	Dajaxice.dooms.prefered_dooms(
		  	        			function(html){
		  	        				$("#preferred-dooms-menu").html(html);      	        				
		  	        			}
		       	        	);
		       	        	break;
		       			case "sell-doom":
		       				Dajaxice.dooms.unsold_dooms(
		  	        			function(html){
		  	        				$("#bought-dooms-menu").html(html);      	        				
		  	        			}
			       	        );
		       				break;
		   			}
		   	    }
		   		
		   	);
		}
	    
	    $("#preferred-dooms-menu").on('click','li .doom-base-preview',function(){
	    	var id = parseInt($($(this).parents('li')).attr("id"));
	    	common_fn.setup_facebox();
	    	$.facebox({ ajax: urls.resolve("buy_doom",{'doomid':id,'buy_doom_redirect':document.location.pathname}) });        	
	    });
	    $("#bought-dooms-menu").on('click','li .doom-base-preview',function(){
	    	var id = parseInt($($(this).parents('li')).attr("id"));
	    	common_fn.setup_facebox();
	    	$.facebox({ ajax: urls.resolve("sell_doom",{'doomid':id}) });        	
	    });
	});
	
	return {
		get_latest_dooms_menu : function (){
			Dajaxice.dooms.prefered_dooms(
		  			function(html){
		  				$("#preferred-dooms-menu").html(html);
		  			}
		       	);
		
			Dajaxice.dooms.unsold_dooms(
		  			function(html){
		  				$("#bought-dooms-menu").html(html);      	        				
		  			}
		      	);
		}
	}
});
	
	
