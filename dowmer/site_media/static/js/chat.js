define(['jquery', 
        'pubsub.client',
        'console',
        'constants',
        'init_js_common_functions_static',
        'urls', //for jqote templates
        'jqote2/jquery.jqote2.min'        
        ], function($,client,myconsole,constants,common_fn){

	$(document).ready(function() {
		$(".chatroom_writing_form").hide();

		function chat_anchor_click(event){
			event.preventDefault();
			var self=$(this);			
			client.pubsub_conn.muc.leave("room@conference.localhost",django_username,function(stanza){
				$(".chatroom_writing_form").hide();
				$("#chatroom-loading").text("You have signed out of the chat. You can now close the window");
				$("#chatroom-loading").show();
				
				document.location = self.attr('href');
			});
			return false;
		}
		$("a").click(chat_anchor_click);
	});

	return {			
		xmpp_other_init : {
			chatroom : {},
			call: function (conn) { 
				var self = this;
				client.pubsub_conn.muc.join("room@conference.localhost", django_username, 
						function(stanza){
					var nick = stanza.getAttribute('from').match(/[a-zA-Z_]+$/gi);

					$("#chatroom-window > ul").append($("#chatroom_message").jqote({user:nick+"[jabber]",content:"loading..."}));
					$("#chatroom-window > ul > li:last .chatroom-user-talk").text(stanza.getElementsByTagName("body")[0].childNodes[0].nodeValue);
					return true;
				}, 
				function(stanza){
					if(stanza.getAttribute('type')=="unavailable"){
						var nick = stanza.getAttribute('from').match(/[a-zA-Z_]+$/gi);
						myconsole.log(nick + " has left");
						self.chatroom[nick] = {};
						$("#chatroom-members > ul > li#" + nick).remove();
					}else if (stanza.getAttribute('type')=="error"){
						$("#chatroom-loading").text("You are logged in from another location or you didn't log out properly. Either way, you cannot chat. Try again later.");
					}else{                
						var nick = stanza.getAttribute('from').match(/[a-zA-Z_]+$/gi);
						myconsole.log(nick + " has joined");
						self.chatroom[nick] = {};
						Dajaxice.profiles.get_avatar_url(
								function(data){
									self.chatroom[nick] = data;
									$("#chatroom-members > ul").append($("#chatroom_user").jqote({"url":data.url,"user":nick}));
								},
								{"user":nick});
						if (nick==django_username){
							$(".chatroom_writing_form").show();
							$("#chatroom-loading").hide();
						}
					}
					return true;
				});
			}
		},
		event_cb : function (obj, node,conn) {
			if(node=="/everyone"){        
				switch(obj.type){
				case constants.NotifTypes.CHAT_MESSAGE:
					$("#chatroom-window > ul").append($("#chatroom_message").jqote(obj));                
					break;
				}
				common_fn.setup_facebox();
			}
		}			
	}
}
);