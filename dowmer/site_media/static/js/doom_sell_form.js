define(["jquery",
        "constants",
        "init_js_common_functions_static",
        "urls",
        "jqote2/jquery.jqote2.min",
        'jquery.form/jquery.form'
        ],function($,constants,common_fn,urls){
	return {
		ajaxify_form : function(){

			$(document).ready(function() {     
				$.facebox.settings.closeImage = '/site_media/static/js/facebox/src/closelabel.png';
				$.facebox.settings.loadingImage = '/site_media/static/js/facebox/src/loading.gif';
				$('.sell_doom_form').ajaxForm(
						{
							dataType:  'json',	
							beforeSubmit: function(){
								$.facebox.settings.closeOnClick = false;
								$.facebox(function(){}); //display a waiting clock
							},
							success:
								function(data) {
								$.facebox.settings.closeOnClick = true;
								switch(data.transfer){
								case constants.DoomTransfer.TRANSACTION_COMPLETE:
									$.facebox($("#doom_sell_success_page").jqote(data));
									//if the form is opened in a new tab, redirect to wall
									if(window.location.href.match(/dooms\/sell\/[0-9]+/))
										window.location.href = urls.resolve("wall",{username:django_username});									
									break;
								case constants.DoomTransfer.TRANSACTION_FAILED_AND_REDIRECT:
									if(data.message_type==constants.DoomTransferDetail.NOT_LOGGED_IN){
										$.facebox($("#redirect_to_login_facebox").jqote(data));
										window.location.href = urls.resolve("login",{next:common_fn.prefix_slash(window.location.pathname)});
									}                            
									break;
								case constants.DoomTransfer.TRANSACTION_FAILED:
									switch(data.message_type){
									case constants.DoomTransferDetail.INVALID_DATA:                    		
										$("#form_errors_"+data.doomid).html($("#form_error_facebox").jqote(data.form_errors));
										break;
									case constants.DoomTransferDetail.FORBIDDEN_STATUS:    				
										$.facebox("This doom is locked, you cannot buy it.<br/>It is locked because all of its competitions are finished or are almost finished.");
										break;
									default:
										$.facebox($("#doom_sell_fail_page").jqote(data));
									break;
									}

									break;

								}
							},
							error:function(data){
								$.facebox.settings.closeOnClick = true;
								$.facebox($("#doom_sell_unknown_fail_page").jqote({data:data.responseText}));                
							}
						});     

			}); 
		}
	};
});