define(['jquery', 
        'page_state',
        'console', 
        'constants',
        'init_js_common_functions_static',
        'urls', //for jqote templates
        'jqote2/jquery.jqote2.min'        
        ], function($,page_state,myconsole,constants,common_fn){
				return {
					notification_wall_event_cb : function (object, node,conn) {
					    switch(object.type){        
					        case constants.NotifTypes.DOOM_SOLD:            
					            if(object.transaction.user_paid==django_username){
					                page_state.interactive_notifications.add(object);
					            	if(page_state.interactive_notifications.verbose){
					            		$("#notifications-page").prepend(common_fn.notification_embed_page($("#messagetransactionwallpost_page").jqote(object)));
					            		$("span.timeago").timeago();
					            	}
					            }
					            break;
					        case constants.NotifTypes.NEWS_RECIEVED:
					        	if(object.wallpost_owner==django_username){
						            page_state.interactive_notifications.add(object);
						        	if(page_state.interactive_notifications.verbose){
						        		$("#notifications-page").prepend(common_fn.notification_embed_page($("#message_page").jqote(object)));
						        		$("span.timeago").timeago();
						        	}
					        	}
					            break;
					        case constants.NotifTypes.DOOM_STATUS:        	
					        	if(object.notification.content_object.doom.owner==django_username){
					        		page_state.interactive_notifications.add(object);
						        	if(page_state.interactive_notifications.verbose){
							        	switch(object.notification.content_object.doom.status){
							        	case constants.DoomStatus.WON:
							        		$("#notifications-page").prepend(common_fn.notification_embed_page($("#doom_won_page").jqote(object.notification.content_object.transaction)));
							        		break;
							        	case constants.DoomStatus.LOST:
							        		$("#notifications-page").prepend(common_fn.notification_embed_page($("#doom_lost_page").jqote(object.notification.content_object.doom)));
							        		break;        		
							        	}
							        	$("span.timeago").timeago();
						        	}
					        	}
					        default:
					            break;
					    }
					
					}
				}
});
