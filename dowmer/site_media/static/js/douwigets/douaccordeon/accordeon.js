define(['jquery', 'jquery.hoverIntent.min', 'jqueryui/jquery.ui.widget'], function($){
	$.widget("ui.douaccordeon",{
		    _init : function() {            
		    	this.element.addClass("douaccordeon");            
		        var _this = this;
		        
		        $('.douaccordeon-parent > .douaccordeon-header > .douaccordeon-title',this.element).off();
		        
		        $('.douaccordeon-parent > .douaccordeon-header > .douaccordeon-title',this.element).hoverIntent(
		            function () {                	
		                _this.mouse_in($(this).parent().parent());
		                _this.change_state();
		                
		            },function(){}
		        );
		        $('.douaccordeon-parent > .douaccordeon-header > .douaccordeon-title',this.element).bind('click',
		        	function(){
		        		
		        		_this.mouse_in($(this).parent().parent());
		        		_this.change_state();                    
		    		}
		        );
		
		    },  
		    mouse_in: function (which) {
		    	
		        this.options.mouse_in = true;
		        
		        if($(which).hasClass("douaccordeon-parent")){
		            this.options.menu_element = which;
		        }
		    },
		    mouse_out: function () {
		        this.options.mouse_in = false;
		    },
		    is_equal: function(obj1,obj2){
		        return !obj1.not( obj2 ).length;
		    },
		    change_state : function(){
		        if(this.options.mouse_in){
		        	if($('.douaccordeon-body',this.options.menu_element).is(':visible')) return;
		            if(this.options.last_menu_element!=undefined && !this.is_equal(this.options.last_menu_element , this.options.menu_element)){                
		                $('.douaccordeon-body', this.options.last_menu_element).slideUp(100);
		                $('.douaccordeon-summary', this.options.last_menu_element).fadeIn();
		                
		                this.element.trigger('douaccordeon_closed',this.options.last_menu_element);
		        	}
		            this.options.last_menu_element = this.options.menu_element;
		            $('.douaccordeon-body', this.options.menu_element).slideDown(100);
		            $('.douaccordeon-summary', this.options.menu_element).fadeOut();
		            this.element.trigger('douaccordeon_opened',this.options.menu_element);
		        }else{                
		            //bug       
		        }
		    },
		            
		    options: {
		        mouse_in : false,
		        menu_element : undefined,
		        last_menu_element : undefined
		    }
		}
	);	
});