define(['jquery', 'console','jqueryui/jquery.ui.widget'], function($,myconsole){
	return $.widget("ui.doomenu", {
		    _create : function() {
		    	this.element.addClass("doomenu");
		        var _this = this;            
		
		         $(document).click(function(event){                
		            if($(event.target).parents('html').length==0) return; //element has been removed
		        	if($(event.target).parents('.ui-menu').length) return; //jquery menu clicked
		        	if($(event.target).parents('.doomenu').length){ //inside menu
		        		
		        		_this.mouse_in($(event.target).parents('.menu_parent'));
		        		_this.change_state();
		        	}else{
		        		_this.mouse_out();
		        		_this.change_state();
		        	}
		        });
		         
		    },        
		    mouse_in: function (which) {   
		        this.options.mouse_in = true;
		        if($(which).attr("class")=="menu_parent"){
		            this.options.menu_element = which;
		        }else{
		        	myconsole.log("err");
		        }
		    },
		    mouse_out: function () {        	
		        this.options.mouse_in = false;
		    },
		    is_equal: function(obj1,obj2){
		        return !obj1.not( obj2 ).length;
		    },
		    change_state : function(){            
		        if(this.options.mouse_in){     
		            if(this.options.last_menu_element!=undefined && !this.is_equal(this.options.last_menu_element , this.options.menu_element)){                    
		                $('> ul', this.options.last_menu_element).hide(); 
		                this.element.trigger('doumenu_closed',this.options.last_menu_element);
		            }
		            var obj=$('> ul', this.options.menu_element);
		            if(!obj.is(":visible")){
		            	obj.show();
		        		this.element.trigger('doumenu_opened',this.options.menu_element);                	
		            }
		            this.options.last_menu_element = this.options.menu_element;                
		        }else{
		            $('> li > ul', this.element).hide();
		            this.element.trigger('doumenu_closed',this.options.menu_element);
		        }
		    },
		            
		    options: {
		        mouse_in : false,
		        menu_element : undefined,
		        last_menu_element : undefined
		    }
		}
	);
});	
