define(["jquery"],function($){
	(function($){
		$.fn.jqote_postprocess = function(){
			$(".doom-preview-to-load").each(function(){				
				require("dooms").preview_doom($(this),$(this).attr("value"));				
				$(this).toggleClass('doom-preview-to-load doom-preview-loaded');
			});
			// allow for chaining
			return this;
	
		};
	
	})($);
});