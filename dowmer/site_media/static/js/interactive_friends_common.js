define(['jquery', 
        'console', 
        'init_js_common_functions_static',
        'urls', //for jqote templates
        'jqote2/jquery.jqote2.min'        
        ],function($,myconsole,common_fn){
			var interactive_friends_common = function(){};
			
			interactive_friends_common.prototype.friendshipinvitation_menu_template = function (object){
				    switch(object.status){
				        case "2":
				            return $("#friendship_request_menu").jqote(object);
				            break;
				        case "5":
				            return $("#friendship_completed_menu").jqote(object);
				            break;
				        case "6":
				            return $("#friendship_rejected_menu").jqote(object);
				            break;
				    }
				};
				
			interactive_friends_common.prototype.friendshipinvitation_page_template = function (object){
			    switch(object.status){
			        case "2":
			            return $("#friendship_request_page").jqote(object);
			            break;
			        case "5":
			            return $("#friendship_completed_page").jqote(object);
			            break;
			        case "6":
			            return $("#friendship_rejected_page").jqote(object);
			            break;
			    }
			};
			
			interactive_friends_common.prototype.update_invitation_accept = function (data){
			    $(".if_invitation_"+data.id).closest(".douaccordeon-parent").replaceWith(this.friendshipinvitation_menu_template(data));
			    $(".if_invitation_"+data.id).closest(".notification-parent").replaceWith(this.friendshipinvitation_page_template(data));        
			};
			
			interactive_friends_common.prototype.add_invitation_accepted = function (data){
			    $("#notifications-page").prepend(common_fn.notification_embed_page(this.friendshipinvitation_page_template(data)));
			    $(".no-notifications").remove();    
			};
			
			interactive_friends_common.prototype.add_invitation_reject = function (data){
				$("#notifications-page").prepend(common_fn.notification_embed_page(this.friendshipinvitation_page_template(data)));
			    $(".no-notifications").remove();
			};
			
			interactive_friends_common.prototype.update_invitation_reject = function (data){
				$(".if_invitation_"+data.id).closest(".douaccordeon-parent").replaceWith(this.friendshipinvitation_menu_template(data));
				$(".if_invitation_"+data.id).closest(".notification-parent").replaceWith(this.friendshipinvitation_page_template(data));
			};
			
			interactive_friends_common.prototype.accept_invitation = function (id){	
				var self = this;
			    Dajaxice.interactive_friends.friendship_accept(function(data){self.update_invitation_accept(data);},{'id' : id});
			};
			
			interactive_friends_common.prototype.decline_invitation = function (id){
				var self = this;
			    Dajaxice.interactive_friends.friendship_decline(function(data){self.update_invitation_reject(data);},{'id' : id});
			};
			
			interactive_friends_common.prototype.interactive_friends_events_common = function interactive_friends_events_common(obj, node,conn){
			
			};
			return new interactive_friends_common();
		
});