define(["jquery","page_state"],function($,page_state){
	$(document).on("click",".pagenumber.ajax",function(e){
		e.preventDefault();
		page_state.page=parseInt($(this).attr("value"));
		page_state.search.execute(page_state);
	});
	
});