function subscriber_list_for_node(node,tag){
	Dajaxice.pilot.subscriber_list_for_node(
			function(data){
				$(tag).html(data);
				$(tag).toggle();
				
			},
			{'node':node}
		);
}

function check_missing_nodes(node,tag){
	$.get('/pilot/pubsub_nodes/missing_nodes',
			function(data){
				$("#missing-nodes").html(data);
			}
		);
}

function checkAll(form)
{
	$('input[type=checkbox]',form).attr('checked', true);
}

function uncheckAll(form)
{
	$('input[type=checkbox]',form).attr('checked', false);
}

function publish_to_node(node_tag,payload_tag,result_tag){
	Dajaxice.pilot.publish_json(
			function(data){
				LoadXMLString(result_tag,data);
				var xml = $.parseXML( data )
				$("#" + result_tag).prepend("<p>Reply:</p>");
				if($(xml).find('iq').attr('type') == "result"){
					$("#"+result_tag).prepend("<p><strong>Success</strong></p>");
				}else{
					$("#"+result_tag).prepend("<p><strong>!Possible failure!</strong></p>");
				}
				
			},
			{
				'node':$(node_tag).val(),
				'payload':$(payload_tag).val()
			}
		);

}