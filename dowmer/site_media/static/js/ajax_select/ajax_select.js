define(["jquery"],function($){
	function generate_containers(container,options){
		if(options.list_field == undefined)
			options.list_field_element = $("#" + $(container).attr("id")+"_hidden",$(container));
		else
			options.list_field_element = $(options.list_field,$(container));
		
		if(options.deck == undefined)
			options.deck_element = $("#" + $(container).attr("id")+"_on_deck",$(container));
		else
			options.deck_element = $(options.deck,$(container));
		
		if(options.autocomplete_template == undefined)
			options.autocomplete_template_element = $("#autocomplete_template",$(container))
		else
			options.autocomplete_template_element = $(options.autocomplete_template,$(container))
			
		if(options.text_field == undefined)		
			options.text_field_element = $("#" + $(container).attr("id")+"_text",$(container));
		else
			options.text_field_element = $(options.text_field,$(container));
	}
	
	function add_item(container,pk,repr,options){
		if (options==undefined){
			options = {};
			generate_containers(container,options);
		}
		var prev = options.list_field_element.val();

		if (prev.indexOf("|"+pk+"|") == -1) { //element does not exist
			options.list_field_element.val((prev ? prev : "|") + pk + "|");	            
			options.deck_element.append(options.autocomplete_template_element.jqote({pk: pk,content:repr}));	
			return true;
        }
		return false;
	}
	
	function clear(container,options){
		if (options==undefined){
			options = {};
			generate_containers(container,options);
		}
		options.list_field_element.val("|");
		options.deck_element.html("");
	}
	
	(function($){
		$.fn.autocompletehtml = function() {
			this.data("autocomplete")._renderItem = function _renderItemHTML(ul, item) {
				return $("<li></li>")
					.data("item.autocomplete", item)
					.append("<a>" + item.match + "</a>")
					.appendTo(ul);
			};
			return this;
		};
		
		$.fn.autocompleteselectmultiple = function(options){
			var self = this;
			generate_containers(this,options);
			
			options.select = function(event, ui){
				this.value = '';
				
				if (add_item(self,ui.item.pk,ui.item.repr,options))
					$(self).trigger("autocomplete_element_selected",ui.item.pk);
		        
				return false;
			};
			options.text_field_element.autocomplete(options);
			options.text_field_element.autocompletehtml();
	
			$(this).on("click","a.autocomplete-item-kill",function(){				
				var pk = $(this).attr("value");
				var deck_element_selected = $('li#'+pk,options.deck_element);
				
				options.list_field_element.val(options.list_field_element.val().replace("|" + pk + "|", "|"));
				deck_element_selected.remove();
				$(self).trigger("autocomplete_element_removed",pk);
				return false;
			});
			
			return this;
		};
		
		$.fn.autocompleteselect = function(options){			
			var self = this;
			generate_containers(this,options);
			
			if (options.select == undefined){
				options.select = function(event, ui){
					this.value = '';
					var pk = ui.item.pk;
					var repr = ui.item.repr;
					
					options.deck_element.html(options.autocomplete_template_element.jqote({pk: pk,content:repr}));
					$(self).trigger("autocomplete_element_selected",pk);
					return false;
				};
				
			}
			options.text_field_element.autocomplete(options);
			options.text_field_element.autocompletehtml();
	
			$(this).on("click","a.autocomplete-item-kill",function(){
				var pk = $(this).attr("value");
				var deck_element_selected = $('li#'+pk,options.deck_element);
				deck_element_selected.remove();
				$(self).trigger("autocomplete_element_removed",pk);
				return false;
			});
			
			return this;
		};
	
	})($);
	return {
		add_item : add_item,
		clear : clear,
		generate_containers : generate_containers
	}
});