define(['jquery', 
        'page_state',
        'pubsub.settings', 
        'console', 
        'constants',        
        'init_js_common_functions_static',
        'protocol',
        'interactive_friends',
        'interactive_friends_common',
        'notifications_wall',
        'urls', //for jqote templates
        'jqote2/jquery.jqote2.min'        
        ], function($,page_state,settings,myconsole,constants,common_fn,protocol,interactive_friends,interactive_friends_common,notifications_wall){
	var notifications = function(){};

	notifications.prototype.event_cb = function(obj, node,conn){};

	notifications.prototype.notification_response = function (data){
		if(settings.DEBUG)
			myconsole.log(data);
	};

	notifications.prototype.notification_event_cb = function(entry, node,conn) {  
		if(settings.DEBUG)  
			myconsole.log("notification_event_cb on: "+node+":"+ $(entry).text());
		//try{
		var obj = $.parseJSON($(entry).text());
		switch(obj.type){                
		case constants.NotifTypes.INVITATION_IS_ACCEPTED:
			if(obj.invitation.from == django_username){                    
				myconsole.log("subscribing to " +  obj.invitation.to);
				Dajaxice.pubsub.subscribe_to_friend(this.notification_response,{
					'jid' : protocol.xmpp_credentials.jid,
					'friend' : obj.invitation.to
				}
				);
			}                //update categories, dooms or whatever
			break;
		case constants.NotifTypes.ACCEPTED_INVITATION:
			if(obj.invitation.to == django_username){                    
				myconsole.log("subscribing to " +  obj.invitation.from);
				Dajaxice.pubsub.subscribe_to_friend(this.notification_response,{
					'jid' : protocol.xmpp_credentials.jid,
					'friend' : obj.invitation.from
				}
				);
			}
			break;
		case constants.NotifTypes.NOTIFICATIONS_READ:
			page_state.static_notifications.count = 0;
			page_state.interactive_notifications.clear();
			common_fn.badge_notification_based_on_page_settings();
			break;
		case constants.NotifTypes.SYSTEM_NOTICE:
			$(".no-notifications").remove();
			page_state.interactive_notifications.add(obj);
			$("#notifications-page").prepend(common_fn.notification_embed_page($("#system_notice_notification_page").jqote(obj.notification.content_object)));
			$("span.timeago").timeago();
			common_fn.badge_notification_based_on_page_settings();
			break;
		default:
			break;
		}
		interactive_friends_common.interactive_friends_events_common(obj, node,conn);
		interactive_friends.interactive_friends_event_cb(obj, node,conn);
		notifications_wall.notification_wall_event_cb(obj, node,conn);		
		this.event_cb(obj, node,conn);
		common_fn.badge_notification_based_on_page_settings();
		/*}catch(error){
			        if(settings.DEBUG){
			            myconsole.log("notification_event_cb: exception:"+error.name);
			            myconsole.log("entry:");
			            myconsole.log(entry);
			            myconsole.log("node:" + node);
			        }
			    }*/
	};

	notifications.prototype.get_latest_notifications_menu = function (){			            
		Dajaxice.interactive_notification.list_all(
				function(data){
					$("#notif-accordeon").html($("loading_notifications_page").jqote());
					for (var i in data){
						switch(data[i].content_model){
						case "friendshipinvitation":
							$("#notif-accordeon").append(interactive_friends_common.friendshipinvitation_menu_template(data[i].content_object));
							break;
						case "wallpost":                        
							switch(data[i].content_object.content_model){
							case "messagetransactionwallpost":
								$("#notif-accordeon").append($("#messagetransactionwallpost_menu").jqote(data[i].content_object.content_object));
								break;
							case "message":
								$("#notif-accordeon").append($("#message_menu").jqote(data[i].content_object));
								break;
							}
							break;
						case "doomstatuschangednotification":                    	
							switch(data[i].content_object.doom.status){
							case constants.DoomStatus.PENDING:
								break;
							case constants.DoomStatus.WON:                    		
								$("#notif-accordeon").append($("#doom_won_menu").jqote(data[i].content_object.transaction));
								break;
							case constants.DoomStatus.LOST:
								$("#notif-accordeon").append($("#doom_lost_menu").jqote(data[i].content_object.doom));
								break;
							case constants.DoomStatus.FROZEN:
								break;
							}

							break;
						case "systemnoticenotification":
							$("#notif-accordeon").append($("#system_notice_notification_menu").jqote(data[i].content_object));
							break;
						}
					}
					if(data.length==0){ 
						$("#notif-accordeon").append($("#no_notifications_menu").jqote());
						$("#notif-accordeon").append($("#see_notificationpage_menu").jqote());                
					}else 
						$("#notif-accordeon").append($("#see_all_notifications_menu").jqote());
					$("#notif-accordeon").douaccordeon();
					common_fn.setup_facebox();
				}	
		);
	};

	return new notifications();

});