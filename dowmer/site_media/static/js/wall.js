define(['jquery', 
        'console',
        'pubsub.settings',
        'constants',
        'init_js_common_functions_static',
        'protocol',
        'urls', //for jqote templates     
        'dooms',
        'jqote2/jquery.jqote2.min',
        'jquery.jqote_postprocess'
        ], 
    function($,myconsole,settings,constants,common_fn,protocol){	
		return {
			create_event : function (username){
				function doom_sub_response(data){
				    if(settings.DEBUG)
				    	myconsole.log(data);
				}
		
			    return function(obj, node,conn) {
			        if(settings.DEBUG){
			            myconsole.log("this is wall");
			            myconsole.log(obj);
			            myconsole.log(node);
			        }
			        //try{        	
			            switch(obj.type){
			                case constants.NotifTypes.DOOM_NEW_SALE:
			                    if(node.substring((node).length-5)=="/wall"){
				                	if(!$('.wallpost_doom_'+obj.doom.id).length){
				                		myconsole.log('subscribing to doom ' + obj.doom.id);
				                		Dajaxice.pubsub.subscribe_to_doom(doom_sub_response,{'jid':protocol.xmpp_credentials.jid,'doomid':obj.doom.id});
				                    }
			                		$('#wall-contents #wall-activity-panel').prepend("<li>"+$('#wallpost_wall_prelude').jqote(obj)+$('#wallpost_doom').jqote(obj)+"</li>").jqote_postprocess();
			                		$('#selling-panel').prepend($("#frontsales_doom").jqote(obj)).jqote_postprocess();                       
			                	}
			                    if(node.substring(0,6)=="/dooms"){
			                        $('.wallpost_doom_'+obj.doom.id+'> div > #wallpost_doom_interaction').replaceWith($("#wallpost_doom_interaction").jqote(obj)).jqote_postprocess();
			                    }
			                    break;
			                case constants.NotifTypes.DOOM_BOUGHT:
			                	if(node.substring((node).length-5)=="/wall"){  
			                		if(!$('.wallpost_doom_'+obj.doom.id).length){
			                			myconsole.log('subscribing to doom ' + obj.doom.id);
			                            Dajaxice.pubsub.subscribe_to_doom(doom_sub_response,{'jid':protocol.xmpp_credentials.jid,'doomid':obj.doom.id});
			                        }
			                		$('#wall-contents #wall-activity-panel').prepend("<li>"+$('#wallpost_wall_prelude').jqote(obj)+$('#wallpost_doom').jqote(obj)+"</li>").jqote_postprocess();                		
			                        $('#frontsale_doom_'+obj.doom.id).remove();                        
			                	}
			                    if(node.substring(0,6)=="/dooms"){
			                        $('.wallpost_doom_'+obj.doom.id+'> div > #wallpost_doom_interaction').replaceWith($("#wallpost_doom_interaction").jqote(obj)).jqote_postprocess();
			                    }
			                    break;
			                case constants.NotifTypes.DOOM_SOLD:                	
			                    if(node.substring((node).length-5)=="/wall"){                		
			                        $('#frontsale_doom_'+obj.doom.id).remove();
			                        //if a doom is sold, do not add a wall post, just remove it. 
			                        //there is no "doom sold" post type anyway, just doom bought and "selling doom"
			                        //$('#wall-contents #wall-activity-panel').prepend("<li>"+$('#wallpost_wall_prelude').jqote(obj)+$('#wallpost_doom').jqote(obj)+"</li>");                        
			                    }
			                    if(node.substring(0,6)=="/dooms"){
			                        $('.wallpost_doom_'+obj.doom.id+'> div > #wallpost_doom_interaction').replaceWith($("#wallpost_doom_interaction").jqote(obj)).jqote_postprocess();
			                    }
			                    break;
			                case constants.NotifTypes.DOOM_STATUS:
			                	var doom = obj.notification.content_object.doom;
			                	
			                	if(node.substring((node).length-5)=="/wall"){                		
			                        $('#frontsale_doom_'+doom.id).remove();
			                        if(doom.status==constants.DoomStatus.WON){
			                        	var wallpost = $.extend(obj.notification.content_object.wallpost,obj.notification.content_object.wallpost.content_object);
			                        	wallpost["doom"]=doom;
			                        	wallpost["id"]=obj.id;
			                        	$('#wall-contents #wall-activity-panel').prepend("<li>"+$('#wallpost_wall_prelude').jqote(wallpost)+$('#wallpost_doom').jqote(wallpost)+"</li>").jqote_postprocess();
			                        	$('#winning-panel').prepend($("#victories_doom").jqote(wallpost)).jqote_postprocess();
			                        }
			                    }
			                    if(node.substring(0,6)=="/dooms"){
			                        $('.wallpost_doom_'+doom.id+'> div > #wallpost_doom_interaction').replaceWith($("#wallpost_doom_interaction").jqote({'doom':doom})).jqote_postprocess();
			                    }
			                    break;
			                case constants.NotifTypes.INVITATION_IS_ACCEPTED:
			                	if(obj.invitation.from == django_username && obj.invitation.to == django_other_username){
			                        Dajaxice.wall.wall_global_display_and_subscrible(Dajax.process,{'username':obj.invitation.to,'jid':protocol.xmpp_credentials.jid});
			                    } else if(node.substring((node).length-5)=="/wall"){               		
			                        
			                        if($('#wallpost_'+obj.id).length){
			                        	//do nothing
			                        }else{                            
			                        	$('#wall-contents #wall-activity-panel').prepend("<li>"+$('#wallpost_wall_prelude').jqote(obj)+$('#wallpost_friendship').jqote(obj)+"</li>").jqote_postprocess();
			                        }                        
			                    }
			                    
			                    break;
			                case constants.NotifTypes.ACCEPTED_INVITATION:
			                	if(obj.invitation.to == django_username && obj.invitation.from == django_other_username){
			                        Dajaxice.wall.wall_global_display_and_subscrible(Dajax.process,{'username':obj.invitation.from,'jid':protocol.xmpp_credentials.jid});
			                    }else if(node.substring((node).length-5)=="/wall"){
			                        if($('#wallpost_'+obj.id).length){
			                        	//do nothing
			                        }else{                            
			                        	$('#wall-contents #wall-activity-panel').prepend("<li>"+$('#wallpost_wall_prelude').jqote(obj)+$('#wallpost_friendship').jqote(obj)+"</li>").jqote_postprocess();
			                        }
			                    }                       
			                    break;
			                case constants.NotifTypes.NEWS_SENT:
			                    
			                    break;
			                case constants.NotifTypes.NEWS_RECIEVED:
			                    if(node.substring((node).length-5)=="/wall"){
			                        if($('#wallpost_'+obj.id).length){ //the tag exists
			                            //do nothing
			                        }else{                            
			                            $('#wall-contents #wall-activity-panel').prepend("<li>"+$('#wallpost_wall_prelude').jqote(obj)+$('#wallpost_doom').jqote(obj)+"</li>").jqote_postprocess();
			                        }
			                    }
			                    break;
			                default:
			                    break;
			        	}
			            common_fn.setup_facebox();
			            $("span.timeago").timeago();
			        /*}catch(error){
			            myconsole.log("exception:"+error.name)
			        }*/
			    }
			}
		}
});
