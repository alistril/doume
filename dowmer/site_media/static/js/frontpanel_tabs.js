define(['jquery'], function($){
	$(document).ready(function(){
		$("ul.showcase-tabs li").click(function(){
			$("ul.showcase-tabs li").removeClass("showcase-tabs-selected");
			$(this).addClass("showcase-tabs-selected");
			
			switch($(this).attr("id")){
			case "victories":
				$("#selling-panel").hide();
				$("#winning-panel").show();
				break;
			case "sales":
				$("#winning-panel").hide();
				$("#selling-panel").show();
				break;
	        case "hide":
	            $("#winning-panel").hide();
				$("#selling-panel").hide();
				break;
			}
		});
	});
});