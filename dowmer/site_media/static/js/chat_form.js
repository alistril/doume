define(['jquery', 
        'console',
        'jqote2/jquery.jqote2.min',
        'jquery.form/jquery.form'
        ], function($,myconsole){
	$(document).ready(function() {
	    $.facebox.settings.closeImage = '/site_media/static/js/facebox/src/closelabel.png';
		$.facebox.settings.loadingImage = '/site_media/static/js/facebox/src/loading.gif';
	    $('.chatroom_writing_form').ajaxForm(
	        {
	            dataType:  'json',
	            success:
	            function(data) {
	                myconsole.log(data);
	            },
	            error:function(){
	                $.facebox($("#chat_send_failed").jqote());                
	            },
	            beforeSubmit:function(){
	                $(':input','.chatroom_writing_form')
	                 .not(':button, :submit, :reset, :hidden')
	                 .val('')
	                 .removeAttr('checked')
	                 .removeAttr('selected');
	
	            }
	        });     
	        
	}); 
});