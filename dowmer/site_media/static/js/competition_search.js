define(["jquery",
        "page_state",
        "protocol",
        "pubsub.client",
        "init_js_common_functions_static",
        "notifications",
        "doom_search",
        "ajax_select/ajax_select",        
        "pagination"
        ], function($,page_state,protocol,client,common_fn,notifications,doom_search,ajax_select){
	return {
		update_work_panel_with_competition_search : function (data){
		    if(client.pubsub_conn!=undefined)
		    	client.pubsub_conn.disconnect();
		    
		    page_state.personal_panel.user = django_username;
			$("#id_search_field_competitions_text").autocomplete('close');
			$("#work-panel").html(data);    
		    
		    /*$.ajax(
				{
					type : 'POST',
					url : '/async',
					dataType : 'json',
					data : xmpp_options,
					success : function(data){				
						//console_log(data);
		                protocol.xmpp_credentials = 
		                    {
		                        jid : data.jid,
		                        rid : data.rid,
		                        sid : data.sid
		                    };
						client.init_pubsub(xmpp_credentials.jid,xmpp_credentials.rid,xmpp_credentials.sid,function(entry, node,conn){notifications.notification_event_cb(entry, node,conn);});
					},
					error : function(XMLHttpRequest, textStatus, errorThrown) {
						console_log('error');
						console_log(textStatus);
						console_log(errorThrown);
					}
				}
		        );*/
			protocol.xmpp_options = {'page' : page_state.page};
			protocol.init_session(notifications);		    
		},
		
		update_custom_panel_with_competition_search : function (search_val,tag){
		    page_state.page=1;
		    page_state.search.init(Dajaxice.competition_search.execute,
		            function(data){
							$("#id_page-search_field_competitions_text").autocomplete('close');
							$(tag).html(data);
						},
		        {'search_val':search_val});
		    
			Dajaxice.competition_search.execute(
						page_state.search.callback,
						{'search_val':search_val,'page':page_state.page}
					);
		},
		
		update_custom_panel_with_single_competition : function (id){
			$.facebox(function() {
				Dajaxice.competition_search.execute_for_specific_competition(function(data){
					$.facebox(data);
					
				},{'competition_id':id})		  
				}); 
		},
		
		submit_search_bar_competition : function (){		
			var search_val = $("#search_bar_competition_form input:text").val();
		    page_state.page=1;
		    page_state.search.init(Dajaxice.competition_search.execute,this.update_work_panel_with_competition_search,{'search_val':search_val,'page':page_state.page});
		    
			Dajaxice.competition_search.execute(this.update_work_panel_with_competition_search,{'search_val':search_val,'page':page_state.page});
		    if(page_state.personal_panel.user != django_username){
		        Dajaxice.profiles.get_panel_html(common_fn.update_personal_panel_with_user,{'username':django_username});
		    }
		},
		
		find_dooms_related_to_competition : function (id){
		    page_state.page=1;
		    page_state.search.init(Dajaxice.competition_search.related,doom_search.update_work_panel_with_doom_search,{'competition_id':id});
			Dajaxice.competition_search.related(doom_search.update_work_panel_with_doom_search,{'competition_id':id,'page':page_state.page});
		    if(page_state.personal_panel.user != django_username){
		        Dajaxice.profiles.get_panel_html(common_fn.update_personal_panel_with_user,{'username':django_username});        
		    }
		},
		
		add_competition_to_doom_create_form : function (id){
			Dajaxice.competition_search.competition_configure_html(
					function(data){
						ajax_select.add_item("#id_page-competition",id,data);
					},
					{'id':id}
				);
		},
		
		hide_search_results_in_doom_create_page : function (){
		    $('#work-panel #doomsearch-panel').hide();
		}
	}
});