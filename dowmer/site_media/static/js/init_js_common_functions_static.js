define(['jquery',
        'page_state',
        'jqote2/jquery.jqote2.min',
        'badger/badger'
        ], function($,page_state){
		function replace_avatar_url(tag,user,url){
		    $(tag+user).html('<img src="'+url+'"/>');
		}
	return {
		setup_facebox : function () {
		    $.facebox.settings.closeImage = '/site_media/static/js/facebox/src/closelabel.png';
		    $.facebox.settings.loadingImage = '/site_media/static/js/facebox/src/loading.gif';
		    $.facebox.settings.closeOnClick = true;
		    $('a[rel*=facebox]').facebox({
		        loadingImage: '/site_media/static/js/facebox/src/loading.gif',
		        closeImage: '/site_media/static/js/facebox/src/closelabel.png'
		    });
		    
		 },
		
		replace_avatar : function (data){    
		    replace_avatar_url('.avatar_',data.user,data.url);
		},
		
		choose_not_me : function (user1,user2){
		    return user1==django_username?user2:user1;
		},
		
		ISODateString : function (d){
		 function pad(n){return n<10 ? '0'+n : n}
		 return d.getUTCFullYear()+'-'
		      + pad(d.getUTCMonth()+1)+'-'
		      + pad(d.getUTCDate())+'T'
		      + pad(d.getUTCHours())+':'
		      + pad(d.getUTCMinutes())+':'
		      + pad(d.getUTCSeconds())+'Z'
		 },
		
		prefix_slash : function (s){
			if(s[0]=='/') return s;
			return '/'+s;
		},
		
		 badge_notification_based_on_page_settings : function (){
		    if(page_state.interactive_notifications.visibility.show){
		        var nb = page_state.static_notifications.count + page_state.interactive_notifications.list.length;
		        if (nb==0)
		            $("#notifications").badger('');
		        else
		            $("#notifications").badger(nb.toString());
		    }
		},
	
		countProperties : function (obj) {
		    var count = 0;
		
		    for(var prop in obj) {
		        if(obj.hasOwnProperty(prop))
		                ++count;
		    }
		
		    return count;
		},
		
		notification_embed_page : function (html){
		    return $("#notification_embedder_page").jqote({'content':html});
		},
		
		update_personal_panel_with_user : function (data){	
			$("#personal-panel").html(data);
		}
	}
});