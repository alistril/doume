define(
		[ 'jquery', 'page_state_initialized','generic_help','qTip2' ],
		function($,page_state) {
			$(document).ready(function(){
				if(page_state.help.home.showcase)
					$('#selling-panel-controls').qtip({
						content: {
							text: 	'Here will appear dooms that might interest you.' +  
									'These dooms are compiled from your friends activity.' +
									'<ul>' +
									'<li>The Sales category selects interesting dooms you could buy.</li>' +
									'<li>The Victories category informs you about what others won</li>' +
									'</ul>'
									, 
							title: {
								text: 'Doom panel',
								button: true
							}
						},
						position: {
							my: "top left",
							at: "bottom left"
						},
						show: {
							event: false,
							ready: true, 
						},
						hide : false,
						events: {
							hide: function(){
								page_state.help.home.showcase = 0;
								page_state.help.sync();
							}
						},								
						style: {
							classes: 'ui-tooltip-blue ui-tooltip-shadow'
						}
					});
				if(page_state.help.home.activity)
					$('#home-activity-panel').qtip({
						content: {
							text: 	'Your friends activity ' +  
									'will be displayed here',
							title: {
								text: 'Home activity',
								button: true
							}
						},
						position: {
							my: "top center",
							at: "center" 
						},
						show: {
							event: false, 
							ready: true 
						},
						hide : false,
						events: {
							hide: function(){
								page_state.help.home.activity = 0;
								page_state.help.sync();
							}
						},								
						style: {
							classes: 'ui-tooltip-blue ui-tooltip-shadow'
						}
					});		
				if(page_state.help.home.doom_selling)
					$('.doom-sellbutton').parent().parent().qtip({
						content: {
							text: 	'This doom is yours, you can sell it. ' +  
									'To sell more of your dooms go to your ' + 
									'transaction menu (top of the page) ' + 
									'and select <strong>Sell doom</strong>.',
						},
						position: {
							my: "right center",
							at: "left center"
						},
						show: {
							delay: 500
						},
						//hide : 'unfocus',
						events: {
							hide: function(){
								//page_state.help.bank.crediting_transaction = 0;
								//page_state.help.sync();
							}
						},								
						style: {
							classes: 'ui-tooltip-green ui-tooltip-shadow'
						}
					});
				if(page_state.help.home.doom_buying)
					$('.doom-buybutton').parent().parent().qtip({
						content: {
							text: 	'Click <strong>buy</strong> to acquire this doom. ' +  
									'To buy more of dooms look up to the ' + 
									'<strong>sales</strong> panel or go to the ' + 
									'transaction menu (top of the page).',
							
						},
						position: {
							my: "right center",
							at: "left center"
						},
						show: {
							delay: 500
						},
						//hide : 'unfocus',
						events: {
							hide: function(){
								//page_state.help.bank.crediting_transaction = 0;
								//page_state.help.sync();
							}
						},								
						style: {
							classes: 'ui-tooltip-green ui-tooltip-shadow'
						}
					});
					
			}); //dom ready
		});