define(["jquery",
        "constants", 
        "urls"],function($,constants,urls){
	$(document).ready(function() {
		$("#payment-service-select-form select").change(function(){
			$.post(
					urls.resolve("payment_services_detail_form"), 
					$("#payment-service-select-form").serialize(),
					function(data) {
						$("#payment-service-config-form").html(data);
					});
		});
	}); 
});