define(['jquery',
        'urls',
        'dooms',
        "jqote2/jquery.jqote2.min",
        'jquery.form/jquery.form'
        ], function($,urls,dooms){
	return { 
		ajaxify_form : function(doomid,doom_buy_redirect){ 
			$(document).ready(function() {     
				$.facebox.settings.closeImage = '/site_media/static/js/facebox/src/closelabel.png';
				$.facebox.settings.loadingImage = '/site_media/static/js/facebox/src/loading.gif';
				var url;
				if (doom_buy_redirect != undefined)					
					urls.resolve("buy",{id: doomid,doom_buy_redirect:doom_buy_redirect});
				else
					urls.resolve("buy",{id: doomid});
				$(".buy_doom_form#doom"+doomid).attr("action",url);

				$(".buy_doom_form#doom"+doomid).ajaxForm(
						{
							dataType:  'json',
							beforeSubmit:  function(formData, jqForm, options) {
								$.facebox.settings.closeOnClick = false;
								$.facebox(function(){}); //display a waiting clock                
								return true; 
							} ,
							success: function(data){
								$.facebox.settings.closeOnClick = true;
								dooms.doom_buy_cb(data);
							},
							error: function(){
								$.facebox.settings.closeOnClick = true;
								$.facebox($("#doom_buy_unknown_fail_page").jqote({}));                
							}
						});
			});
		}
	};
});