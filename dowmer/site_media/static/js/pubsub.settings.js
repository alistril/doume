define(function(){
	return {
	    BOSH_SERVICE: '/http-bind/',
	    DOMAIN: 'localhost',
	    RESOURCE: 'strophejs',
	    PUBSUB_SERVICE: 'pubsub.localhost',
	    DEBUG: true,
	    DEBUG_USE_INTERNAL_CONSOLE: false
	};
});
