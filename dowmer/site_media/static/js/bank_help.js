define(
		[ 'jquery', 'page_state_initialized','generic_help','qTip2' ],
		function($,page_state) {
			$(document).ready(function(){
				if(page_state.help.bank.operations)
					$('#credit-operations').qtip({
						content: {
							text: 	'Here you transfer funds to or from your doume acount. ' +  
									'You can add money via paypal for example.'
									, 
							title: {
								text: 'Transfer funds',
								button: true
							}
						},
						position: {
							my: "top left", // Use the corner...
							at: "bottom left" // ...and opposite corner
						},
						show: {
							event: false,
							ready: true, // ... but show the tooltip when ready
						},
						hide : false,
						events: {
							hide: function(){
								page_state.help.bank.operations = 0;
								page_state.help.sync();
							}
						},								
						style: {
							classes: 'ui-tooltip-blue ui-tooltip-shadow'
						}
					});
				if(page_state.help.bank.transactions)
					$('#bank-transactions').qtip({
						content: {
							text: 	'Here is a list of the transactions you mande ' +  
									'and those made to you',
							title: {
								text: 'Transactions',
								button: true
							}
						},
						position: {
							my: "top center", // Use the corner...
							at: "bottom center" // ...and opposite corner
						},
						show: {
							event: false, // Don't specify a show event...
							ready: true // ... but show the tooltip when ready
						},
						hide: false,
						events: {
							hide: function(){
								page_state.help.bank.transactions = 0;
								page_state.help.sync();
							}
						},								
						style: {
							classes: 'ui-tooltip-blue ui-tooltip-shadow'
						}
					});	
				
				if(page_state.help.bank.crediting_transaction)
					$('.transaction-credit').qtip({
						content: {
							text: 	'This transaction added credits to your account. ' +  
									'Transactions crediting you money are marked in blue.',
						},
						position: {
							my: "bottom center", // Use the corner...
							at: "center" // ...and opposite corner
						},
						show: {
							delay: 500
						},
						//hide : 'unfocus',
						events: {
							hide: function(){
								//page_state.help.bank.crediting_transaction = 0;
								//page_state.help.sync();
							}
						},								
						style: {
							classes: 'ui-tooltip-green ui-tooltip-shadow'
						}
					});
				if(page_state.help.bank.debiting_transaction)
					$('.transaction-debit').qtip({
						content: {
							text: 	'This transaction pays credits from your account. ' +  
									'Transactions debiting your money are marked in red.',
						},
						position: {
							my: "bottom center", // Use the corner...
							at: "center" // ...and opposite corner
						},
						show: {
							delay: 500
						},
						//hide : 'unfocus',
						events: {
							hide: function(){
								//page_state.help.bank.crediting_transaction = 0;
								//page_state.help.sync();
							}
						},								
						style: {
							classes: 'ui-tooltip-green ui-tooltip-shadow'
						}
					});
					
			}); //dom ready
		});