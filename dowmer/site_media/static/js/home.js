define(['jquery', 
        'console',
        'pubsub.settings',
        'constants',
        'init_js_common_functions_static',
        'protocol',
        'urls', //for jqote templates
        'dooms',
        'jqote2/jquery.jqote2.min',
        'jquery.jqote_postprocess'
        ], 
        function($,myconsole,settings,constants,common_fn,protocol){
	
	function doom_sub_response(data){
		if(settings.DEBUG)
			myconsole.log(data);

	}

	return { 
		event_cb : function(obj, node,conn) {
			try{
				if(settings.DEBUG){
					myconsole.log("this is home");
					myconsole.log(obj);
					myconsole.log(node);
				}
				switch(obj.type){
				case constants.NotifTypes.DOOM_BOUGHT:
					$('#frontsale_doom_'+obj.doom.id).remove();
					break;
				case constants.NotifTypes.INVITATION_IS_ACCEPTED:
				case constants.NotifTypes.ACCEPTED_INVITATION:
					if(obj.invitation.to == django_username || obj.invitation.from == django_username){
						Dajaxice.home.home_global_display(Dajax.process,{});
					}
					break;
	
				}
	
				if(node.substring((node).length-8)=="/friends"){
					if(obj.wallpost_owner==django_username){
						return;
					}else if('writer' in obj){
						if(obj.writer==django_username){
							return;
						}
					}
				}
	
				switch(obj.type){
				case constants.NotifTypes.DOOM_NEW_SALE:                
					if(node.substring((node).length-8)=="/friends"){
						if(!$('.wallpost_doom_'+obj.doom.id).length){
							myconsole.log('subscribing to doom ' + obj.doom.id);
							Dajaxice.pubsub.subscribe_to_doom(doom_sub_response,{'jid':protocol.xmpp_credentials.jid,'doomid':obj.doom.id});
						}
						$('#home-contents #home-activity-panel').prepend("</li>");
						$('#home-contents #home-activity-panel').prepend($('#wallpost_doom').jqote(obj)).jqote_postprocess();
						$('#home-contents #home-activity-panel').prepend($('#wallpost_users_interaction').jqote(obj));
						$('#home-contents #home-activity-panel').prepend("<li>");
						$('#selling-panel').prepend($("#frontsales_doom").jqote(obj)).jqote_postprocess();                    
					}
					if(node.substring(0,6)=="/dooms"){
						$('.wallpost_doom_'+obj.doom.id+'> div > #wallpost_doom_interaction').replaceWith($("#wallpost_doom_interaction").jqote(obj)).jqote_postprocess();
					}
					break;
				case constants.NotifTypes.DOOM_BOUGHT:
					if(node.substring((node).length-8)=="/friends"){
						if(!$('.wallpost_doom_'+obj.doom.id).length){
							myconsole.log('subscribing to doom ' + obj.doom.id);
							Dajaxice.pubsub.subscribe_to_doom(doom_sub_response,{'jid':protocol.xmpp_credentials.jid,'doomid':obj.doom.id});
						}      
						$('#home-contents #home-activity-panel').prepend("</li>");
						$('#home-contents #home-activity-panel').prepend($('#wallpost_doom').jqote(obj)).jqote_postprocess();                		
						$('#home-contents #home-activity-panel').prepend($('#wallpost_users_interaction').jqote(obj)).jqote_postprocess();
						$('#home-contents #home-activity-panel').prepend("<li>");
						$('#frontsale_doom_'+obj.doom.id).remove();
					}
					if(node.substring(0,6)=="/dooms"){
						$('.wallpost_doom_'+obj.doom.id+'> div > #wallpost_doom_interaction').replaceWith($("#wallpost_doom_interaction").jqote(obj)).jqote_postprocess();
					}
					break;
				case constants.NotifTypes.DOOM_STATUS:
					var doom = obj.notification.content_object.doom;
	
					if(node.substring((node).length-8)=="/friends"){
						if(doom.owner!=django_username){ //requests on me are not handeled by the default rejection case (above) cause the status change notif is not a wallpost
							$('#frontsale_doom_'+doom.id).remove();
							if(doom.status==constants.DoomStatus.WON){
								var wallpost = jQuery.extend(obj.notification.content_object.wallpost,obj.notification.content_object.wallpost.content_object);
								wallpost["doom"]=doom;
								wallpost["id"]=obj.id;
								$('#home-contents #home-activity-panel').prepend("<li>"+$('#wallpost_users_interaction').jqote(wallpost)+$('#wallpost_doom').jqote(wallpost)+"</li>").jqote_postprocess();
								$('#winning-panel').prepend($("#victories_doom").jqote(wallpost)).jqote_postprocess();
							}
						}
					}
					if(node.substring(0,6)=="/dooms"){
						$('.wallpost_doom_'+doom.id+'> div > #wallpost_doom_interaction').replaceWith($("#wallpost_doom_interaction").jqote({'doom':doom})).jqote_postprocess();
					}
					break;
				case constants.NotifTypes.DOOM_SOLD:                	
					if(node.substring((node).length-8)=="/friends"){             		
						$('#frontsale_doom_'+obj.doom.id).remove();
					}
					if(node.substring(0,6)=="/dooms"){
						$('.wallpost_doom_'+obj.doom.id+'> div > #wallpost_doom_interaction').replaceWith($("#wallpost_doom_interaction").jqote(obj)).jqote_postprocess();
					}
					break;
				case constants.NotifTypes.INVITATION_IS_ACCEPTED:
					if(node.substring((node).length-8)=="/friends"){
						if($('#invitation_'+obj.invitation.id).length){ //the tag exists
							//do nothing
						}else{       
							$('#home-contents #home-activity-panel').prepend("</li>");
							$('#home-contents #home-activity-panel').prepend($('#wallpost_friendship').jqote(obj));
							$('#home-contents #home-activity-panel').prepend($('#wallpost_users_interaction').jqote(obj));
							$('#home-contents #home-activity-panel').prepend("<li>");
						}
					}
					break;
				case constants.NotifTypes.ACCEPTED_INVITATION:
					if(node.substring((node).length-8)=="/friends"){
						if($('#invitation_'+obj.invitation.id).length){ //the tag exists
							//do nothing
						}else{                    	
							$('#home-contents #home-activity-panel').prepend("</li>");
							$('#home-contents #home-activity-panel').prepend($('#wallpost_friendship').jqote(obj));
							$('#home-contents #home-activity-panel').prepend($('#wallpost_users_interaction').jqote(obj));
							$('#home-contents #home-activity-panel').prepend("<li>");
						}
					}
					break;
				case constants.NotifTypes.NEWS_SENT:                
				case constants.NotifTypes.NEWS_RECIEVED:
					if(node.substring((node).length-8)=="/friends"){
						if($('#wallpost_'+obj.id).length){ //the tag exists
							//do nothing
						}else{                    	
							$('#home-contents #home-activity-panel').prepend("</li>");
							$('#home-contents #home-activity-panel').prepend($('#wallpost_doom').jqote(obj)).jqote_postprocess();
							$('#home-contents #home-activity-panel').prepend($('#wallpost_users_interaction').jqote(obj));
							$('#home-contents #home-activity-panel').prepend("<li>");
						}
					}
					break;
				default:
					break;
				}
				common_fn.setup_facebox();
				$('span.timeago').timeago();
	
			}catch(error){
				if(settings.DEBUG)
					myconsole.log("exception:"+error.name)
			}
		}
	};
});