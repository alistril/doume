define(['jquery', 
        'console',
        'constants',
        'urls', //for jqote templates
        'jqote2/jquery.jqote2.min',
        'jquery.jqote_postprocess'
        ], 
        function($,myconsole,constants){
	return {
		doom_buy_cb : function (data){
			$.facebox.settings.closeImage = '/site_media/static/js/facebox/src/closelabel.png';
			$.facebox.settings.loadingImage = '/site_media/static/js/facebox/src/loading.gif';
			switch(data.transfer){
			case constants.DoomTransfer.TRANSACTION_COMPLETE:  		
				$.facebox("Payment finished.<br/>Click anywhere on the page to close this window.");
				break;
			case constants.DoomTransfer.TRANSACTION_REDIRECT:
				window.location.replace(data.redirect_url)
				break;
			case constants.DoomTransfer.TRANSACTION_FAILED_AND_REDIRECT:
				if(data.message_type==constants.DoomTransferDetail.NOT_LOGGED_IN){
					$.facebox($("#redirect_to_login_facebox").jqote(data));
					window.location.href = urls.resolve("login",{next:common_fn.prefix_slash(window.location.pathname)});
				}else{
					window.location.href = data.redirect_url;
				}             
				break;
			case constants.DoomTransfer.TRANSACTION_FAILED:
				switch(data.message_type){
				case constants.DoomTransferDetail.NOT_ON_SALE:    				
					$.facebox("This doom is not for sale.<br/>Click anywhere on the page to close this window.");
					break;
				case constants.DoomTransferDetail.CANNOT_BUY_FROM_SELF:
					$.facebox("You are the owner of this doom. You cannot buy from yourself.<br/>Click anywhere on the page to close this window.");
					break;
				case constants.DoomTransferDetail.PAYMENT_FAILED:        			
					$.facebox("Payment failed.<br/>Click anywhere on the page to close this window.");
					break;
				case constants.DoomTransferDetail.FORBIDDEN_STATUS:    				
					$.facebox("This doom is locked, you cannot buy it.<br/>It is locked because all of its competitions are finished or are almost finished.");
					break;
				}
				break;
			}
		},
		preview_doom : function (tag,id){
			function preview_doom_attempts(tag,id,attempt,max_attempts){
				if(attempt>max_attempts) return;

				Dajaxice.dooms.preview_doom(
						function(data){	                
							if(data.type=="success"){
								$(tag).html(data.html);
							}else{
								$(tag).html($("#loading_menu_next_attempt").jqote({"attempt":attempt,"max_attempts":max_attempts-1}));
								setTimeout(function(){preview_doom_attempts(tag,id,attempt+1,max_attempts);},2000);
							}
						},			
						{"doomid":id});
			}
			
			preview_doom_attempts(tag,id,0,10);
		},		

		preview_doom_victory : function (tag,id){
			function preview_doom_victory_attempts(attempt,max_attempts){
				if(attempt>max_attempts) return;

				Dajaxice.dooms.preview_doom_victory(
						function(data){	                
							if(data.type=="success"){
								$(tag).html(data.html);
							}else{
								$(tag).html($("#loading_menu_next_attempt").jqote({"attempt":attempt,"max_attempts":max_attempts-1}));
								setTimeout(function(){preview_doom_attempts(tag,id,attempt+1,max_attempts);},2000);
							}
						},			
						{"doomid":id});
			}
			
			preview_doom_victory_attempts(0,10);
		}
	};
});