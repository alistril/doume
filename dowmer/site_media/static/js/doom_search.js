define(['jquery', 
        'page_state',
        'protocol',
        'pubsub.client',
        'console',
        'constants',
        'init_js_common_functions_static',
        'notifications',
        'pagination'
        ], function($,page_state,protocol,client,myconsole,constants,common_fn,notifications){
			return {
				update_work_panel_with_doom_search : function (data){
				    if(client.pubsub_conn!=undefined)
				    	client.pubsub_conn.disconnect();
				    page_state.personal_panel.user = django_username;
					$("#id_search_field_text").autocomplete('close');
					$("#work-panel").html(data);   
					$("span.timeago").timeago();
					common_fn.setup_facebox();
				    /*$.ajax(
						{
							type : 'POST',
							url : '/async',
							dataType : 'json',
							data : xmpp_options,
							success : function(data){				
								//console_log(data);
				                xmpp_credentials = 
				                    {
				                        jid : data.jid,
				                        rid : data.rid,
				                        sid : data.sid
				                    };
								client.init_pubsub(xmpp_credentials.jid,xmpp_credentials.rid,xmpp_credentials.sid,notification_event_cb);
							},
							error : function(XMLHttpRequest, textStatus, errorThrown) {
								console_log('error');
								console_log(textStatus);
								console_log(errorThrown);
							}
						}
				        );*/
					protocol.xmpp_options = {'page' : page_state.page};
					protocol.init_session(notifications);
				},
				
				submit_search_bar_doom : function (){	
				    page_state.page=1;
					var search_val = $("#search_bar_doom_form input:text").val();    
				    page_state.search.init(Dajaxice.doom_search.execute,this.update_work_panel_with_doom_search,{'search_val':search_val});
					Dajaxice.doom_search.execute(this.update_work_panel_with_doom_search,{'search_val':search_val,'page':page_state.page});
				    if(page_state.personal_panel.user != django_username){
				        Dajaxice.profiles.get_panel_html(common_fn.update_personal_panel_with_user,{'username':django_username});
				    }
				}
			}
});
