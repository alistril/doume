define(
		[ 'jquery', 'page_state_initialized','generic_help','qTip2' ],
		function($,page_state) {
			$(document).ready(function(){
				if(page_state.help.wall.showcase)
					$('#selling-panel-controls').qtip({
						content: {
							text: 	'Here will appear dooms you own.' +
									'<ul>' +
									'<li>The Sales category selects interesting dooms you could buy.</li>' +
									'<li>The Victories category informs you about what others won</li>' +
									'</ul>'
									, 
							title: {
								text: 'Doom panel',
								button: true
							}
						},
						position: {
							my: "top left", // Use the corner...
							at: "bottom left" // ...and opposite corner
						},
						show: {
							event: false,
							ready: true, // ... but show the tooltip when ready
						},
						hide : false,
						events: {
							hide: function(){
								page_state.help.wall.showcase = 0;
								page_state.help.sync();
							}
						},								
						style: {
							classes: 'ui-tooltip-blue ui-tooltip-shadow'
						}
					});
				if(page_state.help.wall.activity)
					$('#wall-activity-panel').qtip({
						content: {
							text: 	'Your activity ' +  
									'will be displayed here',
							title: {
								text: 'Your wall',
								button: true
							}
						},
						position: {
							my: "top center", // Use the corner...
							at: "center" // ...and opposite corner
						},
						show: {
							event: false, // Don't specify a show event...
							ready: true // ... but show the tooltip when ready
						},
						hide : false,
						events: {
							hide: function(){
								page_state.help.wall.activity = 0;
								page_state.help.sync();
							}
						},								
						style: {
							classes: 'ui-tooltip-blue ui-tooltip-shadow'
						}
					});		
				if(page_state.help.wall.doom_selling)
					$('.doom-sellbutton').parent().parent().qtip({
						content: {
							text: 	'This doom is yours, you can sell it. ' +  
									'To sell more of your dooms go to your ' + 
									'transaction menu (top of the page) ' + 
									'and select <strong>Sell doom</strong>.',
						},
						position: {
							my: "right center",
							at: "left center"
						},
						show: {
							delay: 500
						},
						//hide : 'unfocus',
						events: {
							hide: function(){
							}
						},								
						style: {
							classes: 'ui-tooltip-green ui-tooltip-shadow'
						}
					});
				if(page_state.help.home.doom_buying)
					$('.doom-buybutton').parent().parent().qtip({
						content: {
							text: 	'Click <strong>buy</strong> to acquire this doom. ' +  
									'To buy more of dooms look up to the ' + 
									'<strong>sales</strong> panel or go to the ' + 
									'transaction menu (top of the page).',
							
						},
						position: {
							my: "right center",
							at: "left center"
						},
						show: {
							delay: 500
						},
						//hide : 'unfocus',
						events: {
							hide: function(){
							}
						},								
						style: {
							classes: 'ui-tooltip-green ui-tooltip-shadow'
						}
					});
					
			}); //dom ready
		});