define(function(){
	return { 
		resolve : function(name,dict){	
			switch(name){
			case "root":
				return "/"
				break;
			case "static_js":
				return "/site_media/static/js/"
				break;
			case "profiles":
				return '/profiles/profile/' + dict.username;
				break;
		    case "buy_doom":
		        return '/dooms/buy/'+dict.doomid+'/?buy_doom_redirect='+dict.buy_doom_redirect;
		        break;
		    case "sell_doom":
		        return '/dooms/sell/'+dict.doomid+'/';
		        break;
		    case "wall":
		        return '/walls/'+dict.username;
		        break;
		    case "bank_account":
		        return '/bank/account';
		        break;
		    case "interactive_notifications":
		    	return '/interactive_notifications/all';
		    	break;
		    case "login":
		        url = '/accounts/login/'
		        if(dict!= undefined && dict.next!=undefined){
		            url+='?next='+dict.next;
		        }
		        return url;
		        break;
			case "payment_services_detail_form":
				return "/payment_services/get_detail_form/";
				break;
		    case "add_payment_service":
		    	return "/payments/add_service/";
		    	break;
		    case "buy":
		    	if (dict.buy_doom_redirect)
		    		return "/dooms/buy/" + dict.id + "/?buy_doom_redirect=" + dict.buy_doom_redirect;
		    	else
		    		return "/dooms/buy/" + dict.id + "/?buy_doom_redirect=" + window.location.href;
		    	break;
		    case "doom_create":
		    	return "/dooms/create/";
		    	break;			
			}		
		}
	}
});