define(['jquery',
        'constants'       
        ], function($,constants){

	$.extend({
	  getUrlVars: function(){
	    var vars = [], hash;
	    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
	    for(var i = 0; i < hashes.length; i++)
	    {
	      hash = hashes[i].split('=');
	      vars.push(hash[0]);
	      vars[hash[0]] = hash[1];
	    }
	    return vars;
	  },
	  getUrlVar: function(name){
	    return $.getUrlVars()[name];
	  }
	});

	return {
		display_transaction_facebox : function (transaction){    
		  	$.facebox.settings.closeImage = '/site_media/static/js/facebox/src/closelabel.png';
			$.facebox.settings.loadingImage = '/site_media/static/js/facebox/src/loading.gif';
		  	switch(transaction.status){
		  		case constants.TransactionStatus.PAYMENT_SUCCESS_THROUGH_ACCOUNT:
		        case constants.TransactionStatus.PAYMENT_SUCCESS_THROUGH_SERVICE:
		        case constants.TransactionStatus.TRANSACTION_SUCCESS:
		  			$.facebox("Payment finished.<br/>Click anywhere on the page to close this window.");
		  			break;
		    	case constants.TransactionStatus.PAYMENT_FAILED:
		    		$.facebox("Payment failed.<br/> No money will be transfered from your account<br/>Click anywhere on the page to close this window.")
		    		break;
		    	case constants.TransactionStatus.PAYMENT_REFUNDED:
		            if(transaction.doom && transaction.doom.status!=constants.DoomStatus.PENDING)
		                $.facebox("Payment successful but all competition of your doom are finished.<br/>You cannot buy a finished doom.<br/>Your credits were refunded back to your account.");
		            else if(!transaction.doom.on_sale){
		                $.facebox("Someone else bought the doom. Payment refunded.");
		            }
		            break;
		        case constants.TransactionStatus.PAYMENT_FAILED_CANCELED:
		            $.facebox("You have canceled your transaction.")
		    		break;
		    }
		},
		
		handle_payment_display : function (){
			var obj = this;
		    if($.getUrlVars()["transactionid"]){
		        var transactionid = $.getUrlVars()["transactionid"];
		        Dajaxice.payments.get_transaction(
		            function(data){
		                if(data.type=="success"){
		                    obj.display_transaction_facebox(data.transaction);
		                }
		            },
		            {"transactionid":transactionid}
		        );
		        
		    }
		}
	}
});