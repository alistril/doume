define(['jquery', 
        'page_state',
        'console', 
        'constants',
        'init_js_common_functions_static',
        'interactive_friends_common'
        ], function($,page_state,myconsole,constants,common_fn,interactive_friends_common){
	
	return {
		interactive_friends_event_cb : function(object, node,conn){
		    switch(object.type){        
		        case constants.NotifTypes.INVITATION_IS_ACCEPTED: //me or my friends issued an invitation which someone accepted
		        	if(object.invitation.from == django_username) //My friend has accepted my invitation
		                page_state.interactive_notifications.add(object);
		        		Dajaxice.interactive_friends.update_friends_panel(Dajax.process);
		        		if(page_state.interactive_notifications.verbose)
		        			interactive_friends_common.add_invitation_accepted(object.invitation);        		
		        			
		        	else{
		        		//one of my friends added someone
		        	}
		            break;
		        case constants.NotifTypes.ACCEPTED_INVITATION: //me or my friends accepted an invitation
		            if(object.invitation.to == django_username) //someone added me
		            	//do not add notification +1 if I accepted an invitation and even less my friends 
		                //page_state.interactive_notifications.add(object);
		            	Dajaxice.interactive_friends.update_friends_panel(Dajax.process);
		            	if(page_state.interactive_notifications.verbose)
		            		interactive_friends_common.update_invitation_accept(object.invitation);            	
		        			
		            else{ //someone added one of my friends
		                //
		            }
		            break;
		        case constants.NotifTypes.REJECTED_INVITATION: //I rejected an invitation (publishable to [nick]/only)
		        	if(object.invitation.to == django_username) //I rejected someone
		                page_state.interactive_notifications.add(object);
		        		if(page_state.interactive_notifications.verbose)
		        			interactive_friends_common.update_invitation_reject(object.invitation);        		
		        			
		            else{ //should not happen
		                myconsole.log("error: weird message.");
		            }
		        	break;
		        case constants.NotifTypes.INVITATION_IS_REJECTED: //My invitation was rejected (publishable to [nick]/only)
		        	if(object.invitation.from == django_username) //someone rejected me
		                page_state.interactive_notifications.add(object);
		        		if(page_state.interactive_notifications.verbose)
		        			interactive_friends_common.add_invitation_reject(object.invitation);
		        			
		            else{ //should not happen
		                myconsole.log("error: weird message.");
		            }
		        	break;
		        case constants.NotifTypes.INVITATION_SENT:
		        	
		        	break;
		    	case constants.NotifTypes.INVITATION_RECIEVED:
		            page_state.interactive_notifications.add(object);
		    		if(page_state.interactive_notifications.verbose){
		    			$(".no-notifications").remove();
		    			$("#notifications-page").prepend(common_fn.notification_embed_page(interactive_friends_common.friendshipinvitation_page_template(object.invitation)));
		    		}
		    		break;
		        default:
		            break;
		    }
		    $("span.timeago").timeago();
		}
	}
});