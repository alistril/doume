define(['jquery', 'pubsub.settings', 'loaders/strophejs',"console"], function($,settings,strophejs,myconsole){
	return {
		pubsub_conn : undefined,
		
		init_pubsub : function(jid,rid,sid,message_handler){
			var conn = new strophejs.Connection(settings.BOSH_SERVICE);
			
			function onconnect(status,error){
				if (settings.DEBUG) { 
		        	myconsole.log("status:"+status);
		        	myconsole.log("error:"+error);
		       	}
		        
		        if(status == strophejs.Status.ATTACHED && settings.DEBUG){
		            myconsole.log("The connection has been attached");
		            myconsole.log("my jid=" + jid);
		        }
		    }
			
			conn.addHandler(
		                function(stanza_xml) {
		                    
		                    if (settings.DEBUG) {        
				                myconsole.log(stanza_xml);
				            }
		                    if($(stanza_xml).find("items").length>0){                        
		                        var items = $(stanza_xml).find("items")                        
		                        var item = items[0];                        
		                        var node = $(item).attr("node");                        
		                        var entry = $(stanza_xml).find("entry");                        
		                        if (entry.length) {
		                            
		                            message_handler(entry, node,conn);
		                        }
		                    }
				            return true;
		                },
						  null,
						  'message',
						  null,
						  null,
						  null);
		    conn.attach(jid, sid, rid, onconnect);
			
			this.pubsub_conn = conn;
		}
	}
});