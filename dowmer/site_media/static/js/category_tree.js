define(['jquery', 
        'console',
        'constants',    
        'urls',
        'loaders/jstree'        
        ], 
    function($,myconsole,constants,urls){
		function navigate(node){			
			$("#category_tree").jstree("deselect_all");
			$("#category_tree").jstree("select_node","#"+node,true);
		}
		
		function toggle_category_display(node,val){    
		    if(val)
		        Dajaxice.categories.detail_display(Dajax.process,{'n':node});
		    else
		        Dajaxice.categories.category_display(Dajax.process,{'n':node});
		        
		}

		$(document).ready(			
		    function () {		    		
		            // TO CREATE AN INSTANCE
		            // select the tree container using jQuery
		            $("#category_tree")
		                // call `.jstree` with the options object
		                .jstree({
		                    // the `plugins` array allows you to configure the active plugins on this instance
		                    "plugins" : ["themes","json","ui","crrm","hotkeys"],
		                    // each plugin you have included can have its own config object
		                    "themes" : {
		                    	"theme" : "default",
		        	            "dots" : true,
		        	            "icons" : true,
		                    	"url" : urls.resolve("static_js") + "jstree/src/themes/default/style.css"
		                    },
		                    "ui": {
		                    	"select_limit" : 1
		                    },
		                    "json" : {
		                    	"progressive_render" : true,
		                        "ajax" : {   
		                            "type": "GET",
		                            //THIS IS NOT EXACTLY A VALID POST BUT IT WORKS. TODO: remove the resulting dajaxice error
		                            "url": "/categories/tree/list_nodes/",
		                            "data" : function (n) {                            	
		                                return { id : n.attr ? n.attr("id") : -1 };
		                            }                            
		                        },
		                        "core" : {  
		                        }
		                        // it makes sense to configure a plugin only if overriding the defaults
		                    }
		                }).on("select_node.jstree", function (event, data) { 
							Dajaxice.categories.category_display(Dajax.process,{'n':data.rslt.obj.attr("id")});   
							$("#category_tree").jstree("open_node","#"+data.rslt.obj.attr("id"));
							
						}).on("__loaded.jstree",function(e, data){
			            	var opennode_list = document.location.pathname.match(/[0-9]+/gi);
			        		
			            	if (opennode_list.length) {
			            	   var opennode = opennode_list[0];
			            	   
			            	   navigate(opennode);    	   			        	   
			            	}
			            });

		            	
		            $(document).ajaxSend(function(event, xhr, settings) {
		                function getCookie(name) {
		                    var cookieValue = null;
		                    if (document.cookie && document.cookie != '') {
		                        var cookies = document.cookie.split(';');
		                        for (var i = 0; i < cookies.length; i++) {
		                            var cookie = jQuery.trim(cookies[i]);
		                            // Does this cookie string begin with the name we want?
		                            if (cookie.substring(0, name.length + 1) == (name + '=')) {
		                                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
		                                break;
		                            }
		                        }
		                    }
		                    return cookieValue;
		                }
		                function sameOrigin(url) {
		                    // url could be relative or scheme relative or absolute
		                    var host = document.location.host; // host + port
		                    var protocol = document.location.protocol;
		                    var sr_origin = '//' + host;
		                    var origin = protocol + sr_origin;
		                    // Allow absolute or scheme relative URLs to same origin
		                    return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
		                        (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
		                        // or any other URL that isn't scheme relative or absolute i.e relative.
		                        !(/^(\/\/|http:|https:).*/.test(url));
		                }
		                function safeMethod(method) {
		                    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
		                }
		
		                if (!safeMethod(settings.type) && sameOrigin(settings.url)) {
		                    xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
		                }
		            });
		            
		            $("#doom-categories-panel").on("click","a.category-tree-navigate",function(e){
		            	e.preventDefault();
		            	navigate(parseInt($(this).attr("id")));		            	
		            });
		            
		            $("#doom-categories-panel").on("click","a.category-tree-toggle-category",function(e){
		            	e.preventDefault();
		            	toggle_category_display(parseInt($(this).attr("id")),$(this).attr("value")=="true");		            	
		            });
		            
		            
		        }		    
		);
		return {
			"navigate" : navigate,
			"toggle_category_display" : toggle_category_display,
			event_cb : function (obj, node,conn) {        
			    try{        
			    	myconsole.log("this is categories");
			        switch(obj.type){
			            case constants.NotifTypes.DOOM_NEW_SALE:
			                
			            case constants.NotifTypes.DOOM_BOUGHT:
			            case constants.NotifTypes.DOOM_SOLD:                   
			                Dajaxice.categories.prefered_dooms_display(Dajax.process);
			                break;
			            default:
			                break;
			        }
			        
			    }catch(error){
			    	myconsole.log("exception:"+error.name)
			    }
			}
		}
});
