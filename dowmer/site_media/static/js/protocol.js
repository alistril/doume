define(["console","pubsub.client","notifications"],function(myconsole, client){
	return {
		xmpp_options : {},
		xmpp_other_init : {},
		dajaxice_settings : {'error_callback': function(data){
            myconsole.log("ajax interrupted");
            myconsole.log(data);
            return false;
        }},
		xmpp_credentials : {},
		init_session : function(notifications){
			self = this;
			Dajaxice.im.create_im_session(
					function(data){
						self.xmpp_credentials = 
						{
								jid : data.jid,
								rid : data.rid,
								sid : data.sid
						};
						client.init_pubsub(self.xmpp_credentials.jid,self.xmpp_credentials.rid,self.xmpp_credentials.sid,function(entry, node,conn){notifications.notification_event_cb(entry, node,conn);});        
						if(self.xmpp_other_init.call){
							self.xmpp_other_init.call(client.pubsub_conn);
						}
					},
					self.xmpp_options,
					self.dajaxice_settings
			);
		}
	}
});
