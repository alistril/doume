from django import forms

from interface.models import Interface

class HomePageForm(forms.ModelForm):
    def display_homepage(self):
        return self.cleaned_data["display_homepage"]
                 
    class Meta:
        model = Interface        
        fields = ('display_homepage',)
