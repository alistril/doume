# -*- coding: utf-8 -*-
# Django settings for crapper project.

import os.path
import posixpath
import logging.config
import sys


PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))

DEBUG = True
TEMPLATE_DEBUG = DEBUG

SERVE_MEDIA = DEBUG

INTERNAL_IPS = [
    "127.0.0.1",
]

SITE_URL = "http://localhost"

ADMINS = [
    # ("Your Name", "your_email@domain.com"),
]

MANAGERS = ADMINS


DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3", # Add "postgresql_psycopg2", "postgresql", "mysql", "sqlite3" or "oracle".
        "NAME": os.path.join(PROJECT_ROOT, "dev.db"),                       # Or path to database file if using sqlite3.
        "USER": "",                             # Not used with sqlite3.
        "PASSWORD": "",                         # Not used with sqlite3.
        "HOST": "",                             # Set to empty string for localhost. Not used with sqlite3.
        "PORT": "",                             # Set to empty string for default. Not used with sqlite3.
    }     
}


# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = "Europe/Paris"
USE_TZ = True

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = "en-us"

REGISTRATION_OPEN = True
LOGIN_REDIRECT_URL = "/home/"

AUTH_PROFILE_MODULE = "profiles.Profile"

XMPP_HOST = "localhost"
MUC_XMPP_HOST = "conference.localhost"
XMPP_BOSH_PORT = 4444
XMPP_PUBSUB_HOST = "pubsub.localhost"
XMPP_JID = "doume@localhost"
XMPP_RESOURCE = "strophejs"
XMPP_PASSWORD = "doume"
BOSH_SERVICE = '/http-bind/'
TUNNEL_EJABBERD_AUTH_GATEWAY_LOG = "ej_log.txt"

HELPTOOLTIPS_DEFAULT_ENABLED = False
HELPTOOLTIPS_DEFAULT = {
                        'wall':{
                              'doom_selling':1, 
                              'activity':1, 
                              'doom_buying':1, 
                              'showcase':1
                              }, 
                        'home':{
                              'doom_selling':1, 
                              'activity':1, 
                              'doom_buying':1, 
                              'showcase':1
                              }, 
                        'bank':{
                              'operations':1, 
                              'crediting_transaction':1, 
                              'debiting_transaction':1, 
                              'transactions':1
                              }, 
                        'general':{
                                 'notifications':1, 
                                 'doom_create':1, 
                                 'transactions':1,
                                 'help_display':1
                                 }, 
                        }

HELPTOOLTIPS_DISABLED = {
                        'wall':{
                              'doom_selling':1, 
                              'activity':0, 
                              'doom_buying':1, 
                              'showcase':0
                              }, 
                        'home':{
                              'doom_selling':1, 
                              'activity':0, 
                              'doom_buying':1, 
                              'showcase':0
                              }, 
                        'bank':{
                              'operations':0, 
                              'crediting_transaction':1, 
                              'debiting_transaction':1, 
                              'transactions':0
                              }, 
                        'general':{
                                 'notifications':0, 
                                 'doom_create':0, 
                                 'transactions':0,
                                 'help_display':0
                                 }, 
                        }

MIN_DOOM_PRICE = 1
MAX_DOOM_PRICE = 100
DEFAULT_CATEGORY_NAME_WHEN_NO_CATEGORIES_DEFINED = "All sports"
SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True


# URL that handles the static files like app media.
# Example: "http://media.lawrence.com"
STATIC_URL = "/site_media/static/"
MEDIA_URL = "/site_media/media/"

STATICFILES_DIRS = [    
    os.path.join(PROJECT_ROOT, "site_media", "static"),
    os.path.join(PROJECT_ROOT, "media"),
]
# URL prefix for admin media -- CSS, JavaScript and images. Make sure to use a
# trailing slash.
# Examples: "http://foo.com/media/", "/media/".
ADMIN_MEDIA_PREFIX = posixpath.join(STATIC_URL, "admin/")

#DAJAXICE_MEDIA_PREFIX="dajaxice"
TEST_RUNNER = 'django_nose.NoseTestSuiteRunner'

# Make this unique, and don't share it with anybody.
SECRET_KEY = "*m41h48v*-ck81k)b*o85u26_)34*gvi8oip$y83yf1&=3*ztp"

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = [
    "django.template.loaders.filesystem.Loader",
    "django.template.loaders.app_directories.Loader",
    "django.template.loaders.eggs.Loader",
]

MIDDLEWARE_CLASSES = [
    "django.middleware.common.CommonMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "groups.middleware.GroupAwareMiddleware",
    "debug_toolbar.middleware.DebugToolbarMiddleware",
    "pagination.middleware.PaginationMiddleware",
    
    #keep this the last line: more info on http://docs.djangoproject.com/en/dev/topics/db/transactions/
    'django.middleware.transaction.TransactionMiddleware',    
]

ROOT_URLCONF = "dowmer.urls"

TEMPLATE_DIRS = [
    os.path.join(PROJECT_ROOT, "templates"),
]

TEMPLATE_CONTEXT_PROCESSORS = [
    "django.contrib.auth.context_processors.auth",    
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "django.core.context_processors.request",
    "django.contrib.messages.context_processors.messages",
    
    "staticfiles.context_processors.static",
    
    "profiles.context_processors.profile",    
    "dooms.context_processors.tlv",
    "doom_search.context_processors.search_bar",
    "competition_search.context_processors.search_bar",
    "transactionbutton.context_processors.search_forms",
    "interactive_notification.context_processors.unread",
    "help.context_processors.tooltip_config"
]

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'dajaxice.finders.DajaxiceFinder',
)


INSTALLED_APPS = [
    # Django
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.sites",
    "django.contrib.messages",
    "django.contrib.humanize",
    "django.contrib.staticfiles",
    
    # external
    "timezones",
    "emailconfirmation",    
    "debug_toolbar",
    "pagination",
    "uni_form",
    "ajax_validation",
    "avatar",    
    "oembed",
    "groups",
    "tagging",
    "tagging_ext",
    "mptt",
    "haystack",  
    "registration",    
    "registration_backends",
    "jsonfield",
    
    "luthor",
    
    "dajaxice",
    "dajax",
    
    "ajax_select",
    "django_nose",


    # project
    "profiles",        
    "friends",
    "interface",
    "teams",
    "interactive_notification",
    "interactive_friends",
    "wall",
    "dooms",    
    "payments",
    "payment_services",
    "bank",
    "categories",
    "home",
    "transactionbutton",
    "miscutils",
    "pubsub",
    "im",
    "doom_search",
    "competition_search",
    "chat",
    "betting_sites",
    "doc",
    "errors",
    "pilot",
    "common_test_dependencies",    
    "help",
    "twisted_server",
    "gunicorn"
    
]

FIXTURE_DIRS = [
    os.path.join(PROJECT_ROOT, "fixtures"),
]

LOGIN_URL = "/accounts/login/" # @@@ any way this can be a url name?
LOGIN_REDIRECT_URLNAME = "home"
MESSAGE_STORAGE = "django.contrib.messages.storage.session.SessionStorage"

HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'haystack.backends.whoosh_backend.WhooshEngine',
        'PATH': 'whoosh/dowmer_index/index.whoosh',
    },
}

AJAX_SELECT_BOOTSTRAP = False
AJAX_SELECT_INLINES = False
AJAX_LOOKUP_CHANNELS = {
    # or write a custom search channel and specify that using a TUPLE
    'select_competition' : ('dooms.lookups', 'CompetitionLookup'),
    'select_doom' : ('dooms.lookups', 'DoomLookup'),
    'search_doom' : ('doom_search.lookups', 'DoomLookup'),
    'search_competition' : ('competition_search.lookups', 'CompetitionLookup')
    # this specifies to look for the class `ContactLookup` in the `peoplez.lookups` module
}


# magically include jqueryUI/js/css
AJAX_SELECT_BOOTSTRAP = False
AJAX_SELECT_INLINES = False

DEBUG_TOOLBAR_CONFIG = {
    "INTERCEPT_REDIRECTS": False,
}

doume = None
void = None
    

xmpp_client = None
try:
    logging.config.fileConfig("logging.conf")
    logging.getLogger("dajaxice").setLevel(logging.DEBUG)
    logging.getLogger("django.db.backends").setLevel(logging.WARNING)
except:
    pass

# local_settings.py can be used to override environment-specific settings
# like database and email that differ between development and production.
try:
    from local_settings import *
except ImportError:
    pass

