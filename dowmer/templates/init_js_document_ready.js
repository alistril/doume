common_fn.setup_facebox();                
payments.handle_payment_display();

doom_create_form.ajaxify_form("menu");

$("#trans-notif-menu").doomenu();


$("#trans-accordeon").douaccordeon();
$("#notif-accordeon").douaccordeon();

$("#info-panel").on("mouseenter",".competition-preview",function(){
	$(".competition-search-interaction",this).show();
});


$("#info-panel").on("mouseleave",".competition-preview",function(){
	$(".competition-search-interaction",this).hide();
});

{% if request.user.is_authenticated %}
$("#trans-notif-menu").bind('doumenu_opened',
    function(event,element){   
		switch($(element).attr("id")){
		case "notifications":
			notifications.get_latest_notifications_menu(); 
            page_state.static_notifications.count = 0;
            page_state.interactive_notifications.clear();
            common_fn.badge_notification_based_on_page_settings();
        	break;
		case "transactions":
			transactionbutton.get_latest_dooms_menu();
			break;
		}
    }
);
{% endif %}
$(document).on("click",".doom-speech > #doom-speech-header > a",function(event){
    event.preventDefault();
    $("#doom-speech-content",$(this).parent().parent()).toggle();
});

$(document).on("click",".wallpost-doom-details > #wallpost-doom-details-header > a",function(event){
    event.preventDefault();
    $("#wallpost-doom-details-contents",$(this).parent().parent()).toggle();
});


$("#work-panel form > ul > li > a#edit-logic").click(function(event){
    event.preventDefault();
    competition_config.competitions_page.toggle_help_config();
    doom_create_form.adapt_forms_to_status("{% url dooms_general_create %}");
});


$("#title-bar form > ul > li > a#edit-logic").click(function(event){
    event.preventDefault();
    competition_config.competitions_menu.toggle_help_config();
    doom_create_form.adapt_forms_to_status("{% url dooms_general_create %}");
});

$(document).on("click","a.if_invitation_accept",function(e){
	e.preventDefault();
	interactive_friends_common.accept_invitation(parseInt($(this).attr("value")));
});
$(document).on("click","a.if_invitation_decline",function(e){
	e.preventDefault();
	interactive_friends_common.decline_invitation(parseInt($(this).attr("value")));
});

$("form#search_bar_competition_form").on("submit",function(){
	competition_search.submit_search_bar_competition();
});

$("form#search_bar_doom_form").on("submit",function(){
	doom_search.submit_search_bar_doom();
});

$(document).on("click","a.find-dooms-related-to-competition", function(){
	competition_search.find_dooms_related_to_competition(parseInt($(this).attr("value")));
});

$("span.timeago").timeago();
common_fn.badge_notification_based_on_page_settings();

$("#profile-menu").hide();
$("#profile-menu").menu();
$("#profile-menu-title").mouseenter(function(){
	$("#profile-menu").show();
});

$("#profile-menu").mouseleave(function(){
	$("#profile-menu").hide();
});

/*
$.ajax(
		{
			type : 'POST',
			url : '/async',
			dataType : 'json',
			data : xmpp_options,
			success : function(data){				
				//console_log(data);
                xmpp_credentials = 
                    {
                        jid : data.jid,
                        rid : data.rid,
                        sid : data.sid
                    };
				client.init_pubsub(xmpp_credentials.jid,xmpp_credentials.rid,xmpp_credentials.sid,function(entry, node,conn){notifications.notification_event_cb(entry, node,conn);});
                if(xmpp_other_init.call){
                    xmpp_other_init.call(pubsub_conn);
                }
			},
			error : function(XMLHttpRequest, textStatus, errorThrown) {
				myconsole.log('error');
				myconsole.log(textStatus);
				myconsole.log(errorThrown);
			}
		}
);
*/

