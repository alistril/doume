from django.shortcuts import render_to_response
from django.template import RequestContext
from transactionbutton.forms import SearchDoomForm,SearchCompetitionForm
from categories.helpers import get_prefered_dooms_context
from dooms.helpers import get_bought_nonsold_dooms_context
from forms import HomePageForm
from interface.utils import interface_for_user
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse  

def group_and_bridge(request):
    """
    Given the request we can depend on the GroupMiddleware to provide the
    group and bridge.
    """
    
    # be group aware
    group = getattr(request, "group", None)
    if group:
        bridge = request.bridge
    else:
        bridge = None
    
    return group, bridge

def group_context(group, bridge):
    # @@@ use bridge
    ctx = {
        "group": group,
    }
    if group:
        ctx["group_base"] = bridge.group_base_template()
    return ctx
    
def home(request,template_name="homepage.html"):
    iface = interface_for_user(request.user)
    if not iface.display_homepage:
        return HttpResponseRedirect(reverse("home_detail"))
    else:
        if request.user.is_authenticated(): #don't display hide-homepage options when not logged in.        
            if request.method == "POST":
                homepage_form = HomePageForm(request.POST,instance = iface)
                if homepage_form.is_valid():
                    homepage_form.save()
                if not homepage_form.display_homepage():
                    return HttpResponseRedirect(reverse("home_detail"))
            else:
                homepage_form = HomePageForm(instance = iface)
        else:
            homepage_form = None
        return render_to_response(template_name, {'homepage_form' : homepage_form},context_instance=RequestContext(request))

def testhtml(request,template_name="testhtml.html"):  
    form_comp_inst = SearchCompetitionForm()
    context = get_bought_nonsold_dooms_context(request)
    context.update({"search_form_comp" : form_comp_inst})
    
    form_doom_inst = SearchDoomForm()
    context.update(get_prefered_dooms_context(request))
    context.update({"search_form_doom" : form_doom_inst})

    return render_to_response(template_name,  context,                            
                              context_instance=RequestContext(request))  
