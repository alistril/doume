from django.conf import settings
from django.conf.urls.defaults import include,url,patterns

#import gevent
#gevent.monkey.patch_all()
from django.contrib import admin
admin.autodiscover()

from dajaxice.core import dajaxice_autodiscover, dajaxice_config
dajaxice_autodiscover()

from pubsub.core import establish_xmpp_link
establish_xmpp_link()

from django.contrib.auth.models import User
settings.doume = User.objects.get(username="doume")
settings.void  = User.objects.get(username="void")

from ajax_select import urls as ajax_select_urls
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

admin.autodiscover()

urlpatterns = patterns(
    "",
    url(r"^$", "views.home", name="home"),
    url(r"^testhtml$", "views.testhtml", name="testhtml"),
    url(r"^profiles/", include("profiles.urls")),
    (r'^accounts/', include('registration_backends.beer.urls')),
    url(r"^avatar/", include("avatar.urls")),
    url(r"^interactive_notifications/", include("interactive_notification.urls")),
    url(r"^interactive_friends/", include("interactive_friends.urls")),
    url(r"^teams/", include("teams.urls")),
    url(r"^walls/", include("wall.urls")),
    url(r"^dooms/", include("dooms.urls")),    
    url(r"^payments/", include("payments.urls")),    
    url(r"^payment_services/", include("payment_services.urls")),    

    url(r"^bank/", include("bank.urls")),
    url(r"^categories/", include("categories.urls")),
    url(r"^home/", include("home.urls")),
    url(r"^transactionbutton/", include("transactionbutton.urls")),    
    url(r"^help/", include("help.urls")),
    
    (r'^admin/lookups/', include(ajax_select_urls)),
    url(r"^admin/", include(admin.site.urls)),
    url(r'^search/', include('haystack.urls')),
    url(dajaxice_config.dajaxice_url, include('dajaxice.urls')),
    url(r'^im/', include('im.urls')),
    url(r'^pilot/', include('pilot.urls')),
    url(r'^chat/', include('chat.urls')),
    url(r'^doc/', include('doc.urls'))
      
)

urlpatterns += staticfiles_urlpatterns()
"""

if settings.SERVE_MEDIA:
    urlpatterns += patterns("",
        url(r"", include("staticfiles.urls")),
    )
"""
