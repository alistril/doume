from django.contrib import admin
from ajax_select import make_ajax_form
from ajax_select.admin import AjaxSelectAdmin
from example.models import *

class PersonAdmin(admin.ModelAdmin):
    pass
admin.site.register(Person,PersonAdmin)

# subclass AjaxSelectAdmin
class LabelAdmin(AjaxSelectAdmin):
    # create an ajax form class using the factory function
    #                     model,fieldlist,   [form superclass]
    form = make_ajax_form(Label,{'owner':'person'})
admin.site.register(Label,LabelAdmin)